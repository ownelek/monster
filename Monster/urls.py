from django.conf.urls import patterns, include, url
from django.contrib import admin

from Monster import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Monster.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^play/$', views.play, name="main.play"),
    url(r'^login/$', views.login_view, name="main.login"),
    url(r'^logout/$', views.logout_view, name="main.logout"),
    url(r'^mapeditor/$', views.editor, name="main.mapedit"),
    url(r'^api/', include('map.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^test/', views.test, name = 'main.test'),
    url(r'^pma/', views.pma, name = 'main.pma'),
)
