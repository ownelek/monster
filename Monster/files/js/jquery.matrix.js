(function($) {
    function tick(original, originalHtml, tickTime, callbacks) {
        var length = $(this).text().length;

        if(callbacks.onTick) callbacks.onTick.call(this);

        $(this).html(original.substr(0, ++length));

        if(length < original.length)
            setTimeout(function() {
                tick.call(this, original, tickTime, callbacks);
            }.bind(this), tickTime);
        else
        {
            $(this).html(originalHtml);
            if(callbacks.onFinish) callbacks.onFinish.call(this);
        }

    }

    $.fn.matrix = function(tickTime, callbacks) {
        return this.each(function() {
            var $this = $(this);
            var original = $this.text(),
                originalHtml = $this.html();

            $this.html("");

            tick.call(this, original, originalHtml, tickTime, callbacks || {});
        });
    };
})(jQuery);
