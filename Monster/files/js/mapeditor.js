(function() {
    // Setup CSRF Token header
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    Backbone.$.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
            }
        }
    });

    window.Mapeditor = {
        Models: [],
        Collections: [],
        Views: [],
        Variables: {},

        start: function() {
            // First collections
            Mapeditor.Variables.urlsModel = new Mapeditor.Models.Urls();
            Mapeditor.Variables.urlsModel.fetch({
                success: success
            });

            // run fetch after successfully loading urls from server
            function success() {
                Mapeditor.Variables.tileSetsCollection.fetch();
                Mapeditor.Variables.tileTypesCollection.fetch();
                Mapeditor.Variables.moveMasksCollection.fetch();
                Mapeditor.Variables.mapsCollection.fetch();
                Mapeditor.Variables.entityTypesCollection.fetch();
                Mapeditor.Variables.monsterTypesCollection.fetch();
            }

            Mapeditor.Variables = _.extend(Mapeditor.Variables, {
                windowsCollection: new Mapeditor.Collections.Windows(),
                tileSetsCollection: new Mapeditor.Collections.TileSets(),
                tileTypesCollection: new Mapeditor.Collections.TileTypes(),
                moveMasksCollection: new Mapeditor.Collections.MoveMasks(),
                mapsCollection: new Mapeditor.Collections.Scenes(),
                entityTypesCollection: new Mapeditor.Collections.EntityTypes(),
                monsterTypesCollection: new Mapeditor.Collections.MonsterTypes()
            });

            // Views later
            Mapeditor.Variables = _.extend(Mapeditor.Variables, {
                tabsView: new Mapeditor.Views.Tabs({ collection: Mapeditor.Variables.windowsCollection }),
                windowsView: new Mapeditor.Views.Windows({ collection: Mapeditor.Variables.windowsCollection }),
                tileTypesView: new Mapeditor.Views.TileTypes({ collection: Mapeditor.Variables.tileTypesCollection }),
                moveMasksView: new Mapeditor.Views.MoveMasks({ collection: Mapeditor.Variables.moveMasksCollection }),
                entityTypesView: new Mapeditor.Views.EntityTypes({ collection: Mapeditor.Variables.entityTypesCollection }),
                entityPropertiesView: new Mapeditor.Views.EntityProperties().render(),
                generalMapModel: new Mapeditor.Models.GeneralMap({
                    width: window.innerWidth,
                    height: window.innerHeight
                }),
                modalAddRoomView: new Mapeditor.Views.ModalAddRoom(),
                modalCreateRoomView: new Mapeditor.Views.ModalCreateRoom(),
                savingMap: false,
                generatingMapImage: false
            });

            Mapeditor.Variables.modalAddRoomView.render();
            Mapeditor.Variables.modalCreateRoomView.render();
            Mapeditor.Variables.tabsView.render();
            Mapeditor.Variables.windowsView.render();

            $(document).on("click", ".showButton", function(event) {
                var $this = $(this),
                    activeButton = $(".showButton.active");
                    sideShow = $("#" + $this.data("for"));

                if(activeButton.length > 0 && !$this.hasClass("active"))
                {
                    activeButton.removeClass("active");
                    $(".sideShow.active").removeClass("active");
                }

                sideShow.toggleClass("active");
                $this.toggleClass("active");
            });

            $("div.sideShow").css({
                height: window.innerHeight
            });
        },

        openGeneralMap: function() {
            var map = new Mapeditor.Models.Window({ title: "General Map", view: new Mapeditor.Views.GeneralMap({ model: Mapeditor.Variables.generalMapModel }) });
            Mapeditor.Variables.windowsCollection.add(map);
            Mapeditor.Variables.windowsCollection.setActive(map);
        },

        openMap: function(map) {
            Mapeditor.setStatus("Wczytywanie {0} ...".format(map.get("name")));
            console.log(map);
            map.get("tiles").fetch({
                reset: true,
                success: function() {
                    map.get("entities").fetch({
                        success: function() {
                            Mapeditor.setStatus("Wczytywanie {0} ... Gotowe!".format(map.get("name")));
                        }
                    });
                }
            });


            var mapViewer = new Mapeditor.Views.MapViewer({ model: map }),
                mapWindow = new Mapeditor.Models.Window({
                    title: map.get("name"),
                    view: mapViewer
            });

            Mapeditor.Variables.windowsCollection.add(mapWindow);
            Mapeditor.Variables.windowsCollection.setActive(mapWindow);
        },

        setStatus: function(text) {
            $("#status").text(text);
        }
    };
})();

(function() {
    Mapeditor.Models.Urls = Backbone.Model.extend({
        url: "/api/"
    });
})();

(function() {
    Mapeditor.Models.Scene = Backbone.RelationalModel.extend({
        defaults: {
            isSaving: false,
            isInvalid: false,
            type: "global",
            x: 0,
            y: 0
        },

        initialize: function() {
            this.listenTo(this.get("tiles"), "update", this.updatePositionMap);
            this.positionMap = [];
            this.tilesInLayers = {};
        },

        checkLayers: function() {
            this.tilesInLayers = this.get("tiles").countBy(function(value){
                return value.get("z");
            });
        },

        createLayer: function(layer) {
            if(this.tilesInLayers[layer] >= this.get("sizex") * this.get("sizey"))
                return false;

            var tiles = this.get("tiles").toJSON(),
                tilesInLayer = _.where(tiles, { z: layer }),
                tileType = Mapeditor.Variables.tileTypesCollection.get(1),
                moveMask = Mapeditor.Variables.moveMasksCollection.get(1);

            _.each(_.range(this.get("x"), this.get("x") + this.get("sizex")), function(x) {
                _.each(_.range(this.get("y"), this.get("y") + this.get("sizey")), function(y) {
                    if(!_.findWhere(tilesInLayer, { x: x, y: y })) {
                        var tile = new Mapeditor.Models.Tile({
                            scene: this,
                            x: x,
                            y: y,
                            z: layer,
                            tileType: tileType,
                            moveMask: moveMask
                        }, { silent: true });
                    }
                }, this);
            }, this);

            this.checkLayers();
            return true;
        },

        removeLayer: function(layer, callback) {
            $.post(this.url() + "/removeLayer", { layer: layer }, function() {
                this.get("tiles").remove(this.get("tiles").filter(function(tile) { return tile.get("z") == layer; }));
                delete this.tilesInLayers[layer];

                _.each(this.get("tiles").filter(function(tile) {
                    return tile.get("z") > layer;
                }), function(tile) {
                    tile.set({
                        z: tile.get("z") - 1
                    });
                });

                _.each(this.tilesInLayers, function(num, index) {
                    if(index > layer)
                    {
                        this.tilesInLayers[parseInt(index) - 1] = this.tilesInLayers[index];
                        delete this.tilesInLayers[index];
                    }
                }, this);

                callback.call(this);
            }.bind(this));
        },

        updatePositionMap: function(collection, options) {
            collection.each(function(tile) {
                this.positionMap[this.get("sizex") * this.get("sizey") * tile.get("z") + this.get("sizex") * tile.get("y") + tile.get("x")] = tile;
            }, this);
        },

        validate: function() {
            var ret = this.collection.find(function(map) {
                return this != map && ( (( this.get("x") > map.get("x") && this.get("x") < map.get("x") + map.get("sizex") ) ||
                    ( map.get("x") > this.get("x") && map.get("x") < this.get("x") + this.get("sizex") )) &&
                    (( this.get("y") > map.get("y") && this.get("y") < map.get("y") + map.get("sizey") )  ||
                    ( map.get("y") > this.get("y") && map.get("y") < this.get("y") + this.get("sizey") )) );
            }, this);

            var isValid = !ret;
            if(this.lastValidation != isValid)
            {
                this.lastValidation = isValid;
                this.trigger("validate", this, isValid);
            }

            console.log("validate", isValid);

            if(!isValid)
                return "map collides with another map";
        },

        relations: [
            {
                type: Backbone.HasMany,
                key: "tiles",
                relatedModel: "Mapeditor.Models.Tile",
                collectionType: "Mapeditor.Collections.Tiles",
                collectionKey: false,
                collectionOptions: function(model) {
                    return { parent: model };
                },
                includeInJSON: false,
                reverseRelation: {
                    key: "scene",
                    includeInJSON: "id"
                }
            },
            {
                type: Backbone.HasMany,
                key: "linked_scenes",
                relatedModel: "Mapeditor.Models.Scene",
                collectionType: "Mapeditor.Collections.Scenes",
                collectionKey: false,
                includeInJSON: "id"
            },
            {
                type: Backbone.HasMany,
                key: "monsters",
                relatedModel: "Mapeditor.Models.MonsterInMap",
                collectionType: "Mapeditor.Collections.MonstersInMap",
                reverseRelation: {
                    key: "scene"
                }
            }
        ]
    });
})();

(function() {
    Mapeditor.Models.Entity = Backbone.RelationalModel.extend({
        getProperty: function(key) {
            return _.find(this.get("properties"), function(prop) {
                return prop.key == key;
            });
        },

        setProperty: function(key, value) {
            var prop = this.getProperty(key);
            prop.value = value;
        },
        
        relations: [
            {
                type: Backbone.HasOne,
                key: "type",
                relatedModel: "Mapeditor.Models.EntityType",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                key: "scene",
                relatedModel: "Mapeditor.Models.Scene",
                includeInJSON: "id",
                reverseRelation: {
                    key: "entities",
                    collectionType: "Mapeditor.Collections.Entities",
                    collectionOptions: function(scene) {
                        return {
                            parentScene: scene
                        };
                    }
                }
            }
        ]
    });
})();

(function() {
    Mapeditor.Models.EntityType = Backbone.RelationalModel.extend({
        defaults: {
            name: "",
            sprite: "",
            visible: false,
            sizex: 0,
            sizey: 0
        }
    });
})();

(function() {
    Mapeditor.Models.GeneralMap = Backbone.Model.extend({
        defaults: {
            offsetX: 0,
            offsetY: 0,
            width: 0,
            height: 0,
            adding: false,
            deleting: false,
            pixelsPerUnit: 5
        }
    });
})();

(function() {
    Mapeditor.Models.MonsterInMap = Backbone.RelationalModel.extend({
        defaults: {
            monster: null,
            type: "grass",
            min_level: 1,
            max_level: 1,
            frequency: 0
        },
        getEncounterChance: function() {
            var max = 0;
            this.collection.each(function(monsterInMap) {
                max += monsterInMap.get("frequency");
            });
            
            return this.get("frequency") / max;
        },
        relations: [{
            type: Backbone.HasOne,
            key: "monster",
            relatedModel: "Mapeditor.Models.MonsterType",
            includeInJSON: "id"
        }]
    });
})();

(function() {
    Mapeditor.Models.MonsterType = Backbone.RelationalModel.extend();
})();

(function() {
    Mapeditor.Models.MoveMask = Backbone.RelationalModel.extend({
        defaults: {
            id: 0,
            name: null,
            color: null
        }
    });
})();

(function() {
    Mapeditor.Models.Tile = Backbone.RelationalModel.extend({
        initialize: function() {
            this._toSave = false;
            this.listenTo(this, "change", this.setChanged);
        },
        setChanged: function() {
            this._toSave = true;
        },
        relations: [
            {
                type: Backbone.HasOne,
                keySource: "tile_type",
                key: "tileType",
                relatedModel: "Mapeditor.Models.TileType",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                keySource: "move_mask",
                key: "moveMask",
                relatedModel: "Mapeditor.Models.MoveMask",
                includeInJSON: "id"
            }
        ]
    });
})();

(function() {
    Mapeditor.Models.TileSet = Backbone.RelationalModel.extend({
        defaults: {
            name: "",
            path: ""
        }
    });
})();

(function() {
    Mapeditor.Models.TileType = Backbone.RelationalModel.extend({
        defaults: {
            id: 0,
            key_name: null,
            x: null,
            y: null
        },
        relations: [{
            type: Backbone.HasOne,
            key: "tileset",
            relatedModel: "Mapeditor.Models.TileSet",
            includeInJSON: "id",
            reverseRelation: {
                key: "tile_types",
                includeInJSON: false
            }
        }]
    });
})();

(function() {
    Mapeditor.Models.Window = Backbone.Model.extend({
        defaults: {
            title: null,
            active: false,
            view: null
        }
    });
})();

(function() {
    Mapeditor.Collections.Entities = Backbone.Collection.extend({
        model: Mapeditor.Models.Entity,
        initialize: function(models, options) {
            if(options && options.parentScene) {
                this.parentScene = options.parentScene;
            }
        },
        url: function() {
            if(this.parentScene) {
                return "{0}/entities/".format(_.result(this.parentScene, "url"));
            }

            return Mapeditor.Variables.urlsModel.get("entities");
        },

        save: function(options) {
            if(!this.parentScene)
                return;

            var xhr = $.ajax({
                url: _.result(this, "url"),
                type: "POST",
                data: JSON.stringify(this.toJSON()),
                dataType: "json",
                contentType: "application/json"
            });

            xhr.done(function(data) {
                this.set(data);
                this.trigger("sync", this, data, options);
            }.bind(this));

            this.trigger("request", this, xhr, options);
        }
    });
})();

(function() {
    Mapeditor.Collections.EntityTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.EntityType,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("entitytypes");
        }
    });
})();

(function() {
    Mapeditor.Collections.MonstersInMap = Backbone.Collection.extend({
        model: Mapeditor.Models.MonsterInMap,

        initialize: function() {
            this.listenTo(this, "add", this.addListener);
            this.listenTo(this, "remove", this.removeListener);
        },

        addListener: function(model, collection, options) {
            this.listenTo(model, "change:frequency", this.updateEncounterChance);
        },

        removeListener: function(model, collection, options) {
            this.stopListening(model);
        },

        updateEncounterChance: function(model, value, options) {
            this.each(function(monsterInMap) {
                monsterInMap.trigger("change:encounter_chance", monsterInMap, monsterInMap.getEncounterChance(), options);
            });
        }
    });
})();

(function() {
    Mapeditor.Collections.MonsterTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.MonsterType,
        url: "/api/monstertypes/"
    });
})();

(function() {
    Mapeditor.Collections.MoveMasks = Backbone.Collection.extend({
        model: Mapeditor.Models.MoveMask,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("movemasks");
        }
    });
})();

(function() {
    Mapeditor.Collections.Scenes = Backbone.Collection.extend({
        model: Mapeditor.Models.Scene,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("maps");
        }
    });
})();

(function() {
    Mapeditor.Collections.Tiles = Backbone.Collection.extend({
        model: Mapeditor.Models.Tile,

        initialize: function(models, options) {
            this.parent = options.parent;
        },

        url: function() {
            return Mapeditor.Variables.urlsModel.get("maps") + this.parent.id + "/tiles/";
        },

        // send clear request, only to server
        clear: function(options) {
            var self = this;
            $.post(this.parent.url() + "/tiles/clear/", function(data) {
                if(options.success) options.success.call(self, this, data);
            }, "json");
        },

        changedTiles: function() {
            return this.filter(function(tile) {
                return tile._toSave || tile.isNew();
            }, this);
        },

        save: function(options) {
            var requestsNum = 0,
                maxRequests = 6,
                tilesPerRequest = 50,
                queue = [],
                tiles = new Mapeditor.Collections.Tiles(this.filter(function(tile) {
                    var r = tile._toSave;
                    tile._toSave = false;
                    return r || tile.isNew();
                }), { parent: this.parent }).toJSON(),
                savedTiles = 0,
                maxTiles = tiles.length;

            console.log(maxTiles, tiles);
            function queueFunction() {
                //console.log(this.tiles);
                var xhr = $.ajax({
                    url: this.url,
                    type: "POST",
                    data: JSON.stringify(this.tiles),
                    dataType: "json",
                    contentType: "application/json"
                }).then(function(data, statusText, jqXHR) {
                    savedTiles += data.length;
                    if(data.length > 0)
                    {
                        _.each(data, function(new_tile) {
                            var tileModel = this.map.positionMap[
                                this.map.get("sizex") * this.map.get("sizey") * new_tile.z +
                                this.map.get("sizex") * new_tile.y +
                                new_tile.x
                            ];

                            if(tileModel) {
                                tileModel.set(new_tile);
                            }
                        }, this);
                    }

                    requestsNum--;
                    fireQueue();
                    this.tilesCollection.trigger("sync", this.tilesCollection, data, xhr, options);
                    //this.tilesCollection.trigger("save", this.tilesCollection, data, savedTiles, maxTiles);
                }.bind(this));

                this.tilesCollection.trigger("request", this.tilesCollection, xhr);
            }

            function fireQueue()
            {
                while(requestsNum < maxRequests && queue.length > 0)
                {
                    setTimeout(queue.shift(), 0);
                    requestsNum++;
                }
            }

            while(tiles.length > 0)
            {
                var context = {
                    tiles: _.first(tiles, tilesPerRequest),
                    tilesCollection: this,
                    map: this.parent,
                    url: this.url()
                };

                tiles = _.rest(tiles, tilesPerRequest);
                queue.push(queueFunction.bind(context));
            }

            fireQueue();
        },

        destroy_outsides: function(options) {
            options = options || {};

            function success(data, status, xhr) {
                if(options.success) options.success.call(this, data, status, xhr);

                this.each(function(tile) {
                    if(!tile)
                        return;

                    var map = tile.get("scene");

                    if( tile.get("x") < map.get("x") || tile.get("x") >= map.get("x") + map.get("sizex") ||
                            tile.get("y") < map.get("y") || tile.get("y") >= map.get("y") + map.get("sizey"))
                        this.remove(tile);
                }, this);
            }

            $.ajax({
                method: "DELETE",
                url: _.result(this, "url") + "clear_outside/",
                success: success.bind(this)
            });
        }
    });
})();

(function() {
    Mapeditor.Collections.TileSets = Backbone.Collection.extend({
        model: Mapeditor.Models.TileSet,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("tilesets");
        }
    });
})();

(function() {
    Mapeditor.Collections.TileTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.TileType,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("tiletypes");
        }
    });
})();

(function() {
    Mapeditor.Collections.Windows = Backbone.Collection.extend({
        model: Mapeditor.Models.Window,

        setActive: function(windowObj) {
            this.forEach(function(element, index, list) {
                if(element == windowObj)
                    element.set({active: true});
                else
                    element.set({active: false});
            });
        }
    });
})();

(function() {
    Mapeditor.Views.Entity = Backbone.View.extend({
        tagName: "div",
        className: "entity",

        initialize: function() {
            this.listenTo(this.model, "change:x", this.render);
            this.listenTo(this.model, "change:y", this.render);
            this.listenTo(this.model, "change:face", this.changeFace);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.css({
                top: this.model.get("y") * 32,
                left: this.model.get("x") * 32,
                width: this.model.get("type").get("sizex") * 32,
                height: this.model.get("type").get("sizey") * 32,
                backgroundImage: "url(/static/{0})".format(this.model.get("type").get("sprite"))
            });
            this.$el.toggleClass(this.model.get("type").get("name"), true);
            this.$el.data("model", this.model);

            this.changeFace();
            return this;
        },

        changeFace: function() {
            this.$el.removeClass(this.model.previous("face"));
            this.$el.addClass(this.model.get("face"));
        }
    });
})();

(function() {
    Mapeditor.Views.EntityProperties = Backbone.View.extend({
        el: "div#entityProperties",

        events: {
            "keyup input": function(event) {
                var $input = $(event.currentTarget);
                if(this.actualModel) {
                    if($input.attr("property-type") == "basic")
                        this.actualModel.set($input.attr("name"), $input.val());
                    else if($input.attr("property-type") == "normal")
                        this.actualModel.setProperty($input.attr("name"), $input.val());
                }
            }
        },

        initialize: function() {
            this.template = _.template( $("#entityPropertiesTemplate").html() );
        },

        render: function(model) {
            this.actualModel = model;
            if(model) {
                this.$el.show();
                this.$el.html(this.template({ model: model }));
            }
            else {
                this.$el.hide();
            }

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.EntityType = Backbone.View.extend({
        tagName: "div",
        className: "entityType block",

        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        initialize: function() {

        },

        render: function() {
            this.$el.data("entityType", this.model);
            this.$el.css({
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32,
                backgroundImage: "url(/static/{0})".format(this.model.get("sprite"))
            });
            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.EntityTypes = Backbone.View.extend({
        el: "div#entityTypesList",

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(element, index, list) {
                var view = new Mapeditor.Views.EntityType({ model: element });
                this.$el.append(view.render().$el);
            }, this);
            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.GeneralMap = Backbone.View.extend({
        tagName: "div",
        id: "generalMap",
        model: Mapeditor.Models.GeneralMap,

        events: {
            "click button.addMap": function(event) {
                this.model.set("adding", !this.model.get("adding"));
            },
            "click button.removeMap": function(event) {
                this.model.set("deleting", !this.model.get("deleting"));
            },
            "mousedown": function(event) {
                if(event.which == 3)
                {
                    this.moving = true;
                    this.mousePosition = [ event.pageX, event.pageY ];
                    this.oldOffset = [ this.model.get("offsetX"), this.model.get("offsetY") ];
                }
            },
            "mousemove": function(event) {
                if(this.moving)
                {
                    this.model.set({
                        offsetX: Math.min(0, this.oldOffset[0] + event.pageX - this.mousePosition[0]),
                        offsetY: Math.min(0, this.oldOffset[1] + event.pageY - this.mousePosition[1])
                    });
                }
            },
            "mouseup": function(event) {
                this.moving = false;
            }
        },

        initialize: function() {
            this.template = _.template( $("#generalMapTemplate").html() );
        },

        render: function() {
            this.$el.html(this.template());
            this.$("#topMeasureView").append(new Mapeditor.Views.TopMeasure({ model: this.model }).render().$el);
            this.$("#leftMeasureView").append(new Mapeditor.Views.LeftMeasure({ model: this.model }).render().$el);
            this.$("#mapsView").append(new Mapeditor.Views.Maps(
                { collection: Mapeditor.Variables.mapsCollection },
                { generalMapModel: this.model }
            ).render().$el);

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.LeftMeasure = Backbone.View.extend({
        tagName: "div",
        id: "leftMeasure",
        model: Mapeditor.Models.GeneralMap,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:offsetY", this.moveMe);
        },

        render: function() {
            this.$el.empty();
            this.$el.css({
                height: this.model.get("offsetY") + this.model.get("height") + 1000
            });

            this.maxI = this.model.get("height") / this.model.get("pixelsPerUnit");
            for(var i = 0; i < this.maxI; i++)
            {
                var view = new Mapeditor.Views.LeftUnit({ model: this.model });
                this.$el.append(view.$el);
                view.render(i);
            }

            return this;
        },

        moveMe: function(model, value, options) {
            this.$el.css({
                height: this.model.get("offsetY") + this.model.get("height") + 1000
            });

            this.$el.parent().scrollTop(-value);
        }
    });
})();

(function() {
    Mapeditor.Views.LeftUnit = Backbone.View.extend({
        tagName: "div",
        className: "unit",

        render: function(i) {
            this.$el.css("top", i * this.model.get("pixelsPerUnit") * 5);
            if(i % 2 === 0) {
                this.$el.html("<span>{0}</span>".format(i * 5));
                this.$("span").css("left", -(('' + (i * 5)).length) * 8 - 2);
            }

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.Map = Backbone.View.extend({
        tagName: "div",
        className: "map",

        events: {
            "click div.name span": function(event) {
                Mapeditor.openMap(this.model);
                event.preventDefault();
            },

            "mousedown div.name span": function(event) {
                // if we want to open map, it gets automatically saved, coz of bubbling
                // this prevent it
                event.stopPropagation();
            },

            "keypress .editName": function(event) {
                if(event.key == "Enter" && $(event.currentTarget).find("input").val().length > 0) {
                    this.model.set("name", $(event.currentTarget).find("input").val());
                }
            },

            "keypress .editKeyName": function(event) {
                if(event.key == "Enter" && $(event.currentTarget).find("input").val().length > 0) {
                    this.model.set("key_name", $(event.currentTarget).find("input").val());
                }
            }
        },

        initialize: function(attrs, options) {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:name", this.updateName);
            this.listenTo(this.model, "change:key_name", this.updateName);
            this.listenTo(this.model, "change:x", this.setPosition);
            this.listenTo(this.model, "change:y", this.setPosition);
            this.listenTo(this.model, "change:sizex", this.setSize);
            this.listenTo(this.model, "change:sizey", this.setSize);
            //this.listenTo(this.model, "change:isSaving", this.updateClass);
            this.listenTo(this.model, "validate", this.updateValidate);
            this.listenTo(this.model, "request", this.setSaving);
            this.listenTo(this.model, "sync", this.unsetSaving);
            this.listenTo(this.model, "destroy", this.remove);
            this.template = _.template( $("#mapTemplate").html() );
            this.generalMapModel = options.generalMapModel;
        },

        render: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");
            this.$el.html( this.template({
                name: this.model.get("name"),
                x: this.model.get("x"),
                y: this.model.get("y"),
                sizex: this.model.get("sizex"),
                sizey: this.model.get("sizey")
            }) );

            this.$el.data("map", this.model);
            this.updateName();

            this.$el.css({
    			top: this.model.get("y") * pixelsPerUnit - 1,
    			left: this.model.get("x") * pixelsPerUnit - 1,
    			width: this.model.get("sizex") * pixelsPerUnit + 1,
    			height: this.model.get("sizey") * pixelsPerUnit + 1
    		});

            return this;
        },

        updateName: function() {
            if(!this.model.get("name")) {
                this.$("div.active").removeClass("active");
                this.$(".editName").addClass("active");
            }
            else if(!this.model.get("key_name")) {
                this.$("div.active").removeClass("active");
                this.$(".editKeyName").addClass("active");
            }
            else {
                this.$("div.active").removeClass("active");
                this.$(".name").addClass("active");
                this.$(".name span").text(this.model.get("name"));
            }
        },

        setPosition: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");

            this.$(".coords").html("{0}, {1}".format(this.model.get("x"), this.model.get("y")));
            this.$el.css({
                top: this.model.get("y") * pixelsPerUnit - 1,
                left: this.model.get("x") * pixelsPerUnit - 1
            });
        },

        setSize: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");

            this.$(".sizex").html(this.model.get("sizex"));
            this.$(".sizey").html(this.model.get("sizey"));

            this.$el.css({
                width: this.model.get("sizex") * pixelsPerUnit + 1,
                height: this.model.get("sizey") * pixelsPerUnit + 1
            });
        },

        updateValidate: function(model, isValid) {
            //console.log(isValid);
            this.$el.toggleClass("invalid", !isValid);
        },

        setSaving: function() {
            this.$el.toggleClass("saving", true);
        },

        unsetSaving: function() {
            this.$el.toggleClass("saving", false);
        }
    });
})();

(function() {
    Mapeditor.Views.MapLayer = Backbone.View.extend({
        tagName: "div",
        className: "layer",
        model: Mapeditor.Collections.Tiles,

        initialize: function(attrs, options) {
            this.layer = options.layer;
            this.state = options.state;
            this.rendered = false;

            this.listenTo(this.model, "add:entities", this.addEntity);
        },

        render: function() {
            this.$el.empty();
            this.model.get("tiles").forEach(function(model, index, collection) {
                if(model.get("z") == this.layer)
                    this.$el.append(new Mapeditor.Views.Tile({ model: model }).render().$el);
            }, this);

            this.model.get("entities").each(function(entity) {
                if(entity.get("z") == this.layer)
                    this.$el.append(new Mapeditor.Views.Entity({ model: entity }).render().$el);
            }, this);

            this.toggleLayer(this.state);
            this.rendered = true;
            return this;
        },

        updateZIndex: function() {
            this.$el.css({
                zIndex: this.layer + 1
            });
        },

        toggleLayer: function(state) {
            this.$el.toggleClass("visible", state == "visible" || state == "halfVisible");
            this.$el.toggleClass("half", state == "halfVisible");
        },

        addEntity: function(entity) {
            console.log("added");
            if(entity.get("z") == this.layer)
                this.$el.append(new Mapeditor.Views.Entity({ model: entity }).render().$el);
        }
    });
})();

(function() {
    Mapeditor.Views.MapViewer = Backbone.View.extend({
        events: {
            "click .world .entity": function(event) {
                this.$(".entity.selected").removeClass("selected");
                var $entity = $(event.currentTarget);
                $entity.addClass("selected");
                Mapeditor.Variables.entityPropertiesView.render($entity.data("model"));
            },
            "click .world .tile": function(event) {
                var $active = $("div.block.active"),
                    $selected = $("div.selected");

                if($active.hasClass("entityType")) {
                    var entityType = $active.data("entityType"),
                        tileModel = $(event.currentTarget).data("tileModel");

                    if(!entityType || !tileModel)
                        return;

                    this.model.get("entities").add(new Mapeditor.Models.Entity({
                        scene: this.model,
                        type: entityType,
                        face: "s",
                        x: tileModel.get("x"),
                        y: tileModel.get("y"),
                        z: tileModel.get("z"),
                        properties: entityType.get("properties").map(function(property) {
                            return {
                                key: property.name,
                                value: property.default
                            };
                        })
                    }));

                    $active.removeClass("active");
                }
                else if($selected.length > 0) {
                    $selected.removeClass("selected");
                    Mapeditor.Variables.entityPropertiesView.render();
                }
            },
            "mousedown .world": function(event) {
                if(event.which == 3)
                {
                    this.rightDown = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldWorldPosition = [ this.worldPosition[0], this.worldPosition[1] ];
                }
            },
            "mousedown .world .tile": function(event) {
                if(event.which == 1)
                {
                    this.mouseDown = true;
                    this.setTile(event);
                    event.preventDefault();
                }
            },
            "mousedown .world .entity.selected": function(event) {
                if(event.which == 1) {
                    this.movingEntity = true;
                    this.movedEntity = $(event.currentTarget).data("model");
                    this.oldEntityPosition = [ this.movedEntity.get("x"), this.movedEntity.get("y") ];
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    event.preventDefault();
                }
            },
            "mousemove .world .tile": "setTile",
            "mousemove .world": function(event) {
                if(this.rightDown) {
                    var mousePosition = [ event.pageX, event.pageY ];
                    this.worldPosition = [
                        this.oldWorldPosition[0] + mousePosition[0] - this.oldMousePosition[0],
                        this.oldWorldPosition[1] + mousePosition[1] - this.oldMousePosition[1]
                    ];

                    this.$(".world").css({
                        top: this.worldPosition[1],
                        left: this.worldPosition[0]
                    });
                }
                else if(this.movingEntity) {
                    var offset = [
                        event.pageX - this.oldMousePosition[0],
                        event.pageY - this.oldMousePosition[1]
                    ];

                    this.movedEntity.set({
                        x: this.oldEntityPosition[0] + Math.round(offset[0] / 32),
                        y: this.oldEntityPosition[1] + Math.round(offset[1] / 32)
                    });
                }
            },

            "click .tabs .tab": "settingsChangeTab",
            "change input.showMoveMask": "toggleMoveMask",
            "change input.showAllLayers": "toggleAllLayers",
            "click input.upperLayer": "goUpperLayer",
            "click input.lowerLayer": "goLowerLayer",
            "click input.save": "saveMap",
            "click input.removeLayer": "removeActualLayer",
            "click input.createMapImage": "createMapImage",

            "click input.add_room": "addRoomModal",
            "click input.create_room": "createRoomModal",

            "click input.add_encounter": function() {
                this.model.get("monsters").add({
                    type: this.$(".encounter_type").val()
                });
            },

            "change input.encounter_chance": function(event) {
                this.model.set("encounter_ratio", parseInt($(event.currentTarget).val().replace("%", "")));
            },

            "change select.encounter_type": "updateEncounters"
        },

        initialize: function() {
            this.template = _.template( $("#mapEditTemplate").html() );

            this.showMoveMask = false;
            this.showLayer = 0;
            this.showAllLayers = false;
            this.worldPosition = [ 0, 0 ];

            //this.listenTo(this.model.get("tiles"), "update", this.reloadLayers);
            this.listenTo(this.model.get("tiles"), "reset", this.reloadLayers);
            this.listenTo(this.model.get("tiles"), "sync", this.updateStatus);
            this.listenTo(this.model.get("entities"), "sync", this.updateStatus);
            this.listenTo(this.model, "add:linked_scenes", this.addRoom);
            this.listenTo(this.model, "add:monsters", this.updateEncounters);
            this.listenTo(this.model, "sync", this.updateStatus);

            $(document).mouseup(function() {
                this.mouseDown = false;
                this.rightDown = false;
                this.movingEntity = false;
            }.bind(this));
        },

        render: function() {
            this.$el.html(this.template());
            this.worldPosition[0] = this.$(".settings").outerWidth();
            this.$(".world").css({
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32,
                left: this.worldPosition[0]
            });
            this.renderLayers();
            this.updateEncounters();

            if(this.model.get("linked_scenes").length > 0)
            {
                var $roomList = this.$(".roomList");
                this.$(".norooms").hide();
                $roomList.show();

                this.model.get("linked_scenes").each(function(room) {
                    $roomList.append(new Mapeditor.Views.RoomLine({ model: room }).render().$el);
                }, this);
            }
            else {
                this.$(".roomList").hide();
                this.$(".norooms").show();
            }

            // check for inconsistency across tiles
            if(this.model.get("tiles").length > this.model.get("sizex") * this.model.get("sizey") * this.maxZ) {
                if(confirm("Wygląda na to ze czesc tilesow jest poza mapa. Usunac je?")) {
                    this.model.get("tiles").destroy_outsides();
                }
            }

            return this;
        },

        reloadLayers: function() {
            this.layers = [];
            this.model.checkLayers();
            this.maxZ = parseInt(_.max(_.keys(this.model.tilesInLayers)));
            this.maxZ = isNaN(this.maxZ) ? 1 : this.maxZ + 1;

            for(var i = 0; i < this.maxZ; i++)
                this.ensureLayer(i);

            this.render();
        },

        settingsChangeTab: function(event) {
            var index = $(event.currentTarget).index();
            var $tab = $(this.$(".settings .content .tab").get(index));
            this.$(".tab.active").removeClass("active");
            $tab.addClass("active");
        },

        addRoom: function(room) {
            this.$(".roomList").append(new Mapeditor.Views.RoomLine({ model: room }).render().$el);
            this.$(".roomList").show();
            this.$(".norooms").hide();
        },

        toggleMoveMask: function() {
            this.showMoveMask = !this.showMoveMask;
            this.$(".world").toggleClass("showMoveMask", this.showMoveMask);
        },

        toggleAllLayers: function() {
            this.showAllLayers = !this.showAllLayers;
            this.renderLayers();
            _.each(this.layers, function(layer, index, list) {
                var state = "none";
                if(this.showAllLayers || this.showLayer == layer.layer)
                    state = "visible";
                else if(this.showLayer - 1 == layer.layer)
                    state = "halfVisible";

                layer.toggleLayer(state);
            }, this);
        },

        goUpperLayer: function() {
            if(this.showLayer < this.maxZ)
                this.showLayer++;

            this.changeLayer();
        },

        goLowerLayer: function() {
            if(this.showLayer > 0)
                this.showLayer--;

            this.changeLayer();
        },

        changeLayer: function() {
            this.ensureLayer(this.showLayer);
            this.$(".showLayer").text(this.showLayer);
            this.renderLayers();
            _.each(this.layers, function(layer, index, list) {
                var state = "none";
                if(this.showAllLayers || this.showLayer == layer.layer)
                    state = "visible";
                else if(this.showLayer - 1 == layer.layer)
                    state = "halfVisible";

                layer.toggleLayer(state);
            }, this);
        },

        ensureLayer: function(layer) {
            this.model.createLayer(layer);

            if(!this.layers[layer])
            {
                this.layers[layer] = new Mapeditor.Views.MapLayer({ model: this.model }, {
                    layer: layer,
                    state: this.showLayer == layer ? "visible" : (this.showLayer - 1 == layer ? "halfVisible" : "none")
                });
            }
        },

        renderLayers: function() {
            var $world = this.$(".world");
            _.each(this.layers, function(layer, index, list) {
                if(!layer.rendered)
                    $world.append(layer.render().$el);
            });
        },

        removeActualLayer: function() {
            if(this.showLayer > 0 && confirm("Jesteś pewien ze chcesz usunąć aktualną warstwe? Zostanie ona rowniez usunieta na serwerze. Tej operacji nie mozna cofnac."))
                this.removeLayer(this.showLayer);
        },

        removeLayer: function(layer) {
            if(layer > 0)
            {
                Mapeditor.setStatus("Usuwanie warstwy ...");
                this.model.removeLayer(layer);
                this.layers[layer].remove();
                this.layers.splice(layer, 1);

                // reorder layers
                _.each(this.layers, function(layer, index) {
                    layer.layer = index;
                    layer.updateZIndex();
                });

                this.maxZ = this.layers.length;

                if(this.showLayer == layer)
                {
                    this.showLayer = 0;
                    this.changeLayer();
                }

                Mapeditor.setStatus("Usuwanie warstwy ... Gotowe!");
            }
        },

        updateEncounters: function() {
            var $tbody = this.$("table.encounters tbody");

            $tbody.empty();

            this.model.get("monsters").each(function(monsterInMap) {
                if(monsterInMap.get("type") == this.$(".encounter_type").val())
                    $tbody.append(new Mapeditor.Views.MonsterInMap({ model: monsterInMap }).render().$el);
            }, this);

            this.$(".encounter_chance").val(this.model.get("encounter_ratio") + "%");
        },

        setTile: function(event) {
            if(this.mouseDown)
            {
                var tileModel = $(event.currentTarget).data("tileModel");
                if(!this.showMoveMask) {
                    var activeTileType = $("div.tile.active").data("tileType");

                    if( !tileModel || !activeTileType || activeTileType == tileModel.get("tileType"))
                        return;

                    tileModel.set({ tileType: activeTileType });
                }
                else {
                    var activeMoveMask = $("div.movemask.active").data("moveMask");

                    if( !tileModel || !activeMoveMask || activeMoveMask == tileModel.get("moveMask") )
                        return;

                    tileModel.set({ moveMask: activeMoveMask });
                }

                event.preventDefault();
            }
        },

        saveMap: function() {
            if(!Mapeditor.Variables.savingMap && !Mapeditor.Variables.generatingMapImage)
            {
                Mapeditor.Variables.savingMap = true;
                Mapeditor.setStatus("Zapisywanie mapy {0} ...".format(this.model.get("name")));
                /*this.model.get("tiles").clear({ success: function() {

                }.bind(this)});*/
                this.savedSize = 0;
                this.totalSaveSize = this.model.get("tiles").changedTiles().length + this.model.get("entities").length + 1;
                this.model.save();
                this.model.get("tiles").save();
                this.model.get("entities").save();
            }
        },

        updateStatus: function(collection, data, options) {
            if(Mapeditor.Variables.savingMap) {
                console.log("sync", collection instanceof Backbone.Collection, data);

                if(collection instanceof Backbone.Collection) this.savedSize += data.length;
                else this.savedSize++;

                if(this.savedSize / this.totalSaveSize >= 1.0)
                {
                    Mapeditor.Variables.savingMap = false;
                    Mapeditor.setStatus("Zapisywanie mapy {0} ... Gotowe!".format(this.model.get("name")));
                }
                else {
                    Mapeditor.setStatus("Zapisywanie mapy {0} ... {1}% ({2} / {3})".format(
                        this.model.get("name"),
                        Math.round(this.savedSize / this.totalSaveSize * 100),
                        this.savedSize,
                        this.totalSaveSize
                    ));
                }
            }
        },

        createMapImage: function() {
            if(!Mapeditor.Variables.savingMap && !Mapeditor.Variables.generatingMapImage) {
                Mapeditor.setStatus("Generowanie obrazu mapy ...");
                Mapeditor.Variables.generatingMapImage = true;
                $.post(this.model.url() + "/image/", function(data) {
                    Mapeditor.setStatus("Generowanie obrazu mapy ... Gotowe!");
                    Mapeditor.Variables.generatingMapImage = false;
                }, "json");
            }
        },

        createRoomModal: function() {
            Mapeditor.Variables.modalCreateRoomView.open(this.model);
        },

        addRoomModal: function() {
            Mapeditor.Variables.modalAddRoomView.open(this.model);
        }
    });
})();

(function() {
    Mapeditor.Views.Maps = Backbone.View.extend({
        tagName: "div",
        id: "maps",

        events: {
            "mousedown": function(event) {
                if(this.generalMapModel.get("adding")) {
                    this.adding = true;
                    this.startPosition = [ event.pageX, event.pageY ];

                    var offset = this.$el.offset();
                    this.$selectionEl = $(document.createElement("div"));
                    this.$selectionEl.addClass("selection");
                    this.$selectionEl.css({
                        top: this.startPosition[1] - offset.top,
                        left: this.startPosition[0] - offset.left
                    });
                    this.$selectionEl.html( this.mapSelectionTemplate() );
                    this.$el.append(this.$selectionEl);

                    event.preventDefault();
                }
            },
            "mousedown .map": function(event) {
                if(event.which == 1 && !this.generalMapModel.get("deleting"))
                {
                    this.map = $(event.currentTarget).data("map");
                    if(this.map.isNew())
                    {
                        this.map = null;
                        return;
                    }
                    this.moveMap = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldMapPosition = [ this.map.get("x"), this.map.get("y") ];
                    event.preventDefault();
                }
            },
            "click .map": function(event) {
                if(this.generalMapModel.get("deleting"))
                {
                    var map = $(event.currentTarget).data("map");
                    if(confirm("Jesteś pewien ze chcesz usunac {0}?".format(this.map.get("name")))) {
                        map.destroy();
                    }
                }
            },
            "mousedown .map div.resize": function(event) {
                if(event.which == 1) {
                    this.map = $(event.currentTarget).parent().data("map");
                    if(this.map.isNew())
                    {
                        this.map = null;
                        return;
                    }
                    this.resizing = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldMapSize = [ this.map.get("sizex"), this.map.get("sizey") ];
                    event.preventDefault();

                    // so we dont resize and move map at the same moment
                    event.stopPropagation();
                }
            },
            "mousemove": function(event) {
                var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");
                if(this.map) {
                    var offset = [
                        Math.floor((event.pageX - this.oldMousePosition[0]) / pixelsPerUnit),
                        Math.floor((event.pageY - this.oldMousePosition[1]) / pixelsPerUnit)
                    ];

                    this.map.validate();

                    if(this.moveMap) {
                        this.map.set({
                            x: this.oldMapPosition[0] + offset[0],
                            y: this.oldMapPosition[1] + offset[1]
                        });
                    }
                    else if(this.resizing) {
                        this.map.set({
                            sizex: this.oldMapSize[0] + offset[0],
                            sizey: this.oldMapSize[1] + offset[1]
                        });
                    }

                    event.preventDefault();
                }
                else if(this.adding) {
                    var elOffset = this.$el.offset(),
                        diff = [
                            event.pageX - this.startPosition[0],
                            event.pageY - this.startPosition[1]
                        ],
                        position = [
                            this.startPosition[0] - elOffset.left,
                            this.startPosition[1] - elOffset.top
                        ],
                        newPosition = [0, 0],
                        size = [0, 0];

                    for(var i = 0; i < 2; i++)
                    {
                        if(diff[i] < 0) {
                            newPosition[i] = Math.floor((position[i] + diff[i]) / pixelsPerUnit) * pixelsPerUnit;
                            size[i] = Math.floor(-diff[i] / pixelsPerUnit) * pixelsPerUnit;
                        }
                        else {
                            newPosition[i] = Math.floor(position[i] / pixelsPerUnit) * pixelsPerUnit;
                            size[i] = Math.floor(diff[i] / pixelsPerUnit) * pixelsPerUnit;
                        }
                    }

                    this.$selectionEl.css({
                        top: newPosition[1],
                        left: newPosition[0],
                        width: size[0],
                        height: size[1]
                    });

                    this.selectionPosition = [ newPosition[0] / pixelsPerUnit, newPosition[1] / pixelsPerUnit ];
                    this.selectionSize = [ size[0] / pixelsPerUnit, size[1] / pixelsPerUnit ];

                    this.$selectionEl.find(".coords").html("{0}, {1}".format(this.selectionPosition[0], this.selectionPosition[1]));

                    this.$selectionEl.find(".sizex").html(this.selectionSize[0]);
                    this.$selectionEl.find(".sizey").html(this.selectionSize[1]);
                }
            },
            "mouseup": function() {
                if(this.map) {
                    if(this.map.isValid())
                        this.map.save();
                    else {
                        if(this.moveMap) {
                            this.map.set({
                                x: this.oldMapPosition[0],
                                y: this.oldMapPosition[1]
                            });
                        }
                        else if(this.resizing) {
                            this.map.set({
                                sizex: this.oldMapSize[0],
                                sizey: this.oldMapSize[1]
                            });
                        }
                    }
                }
                else if(this.adding) {
                    this.$selectionEl.remove();
                    this.generalMapModel.set("adding", false);

                    var model = new Mapeditor.Models.Scene({
                        name: "",
                        key_name: "",
                        x: this.selectionPosition[0],
                        y: this.selectionPosition[1],
                        sizex: this.selectionSize[0],
                        sizey: this.selectionSize[1],
                        tiles: [],
                        entities: []
                    });

                    this.collection.add(model);

                    model.once("change:key_name", function() {
                        if(model.get("name") && model.get("key_name"))
                            model.save();
                    });
                }

                this.moveMap = false;
                this.resizing = false;
                this.map = false;
                this.adding = false;
            }
        },

        initialize: function(attrs, options) {
            this.generalMapModel = options.generalMapModel;

            this.mapSelectionTemplate = _.template( $("#mapSelectionTemplate").html() );

            //this.listenTo(this.collection, "update", this.render);
            this.listenTo(this.collection, "add", this.addMap);

            this.listenTo(this.generalMapModel, "change:offsetX", this.moveMe);
            this.listenTo(this.generalMapModel, "change:offsetY", this.moveMe);
            this.listenTo(this.generalMapModel, "change:adding", this.updateClasses);
            this.listenTo(this.generalMapModel, "change:deleting", this.updateClasses);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(model, index, collection) {
                this.addMap(model);
            }, this);

            this.moveMe();
            this.updateClasses();

            return this;
        },

        addMap: function(model) {
            if(model.get("type") == "global") {
                var view = new Mapeditor.Views.Map({ model: model }, { generalMapModel: this.generalMapModel });
                this.$el.append(view.render().$el);
            }
        },

        moveMe: function() {
            this.$el.css({
                width: this.generalMapModel.get("offsetX") + this.generalMapModel.get("width") + 1000,
                height: this.generalMapModel.get("offsetY") + this.generalMapModel.get("height") + 1000
            });

            this.$el.parent().scrollTop(-this.generalMapModel.get("offsetY")).scrollLeft(-this.generalMapModel.get("offsetX"));
        },

        updateClasses: function() {
            this.$el.toggleClass("adding", this.generalMapModel.get("adding"))
                    .toggleClass("deleting", this.generalMapModel.get("deleting"));
        }
    });
})();

(function() {
    Mapeditor.Views.ModalAddRoom = Backbone.View.extend({
        tagName: "div",
        id: "modalAddRoom",

        initialize: function() {
            this.template = _.template( $("#modalAddRoomTemplate").html() );
            this.actualMap = null;
            console.log("he");
        },

        updateRooms: function() {
            console.log("hi2");
            var $roomsList = this.$("#roomsList"),
                rooms = Mapeditor.Variables.mapsCollection;

            $roomsList.empty();
            $roomsList.append("<option value = '0'> --- </option>");
            rooms.each(function(room) {
                if(room.get("type") == "room" || room.get("type") == "cave") {
                    $roomsList.append("<option value = '{0}'>{1} ({2}) [{3}, {4}]</option>".format(
                        room.id,
                        room.get("name"),
                        room.get("key_name"),
                        room.get("sizex"),
                        room.get("sizey")
                    ));
                }
            }, this);
        },

        render: function() {
            this.$el.html(this.template());
            var $roomsList = this.$("#roomsList"),
                rooms = Mapeditor.Variables.mapsCollection,
                self = this;

            this.$el.dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true,
                width: 500,
                title: "Dodawanie pokoju",
                buttons: {
                    "Dodaj": function() {
                        var id = self.$("#roomsList option:selected").val();
                        if(id)
                        {
                            var room = Mapeditor.Variables.mapsCollection.get(id);
                            self.actualMap.get("linked_scenes").add(room);
                            self.actualMap.save();
                            Mapeditor.setStatus("Dodano pokoj {0} do mapy {1}!".format(room.get("name"), self.actualMap.get("name")));
                            self.actualMap = null;
                            $(this).dialog("close");
                        }
                    },

                    "Anuluj": function() {
                        $(this).dialog("close");
                    }
                }
            });
            return this;
        },

        open: function(map) {
            this.actualMap = map;
            this.updateRooms();
            this.$el.dialog("open");
        }
    });
})();

(function() {
    Mapeditor.Views.ModalCreateRoom = Backbone.View.extend({
        tagName: "div",
        id: "modalCreateRoom",

        initialize: function() {
            this.template = _.template( $("#modalCreateRoomTemplate").html() );
            this.actualMap = null;
        },

        render: function() {
            this.$el.html(this.template());

            this.$el.dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true,
                width: 500,
                title: "Tworzenie pokoju",
                buttons: {
                    "Utworz": function() {
                        var name = this.$(".input_name").val().trim(),
                            key_name = this.$(".input_keyname").val().trim(),
                            type = this.$(".input_type").val(),
                            sizex = parseInt(this.$(".input_sizex").val()),
                            sizey = parseInt(this.$(".input_sizey").val()),
                            $error = this.$("p.error");

                        if(!name) {
                            $error.text("Musisz podać nazwe pokoju!");
                            $error.show();
                            return;
                        }
                        else if(!key_name) {
                            $error.text("Musisz podać bazową nazwe pokoju!");
                            $error.show();
                            return;
                        }

                        var uniq = true;
                        Mapeditor.Variables.mapsCollection.each(function(room) {
                            if(room.get("key_name") == key_name)
                                uniq = false;
                        });

                        if(!uniq) {
                            $error.text("Nazwa bazowa musi być unikalna!");
                            $error.show();
                            return;
                        }

                        var room = new Mapeditor.Models.Scene({
                            name: name,
                            type: type,
                            key_name: key_name,
                            sizex: sizex,
                            sizey: sizey
                        });
                        Mapeditor.Variables.mapsCollection.add(room);

                        this.$(".saving").show();

                        room.once("sync", function() {
                            console.log(this);
                            this.actualMap.get("linked_scenes").add(room);
                            this.actualMap.save();
                            Mapeditor.setStatus("Utworzono pokoj {0}!".format(name));
                            this.$el.dialog("close");
                        }.bind(this));

                        room.save();
                    }.bind(this),

                    "Anuluj": function() {
                        $(this).dialog("close");
                    }
                }
            });

            return this;
        },

        open: function(map) {
            this.actualMap = map;
            this.$(".saving").hide();
            this.$el.dialog("open");
        }
    });
})();

(function() {
    Mapeditor.Views.MonsterInMap = Backbone.View.extend({
        tagName: "tr",

        events: {
            "change select.monster_type": function(event) {
                console.log("changed", this.model, $(event.currentTarget).val());
                this.model.set("monster", parseInt($(event.currentTarget).val()));
            },
            "change input.min_level": function(event) {
                this.model.set("min_level", parseInt($(event.currentTarget).val()));
            },
            "change input.max_level": function(event) {
                this.model.set("max_level", parseInt($(event.currentTarget).val()));
            },
            "change input.frequency": function(event) {
                this.model.set("frequency", parseInt($(event.currentTarget).val()));
            }
        },

        initialize: function() {
            this.template = _.template( $("#monsterInMapTemplate").html() );
            this.listenTo(this.model, "change:encounter_chance", this.updateEncounterChance);
        },

        render: function() {
            this.$el.html(this.template({ monsterInMap: this.model, monsterTypes: Mapeditor.Variables.monsterTypesCollection }));
            return this;
        },

        updateEncounterChance: function(model, value, options) {
            this.$(".encounter_chance").text(Math.round(value * model.get("scene").get("encounter_ratio") * 100) / 100 + "%");
        }
    });
})();

(function() {
    Mapeditor.Views.MoveMask = Backbone.View.extend({
        tagName: "div",
        className: "movemask block",
        model: Mapeditor.Models.MoveMask,

        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        render: function() {
            var color = this.model.get("color");
            this.$el.data("moveMask", this.model);
            this.$el.html(this.model.id);
            this.$el.css({
                backgroundColor: "rgba({0}, {1}, {2}, 0.6)".format(
                    parseInt(color.substring(1, 3), 16),
                    parseInt(color.substring(3, 5), 16),
                    parseInt(color.substring(5, 7), 16)
                )
            });

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.MoveMasks = Backbone.View.extend({
        el: "div#movemask_list",
        model: Mapeditor.Collections.MoveMasks,

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(element, index, list) {
                var view = new Mapeditor.Views.MoveMask({ model: element });
                this.$el.append(view.render().$el);
            }, this);
            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.RoomLine = Backbone.View.extend({
        tagName: "div",
        className: "room",

        events: {
            "click .edit": function() {
                Mapeditor.openMap(this.model);
            },
            "click .remove": function() {
                if(confirm("Jestes pewien ze chcesz usunac ten pokoj ? Tej operacji nie mozna cofnac.")) {
                    this.model.destroy();
                }
            },
            "click .removeLink": function() {
                this.parentMap.get("rooms").remove(this.model);
                this.parentMap.save();
                this.remove();
            }
        },

        initialize: function(options) {
            this.template = _.template( $("#roomLineTemplate").html() );
            this.parentMap = options.map;
            this.listenTo(this.model, "destroy", this.remove);
        },

        render: function() {
            this.$el.html( this.template({
                id: this.model.id,
                name: this.model.get("name"),
                sizex: this.model.get("sizex"),
                sizey: this.model.get("sizey")
            }) );
            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.Tab = Backbone.View.extend({
        tagName: "div",
        className: "tab",
        model: Mapeditor.Models.Window,

        events: {
            "click span.title": function() {
                this.model.collection.setActive(this.model);
            },
            "click span.close": function() {
                this.model.collection.remove(this.model);
            }
        },

        initialize: function() {
            this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:active", this.render);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.html("<span class = 'title'>{0}</span> <span class = 'close'>[X]</span>".format(this.model.get("title")));
            this.$el.toggleClass("active", this.model.get("active"));

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.Tabs = Backbone.View.extend({
        el: "div#tabs",
        model: Mapeditor.Collections.Windows,

        initialize: function() {
            this.listenTo(this.collection, "add", this.addTab);
        },

        render: function() {
            this.collection.forEach(function(element) {
                this.addTab(element);
            }, this);

            return this;
        },

        addTab: function(element, index, list) {
            var view = new Mapeditor.Views.Tab({ model: element });
            this.$el.append(view.$el);
            view.render();
        }
    });
})();

(function() {
    Mapeditor.Views.Tile = Backbone.View.extend({
        tagName: "div",
        className: "tile block",
        model: Mapeditor.Models.Tile,

        initialize: function() {
            this.listenTo(this.model, "change:tileType", this.setTileType);
            this.listenTo(this.model, "change:moveMask", this.setMoveMask);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.data("tileModel", this.model);
            this.$el.append("<div class = 'movemask block'></div>");

            this.setPosition();
            this.setTileType();
            this.setMoveMask();

            return this;
        },

        setPosition: function() {
            this.$el.css({
                top: (this.model.get("y") - this.model.get("scene").get("y")) * 32,
                left: (this.model.get("x") - this.model.get("scene").get("x")) * 32,
            });
        },

        setTileType: function() {
            var tileType = this.model.get("tileType");

            this.$el.css({
                backgroundPosition: tileType.get("x") + "px " + tileType.get("y") + "px",
                backgroundImage: "url('/static/{0}')".format(tileType.get("tileset").get("path"))
            });
        },

        setMoveMask: function() {
            var moveMask = this.model.get("moveMask");

            this.$(".moveMask").css({
                backgroundColor: "rgba({0}, {1}, {2}, 0.6)".format(
                    parseInt(moveMask.get("color").substring(1, 3), 16),
                    parseInt(moveMask.get("color").substring(3, 5), 16),
                    parseInt(moveMask.get("color").substring(5, 7), 16)
                )
            }).text(moveMask.get("id"));
        }
    });
})();

(function() {
    Mapeditor.Views.TileType = Backbone.View.extend({
        tagName: "div",
        className: "tile block",
        model: Mapeditor.Models.TileType,
        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        render: function() {
            this.$el.data("tileType", this.model);
            this.$el.css({
                backgroundPosition: this.model.get("x") + "px " + this.model.get("y") + "px",
                backgroundImage: "url('/static/{0}')".format(this.model.get("tileset").get("path"))
            });

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.TileTypes = Backbone.View.extend({
        el: "div#tiles",
        model: Mapeditor.Collections.TileTypes,

        events: {
            "change .tileset_select": "updateTiletypes"
        },

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            var $tileSetSelect = this.$(".tileset_select"),
                tileSets = Mapeditor.Variables.tileSetsCollection;

            tileSets.each(function(tileset) {
                $tileSetSelect.append("<option value='{0}'>{1}</option>".format(tileset.id, tileset.get("name")));
            }, this);

            this.updateTiletypes();
            return this;
        },

        updateTiletypes: function() {
            var $tileList = this.$("#tile_list"),
                selectedTileSetId = this.$(".tileset_select").val();

            $tileList.empty();
            this.collection.forEach(function(element, index, list) {
                if(element.get("tileset").id == selectedTileSetId) {
                    $tileList.append(new Mapeditor.Views.TileType({model: element}).render().$el);
                }
            }, this);
        }
    });
})();

(function() {
    Mapeditor.Views.TopMeasure = Backbone.View.extend({
        tagName: "div",
        id: "topMeasure",
        model: Mapeditor.Models.GeneralMap,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:offsetX", this.moveMe);
        },

        render: function() {
            this.$el.empty();
            this.$el.css({
                width: this.model.get("offsetX") + this.model.get("width") + 1000
            });

            this.maxI = this.model.get("width") / this.model.get("pixelsPerUnit");

            for(var i = 0; i < this.maxI; i++)
            {
                var view = new Mapeditor.Views.TopUnit({ model: this.model });
                this.$el.append(view.$el);
                view.render(i);
            }

            return this;
        },

        moveMe: function(model, value) {
            this.$el.css({
                width: this.model.get("offsetX") + this.model.get("width") + 1000
            });

            this.$el.parent().scrollLeft(-value);
        }
    });
})();

(function() {
    Mapeditor.Views.TopUnit = Backbone.View.extend({
        tagName: "div",
        className: "unit",

        render: function(i) {
            this.$el.css("left", i * this.model.get("pixelsPerUnit") * 5);
            if(i % 2 === 0) {
                this.$el.html("<span>{0}</span>".format(i * 5));
                this.$("span").css("left", -(('' + (i * 5)).length) * 4);
            }

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.Window = Backbone.View.extend({
        tagName: "div",
        className: "window",
        model: Mapeditor.Models.Window,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:active", this.updateActive);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.append(this.model.get("view").$el);
            this.$el.toggleClass("active", this.model.get("active"));
            this.model.get("view").render();
            return this;
        },

        updateActive: function() {
            this.$el.toggleClass("active", this.model.get("active"));

            return this;
        }
    });
})();

(function() {
    Mapeditor.Views.Windows = Backbone.View.extend({
        el: "div#windows",
        model: Mapeditor.Collections.Windows,

        initialize: function() {
            this.listenTo(this.collection, "add", this.addWindow);
        },

        render: function() {
            this.collection.forEach(function(element, index, list) {
                this.addWindow(element);
            }, this);

            return this;
        },

        addWindow: function(object, collection, options) {
            var view = new Mapeditor.Views.Window({ model: object });
            this.$el.append(view.render().$el);
        }
    });
})();
