(function() {
    Mapeditor.Models.EntityType = Backbone.RelationalModel.extend({
        defaults: {
            name: "",
            sprite: "",
            visible: false,
            sizex: 0,
            sizey: 0
        }
    });
})();
