(function() {
    Mapeditor.Models.Tile = Backbone.RelationalModel.extend({
        initialize: function() {
            this._toSave = false;
            this.listenTo(this, "change", this.setChanged);
        },
        setChanged: function() {
            this._toSave = true;
        },
        relations: [
            {
                type: Backbone.HasOne,
                keySource: "tile_type",
                key: "tileType",
                relatedModel: "Mapeditor.Models.TileType",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                keySource: "move_mask",
                key: "moveMask",
                relatedModel: "Mapeditor.Models.MoveMask",
                includeInJSON: "id"
            }
        ]
    });
})();
