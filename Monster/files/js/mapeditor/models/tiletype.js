(function() {
    Mapeditor.Models.TileType = Backbone.RelationalModel.extend({
        defaults: {
            id: 0,
            key_name: null,
            x: null,
            y: null
        },
        relations: [{
            type: Backbone.HasOne,
            key: "tileset",
            relatedModel: "Mapeditor.Models.TileSet",
            includeInJSON: "id",
            reverseRelation: {
                key: "tile_types",
                includeInJSON: false
            }
        }]
    });
})();
