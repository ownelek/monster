(function() {
    Mapeditor.Models.Window = Backbone.Model.extend({
        defaults: {
            title: null,
            active: false,
            view: null
        }
    });
})();
