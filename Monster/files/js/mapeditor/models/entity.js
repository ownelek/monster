(function() {
    Mapeditor.Models.Entity = Backbone.RelationalModel.extend({
        getProperty: function(key) {
            return _.find(this.get("properties"), function(prop) {
                return prop.key == key;
            });
        },

        setProperty: function(key, value) {
            var prop = this.getProperty(key);
            prop.value = value;
        },
        
        relations: [
            {
                type: Backbone.HasOne,
                key: "type",
                relatedModel: "Mapeditor.Models.EntityType",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                key: "scene",
                relatedModel: "Mapeditor.Models.Scene",
                includeInJSON: "id",
                reverseRelation: {
                    key: "entities",
                    collectionType: "Mapeditor.Collections.Entities",
                    collectionOptions: function(scene) {
                        return {
                            parentScene: scene
                        };
                    }
                }
            }
        ]
    });
})();
