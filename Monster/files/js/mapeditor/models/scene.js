(function() {
    Mapeditor.Models.Scene = Backbone.RelationalModel.extend({
        defaults: {
            isSaving: false,
            isInvalid: false,
            type: "global",
            x: 0,
            y: 0
        },

        initialize: function() {
            this.listenTo(this.get("tiles"), "update", this.updatePositionMap);
            this.positionMap = [];
            this.tilesInLayers = {};
        },

        checkLayers: function() {
            this.tilesInLayers = this.get("tiles").countBy(function(value){
                return value.get("z");
            });
        },

        createLayer: function(layer) {
            if(this.tilesInLayers[layer] >= this.get("sizex") * this.get("sizey"))
                return false;

            var tiles = this.get("tiles").toJSON(),
                tilesInLayer = _.where(tiles, { z: layer }),
                tileType = Mapeditor.Variables.tileTypesCollection.get(1),
                moveMask = Mapeditor.Variables.moveMasksCollection.get(1);

            _.each(_.range(this.get("x"), this.get("x") + this.get("sizex")), function(x) {
                _.each(_.range(this.get("y"), this.get("y") + this.get("sizey")), function(y) {
                    if(!_.findWhere(tilesInLayer, { x: x, y: y })) {
                        var tile = new Mapeditor.Models.Tile({
                            scene: this,
                            x: x,
                            y: y,
                            z: layer,
                            tileType: tileType,
                            moveMask: moveMask
                        }, { silent: true });
                    }
                }, this);
            }, this);

            this.checkLayers();
            return true;
        },

        removeLayer: function(layer, callback) {
            $.post(this.url() + "/removeLayer", { layer: layer }, function() {
                this.get("tiles").remove(this.get("tiles").filter(function(tile) { return tile.get("z") == layer; }));
                delete this.tilesInLayers[layer];

                _.each(this.get("tiles").filter(function(tile) {
                    return tile.get("z") > layer;
                }), function(tile) {
                    tile.set({
                        z: tile.get("z") - 1
                    });
                });

                _.each(this.tilesInLayers, function(num, index) {
                    if(index > layer)
                    {
                        this.tilesInLayers[parseInt(index) - 1] = this.tilesInLayers[index];
                        delete this.tilesInLayers[index];
                    }
                }, this);

                callback.call(this);
            }.bind(this));
        },

        updatePositionMap: function(collection, options) {
            collection.each(function(tile) {
                this.positionMap[this.get("sizex") * this.get("sizey") * tile.get("z") + this.get("sizex") * tile.get("y") + tile.get("x")] = tile;
            }, this);
        },

        validate: function() {
            var ret = this.collection.find(function(map) {
                return this != map && ( (( this.get("x") > map.get("x") && this.get("x") < map.get("x") + map.get("sizex") ) ||
                    ( map.get("x") > this.get("x") && map.get("x") < this.get("x") + this.get("sizex") )) &&
                    (( this.get("y") > map.get("y") && this.get("y") < map.get("y") + map.get("sizey") )  ||
                    ( map.get("y") > this.get("y") && map.get("y") < this.get("y") + this.get("sizey") )) );
            }, this);

            var isValid = !ret;
            if(this.lastValidation != isValid)
            {
                this.lastValidation = isValid;
                this.trigger("validate", this, isValid);
            }

            console.log("validate", isValid);

            if(!isValid)
                return "map collides with another map";
        },

        relations: [
            {
                type: Backbone.HasMany,
                key: "tiles",
                relatedModel: "Mapeditor.Models.Tile",
                collectionType: "Mapeditor.Collections.Tiles",
                collectionKey: false,
                collectionOptions: function(model) {
                    return { parent: model };
                },
                includeInJSON: false,
                reverseRelation: {
                    key: "scene",
                    includeInJSON: "id"
                }
            },
            {
                type: Backbone.HasMany,
                key: "linked_scenes",
                relatedModel: "Mapeditor.Models.Scene",
                collectionType: "Mapeditor.Collections.Scenes",
                collectionKey: false,
                includeInJSON: "id"
            },
            {
                type: Backbone.HasMany,
                key: "monsters",
                relatedModel: "Mapeditor.Models.MonsterInMap",
                collectionType: "Mapeditor.Collections.MonstersInMap",
                reverseRelation: {
                    key: "scene"
                }
            }
        ]
    });
})();
