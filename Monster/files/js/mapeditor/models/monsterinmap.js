(function() {
    Mapeditor.Models.MonsterInMap = Backbone.RelationalModel.extend({
        defaults: {
            monster: null,
            type: "grass",
            min_level: 1,
            max_level: 1,
            frequency: 0
        },
        getEncounterChance: function() {
            var max = 0;
            this.collection.each(function(monsterInMap) {
                max += monsterInMap.get("frequency");
            });
            
            return this.get("frequency") / max;
        },
        relations: [{
            type: Backbone.HasOne,
            key: "monster",
            relatedModel: "Mapeditor.Models.MonsterType",
            includeInJSON: "id"
        }]
    });
})();
