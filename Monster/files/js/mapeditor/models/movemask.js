(function() {
    Mapeditor.Models.MoveMask = Backbone.RelationalModel.extend({
        defaults: {
            id: 0,
            name: null,
            color: null
        }
    });
})();
