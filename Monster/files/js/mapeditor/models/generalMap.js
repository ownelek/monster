(function() {
    Mapeditor.Models.GeneralMap = Backbone.Model.extend({
        defaults: {
            offsetX: 0,
            offsetY: 0,
            width: 0,
            height: 0,
            adding: false,
            deleting: false,
            pixelsPerUnit: 5
        }
    });
})();
