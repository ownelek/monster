(function() {
    // Setup CSRF Token header
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    Backbone.$.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
            }
        }
    });

    window.Mapeditor = {
        Models: [],
        Collections: [],
        Views: [],
        Variables: {},

        start: function() {
            // First collections
            Mapeditor.Variables.urlsModel = new Mapeditor.Models.Urls();
            Mapeditor.Variables.urlsModel.fetch({
                success: success
            });

            // run fetch after successfully loading urls from server
            function success() {
                Mapeditor.Variables.tileSetsCollection.fetch();
                Mapeditor.Variables.tileTypesCollection.fetch();
                Mapeditor.Variables.moveMasksCollection.fetch();
                Mapeditor.Variables.mapsCollection.fetch();
                Mapeditor.Variables.entityTypesCollection.fetch();
                Mapeditor.Variables.monsterTypesCollection.fetch();
            }

            Mapeditor.Variables = _.extend(Mapeditor.Variables, {
                windowsCollection: new Mapeditor.Collections.Windows(),
                tileSetsCollection: new Mapeditor.Collections.TileSets(),
                tileTypesCollection: new Mapeditor.Collections.TileTypes(),
                moveMasksCollection: new Mapeditor.Collections.MoveMasks(),
                mapsCollection: new Mapeditor.Collections.Scenes(),
                entityTypesCollection: new Mapeditor.Collections.EntityTypes(),
                monsterTypesCollection: new Mapeditor.Collections.MonsterTypes()
            });

            // Views later
            Mapeditor.Variables = _.extend(Mapeditor.Variables, {
                tabsView: new Mapeditor.Views.Tabs({ collection: Mapeditor.Variables.windowsCollection }),
                windowsView: new Mapeditor.Views.Windows({ collection: Mapeditor.Variables.windowsCollection }),
                tileTypesView: new Mapeditor.Views.TileTypes({ collection: Mapeditor.Variables.tileTypesCollection }),
                moveMasksView: new Mapeditor.Views.MoveMasks({ collection: Mapeditor.Variables.moveMasksCollection }),
                entityTypesView: new Mapeditor.Views.EntityTypes({ collection: Mapeditor.Variables.entityTypesCollection }),
                entityPropertiesView: new Mapeditor.Views.EntityProperties().render(),
                generalMapModel: new Mapeditor.Models.GeneralMap({
                    width: window.innerWidth,
                    height: window.innerHeight
                }),
                modalAddRoomView: new Mapeditor.Views.ModalAddRoom(),
                modalCreateRoomView: new Mapeditor.Views.ModalCreateRoom(),
                savingMap: false,
                generatingMapImage: false
            });

            Mapeditor.Variables.modalAddRoomView.render();
            Mapeditor.Variables.modalCreateRoomView.render();
            Mapeditor.Variables.tabsView.render();
            Mapeditor.Variables.windowsView.render();

            $(document).on("click", ".showButton", function(event) {
                var $this = $(this),
                    activeButton = $(".showButton.active");
                    sideShow = $("#" + $this.data("for"));

                if(activeButton.length > 0 && !$this.hasClass("active"))
                {
                    activeButton.removeClass("active");
                    $(".sideShow.active").removeClass("active");
                }

                sideShow.toggleClass("active");
                $this.toggleClass("active");
            });

            $("div.sideShow").css({
                height: window.innerHeight
            });
        },

        openGeneralMap: function() {
            var map = new Mapeditor.Models.Window({ title: "General Map", view: new Mapeditor.Views.GeneralMap({ model: Mapeditor.Variables.generalMapModel }) });
            Mapeditor.Variables.windowsCollection.add(map);
            Mapeditor.Variables.windowsCollection.setActive(map);
        },

        openMap: function(map) {
            Mapeditor.setStatus("Wczytywanie {0} ...".format(map.get("name")));
            console.log(map);
            map.get("tiles").fetch({
                reset: true,
                success: function() {
                    map.get("entities").fetch({
                        success: function() {
                            Mapeditor.setStatus("Wczytywanie {0} ... Gotowe!".format(map.get("name")));
                        }
                    });
                }
            });


            var mapViewer = new Mapeditor.Views.MapViewer({ model: map }),
                mapWindow = new Mapeditor.Models.Window({
                    title: map.get("name"),
                    view: mapViewer
            });

            Mapeditor.Variables.windowsCollection.add(mapWindow);
            Mapeditor.Variables.windowsCollection.setActive(mapWindow);
        },

        setStatus: function(text) {
            $("#status").text(text);
        }
    };
})();
