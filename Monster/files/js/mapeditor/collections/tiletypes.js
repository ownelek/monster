(function() {
    Mapeditor.Collections.TileTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.TileType,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("tiletypes");
        }
    });
})();
