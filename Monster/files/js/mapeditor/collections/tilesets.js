(function() {
    Mapeditor.Collections.TileSets = Backbone.Collection.extend({
        model: Mapeditor.Models.TileSet,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("tilesets");
        }
    });
})();
