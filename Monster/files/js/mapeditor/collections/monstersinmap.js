(function() {
    Mapeditor.Collections.MonstersInMap = Backbone.Collection.extend({
        model: Mapeditor.Models.MonsterInMap,

        initialize: function() {
            this.listenTo(this, "add", this.addListener);
            this.listenTo(this, "remove", this.removeListener);
        },

        addListener: function(model, collection, options) {
            this.listenTo(model, "change:frequency", this.updateEncounterChance);
        },

        removeListener: function(model, collection, options) {
            this.stopListening(model);
        },

        updateEncounterChance: function(model, value, options) {
            this.each(function(monsterInMap) {
                monsterInMap.trigger("change:encounter_chance", monsterInMap, monsterInMap.getEncounterChance(), options);
            });
        }
    });
})();
