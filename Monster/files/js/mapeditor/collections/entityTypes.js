(function() {
    Mapeditor.Collections.EntityTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.EntityType,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("entitytypes");
        }
    });
})();
