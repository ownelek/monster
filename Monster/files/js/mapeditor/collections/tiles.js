(function() {
    Mapeditor.Collections.Tiles = Backbone.Collection.extend({
        model: Mapeditor.Models.Tile,

        initialize: function(models, options) {
            this.parent = options.parent;
        },

        url: function() {
            return Mapeditor.Variables.urlsModel.get("maps") + this.parent.id + "/tiles/";
        },

        // send clear request, only to server
        clear: function(options) {
            var self = this;
            $.post(this.parent.url() + "/tiles/clear/", function(data) {
                if(options.success) options.success.call(self, this, data);
            }, "json");
        },

        changedTiles: function() {
            return this.filter(function(tile) {
                return tile._toSave || tile.isNew();
            }, this);
        },

        save: function(options) {
            var requestsNum = 0,
                maxRequests = 6,
                tilesPerRequest = 50,
                queue = [],
                tiles = new Mapeditor.Collections.Tiles(this.filter(function(tile) {
                    var r = tile._toSave;
                    tile._toSave = false;
                    return r || tile.isNew();
                }), { parent: this.parent }).toJSON(),
                savedTiles = 0,
                maxTiles = tiles.length;

            console.log(maxTiles, tiles);
            function queueFunction() {
                //console.log(this.tiles);
                var xhr = $.ajax({
                    url: this.url,
                    type: "POST",
                    data: JSON.stringify(this.tiles),
                    dataType: "json",
                    contentType: "application/json"
                }).then(function(data, statusText, jqXHR) {
                    savedTiles += data.length;
                    if(data.length > 0)
                    {
                        _.each(data, function(new_tile) {
                            var tileModel = this.map.positionMap[
                                this.map.get("sizex") * this.map.get("sizey") * new_tile.z +
                                this.map.get("sizex") * new_tile.y +
                                new_tile.x
                            ];

                            if(tileModel) {
                                tileModel.set(new_tile);
                            }
                        }, this);
                    }

                    requestsNum--;
                    fireQueue();
                    this.tilesCollection.trigger("sync", this.tilesCollection, data, xhr, options);
                    //this.tilesCollection.trigger("save", this.tilesCollection, data, savedTiles, maxTiles);
                }.bind(this));

                this.tilesCollection.trigger("request", this.tilesCollection, xhr);
            }

            function fireQueue()
            {
                while(requestsNum < maxRequests && queue.length > 0)
                {
                    setTimeout(queue.shift(), 0);
                    requestsNum++;
                }
            }

            while(tiles.length > 0)
            {
                var context = {
                    tiles: _.first(tiles, tilesPerRequest),
                    tilesCollection: this,
                    map: this.parent,
                    url: this.url()
                };

                tiles = _.rest(tiles, tilesPerRequest);
                queue.push(queueFunction.bind(context));
            }

            fireQueue();
        },

        destroy_outsides: function(options) {
            options = options || {};

            function success(data, status, xhr) {
                if(options.success) options.success.call(this, data, status, xhr);

                this.each(function(tile) {
                    if(!tile)
                        return;

                    var map = tile.get("scene");

                    if( tile.get("x") < map.get("x") || tile.get("x") >= map.get("x") + map.get("sizex") ||
                            tile.get("y") < map.get("y") || tile.get("y") >= map.get("y") + map.get("sizey"))
                        this.remove(tile);
                }, this);
            }

            $.ajax({
                method: "DELETE",
                url: _.result(this, "url") + "clear_outside/",
                success: success.bind(this)
            });
        }
    });
})();
