(function() {
    Mapeditor.Collections.Entities = Backbone.Collection.extend({
        model: Mapeditor.Models.Entity,
        initialize: function(models, options) {
            if(options && options.parentScene) {
                this.parentScene = options.parentScene;
            }
        },
        url: function() {
            if(this.parentScene) {
                return "{0}/entities/".format(_.result(this.parentScene, "url"));
            }

            return Mapeditor.Variables.urlsModel.get("entities");
        },

        save: function(options) {
            if(!this.parentScene)
                return;

            var xhr = $.ajax({
                url: _.result(this, "url"),
                type: "POST",
                data: JSON.stringify(this.toJSON()),
                dataType: "json",
                contentType: "application/json"
            });

            xhr.done(function(data) {
                this.set(data);
                this.trigger("sync", this, data, options);
            }.bind(this));

            this.trigger("request", this, xhr, options);
        }
    });
})();
