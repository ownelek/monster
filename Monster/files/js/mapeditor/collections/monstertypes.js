(function() {
    Mapeditor.Collections.MonsterTypes = Backbone.Collection.extend({
        model: Mapeditor.Models.MonsterType,
        url: "/api/monstertypes/"
    });
})();
