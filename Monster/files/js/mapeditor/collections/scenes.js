(function() {
    Mapeditor.Collections.Scenes = Backbone.Collection.extend({
        model: Mapeditor.Models.Scene,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("maps");
        }
    });
})();
