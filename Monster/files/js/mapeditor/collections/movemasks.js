(function() {
    Mapeditor.Collections.MoveMasks = Backbone.Collection.extend({
        model: Mapeditor.Models.MoveMask,
        url: function() {
            return Mapeditor.Variables.urlsModel.get("movemasks");
        }
    });
})();
