(function() {
    Mapeditor.Collections.Windows = Backbone.Collection.extend({
        model: Mapeditor.Models.Window,

        setActive: function(windowObj) {
            this.forEach(function(element, index, list) {
                if(element == windowObj)
                    element.set({active: true});
                else
                    element.set({active: false});
            });
        }
    });
})();
