(function() {
    Mapeditor.Views.Tabs = Backbone.View.extend({
        el: "div#tabs",
        model: Mapeditor.Collections.Windows,

        initialize: function() {
            this.listenTo(this.collection, "add", this.addTab);
        },

        render: function() {
            this.collection.forEach(function(element) {
                this.addTab(element);
            }, this);

            return this;
        },

        addTab: function(element, index, list) {
            var view = new Mapeditor.Views.Tab({ model: element });
            this.$el.append(view.$el);
            view.render();
        }
    });
})();
