(function() {
    Mapeditor.Views.LeftMeasure = Backbone.View.extend({
        tagName: "div",
        id: "leftMeasure",
        model: Mapeditor.Models.GeneralMap,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:offsetY", this.moveMe);
        },

        render: function() {
            this.$el.empty();
            this.$el.css({
                height: this.model.get("offsetY") + this.model.get("height") + 1000
            });

            this.maxI = this.model.get("height") / this.model.get("pixelsPerUnit");
            for(var i = 0; i < this.maxI; i++)
            {
                var view = new Mapeditor.Views.LeftUnit({ model: this.model });
                this.$el.append(view.$el);
                view.render(i);
            }

            return this;
        },

        moveMe: function(model, value, options) {
            this.$el.css({
                height: this.model.get("offsetY") + this.model.get("height") + 1000
            });

            this.$el.parent().scrollTop(-value);
        }
    });
})();
