(function() {
    Mapeditor.Views.Windows = Backbone.View.extend({
        el: "div#windows",
        model: Mapeditor.Collections.Windows,

        initialize: function() {
            this.listenTo(this.collection, "add", this.addWindow);
        },

        render: function() {
            this.collection.forEach(function(element, index, list) {
                this.addWindow(element);
            }, this);

            return this;
        },

        addWindow: function(object, collection, options) {
            var view = new Mapeditor.Views.Window({ model: object });
            this.$el.append(view.render().$el);
        }
    });
})();
