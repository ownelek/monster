(function() {
    Mapeditor.Views.Entity = Backbone.View.extend({
        tagName: "div",
        className: "entity",

        initialize: function() {
            this.listenTo(this.model, "change:x", this.render);
            this.listenTo(this.model, "change:y", this.render);
            this.listenTo(this.model, "change:face", this.changeFace);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.css({
                top: this.model.get("y") * 32,
                left: this.model.get("x") * 32,
                width: this.model.get("type").get("sizex") * 32,
                height: this.model.get("type").get("sizey") * 32,
                backgroundImage: "url(/static/{0})".format(this.model.get("type").get("sprite"))
            });
            this.$el.toggleClass(this.model.get("type").get("name"), true);
            this.$el.data("model", this.model);

            this.changeFace();
            return this;
        },

        changeFace: function() {
            this.$el.removeClass(this.model.previous("face"));
            this.$el.addClass(this.model.get("face"));
        }
    });
})();
