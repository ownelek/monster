(function() {
    Mapeditor.Views.GeneralMap = Backbone.View.extend({
        tagName: "div",
        id: "generalMap",
        model: Mapeditor.Models.GeneralMap,

        events: {
            "click button.addMap": function(event) {
                this.model.set("adding", !this.model.get("adding"));
            },
            "click button.removeMap": function(event) {
                this.model.set("deleting", !this.model.get("deleting"));
            },
            "mousedown": function(event) {
                if(event.which == 3)
                {
                    this.moving = true;
                    this.mousePosition = [ event.pageX, event.pageY ];
                    this.oldOffset = [ this.model.get("offsetX"), this.model.get("offsetY") ];
                }
            },
            "mousemove": function(event) {
                if(this.moving)
                {
                    this.model.set({
                        offsetX: Math.min(0, this.oldOffset[0] + event.pageX - this.mousePosition[0]),
                        offsetY: Math.min(0, this.oldOffset[1] + event.pageY - this.mousePosition[1])
                    });
                }
            },
            "mouseup": function(event) {
                this.moving = false;
            }
        },

        initialize: function() {
            this.template = _.template( $("#generalMapTemplate").html() );
        },

        render: function() {
            this.$el.html(this.template());
            this.$("#topMeasureView").append(new Mapeditor.Views.TopMeasure({ model: this.model }).render().$el);
            this.$("#leftMeasureView").append(new Mapeditor.Views.LeftMeasure({ model: this.model }).render().$el);
            this.$("#mapsView").append(new Mapeditor.Views.Maps(
                { collection: Mapeditor.Variables.mapsCollection },
                { generalMapModel: this.model }
            ).render().$el);

            return this;
        }
    });
})();
