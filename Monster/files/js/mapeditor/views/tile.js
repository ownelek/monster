(function() {
    Mapeditor.Views.Tile = Backbone.View.extend({
        tagName: "div",
        className: "tile block",
        model: Mapeditor.Models.Tile,

        initialize: function() {
            this.listenTo(this.model, "change:tileType", this.setTileType);
            this.listenTo(this.model, "change:moveMask", this.setMoveMask);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.data("tileModel", this.model);
            this.$el.append("<div class = 'movemask block'></div>");

            this.setPosition();
            this.setTileType();
            this.setMoveMask();

            return this;
        },

        setPosition: function() {
            this.$el.css({
                top: (this.model.get("y") - this.model.get("scene").get("y")) * 32,
                left: (this.model.get("x") - this.model.get("scene").get("x")) * 32,
            });
        },

        setTileType: function() {
            var tileType = this.model.get("tileType");

            this.$el.css({
                backgroundPosition: tileType.get("x") + "px " + tileType.get("y") + "px",
                backgroundImage: "url('/static/{0}')".format(tileType.get("tileset").get("path"))
            });
        },

        setMoveMask: function() {
            var moveMask = this.model.get("moveMask");

            this.$(".moveMask").css({
                backgroundColor: "rgba({0}, {1}, {2}, 0.6)".format(
                    parseInt(moveMask.get("color").substring(1, 3), 16),
                    parseInt(moveMask.get("color").substring(3, 5), 16),
                    parseInt(moveMask.get("color").substring(5, 7), 16)
                )
            }).text(moveMask.get("id"));
        }
    });
})();
