(function() {
    Mapeditor.Views.MonsterInMap = Backbone.View.extend({
        tagName: "tr",

        events: {
            "change select.monster_type": function(event) {
                console.log("changed", this.model, $(event.currentTarget).val());
                this.model.set("monster", parseInt($(event.currentTarget).val()));
            },
            "change input.min_level": function(event) {
                this.model.set("min_level", parseInt($(event.currentTarget).val()));
            },
            "change input.max_level": function(event) {
                this.model.set("max_level", parseInt($(event.currentTarget).val()));
            },
            "change input.frequency": function(event) {
                this.model.set("frequency", parseInt($(event.currentTarget).val()));
            }
        },

        initialize: function() {
            this.template = _.template( $("#monsterInMapTemplate").html() );
            this.listenTo(this.model, "change:encounter_chance", this.updateEncounterChance);
        },

        render: function() {
            this.$el.html(this.template({ monsterInMap: this.model, monsterTypes: Mapeditor.Variables.monsterTypesCollection }));
            return this;
        },

        updateEncounterChance: function(model, value, options) {
            this.$(".encounter_chance").text(Math.round(value * model.get("scene").get("encounter_ratio") * 100) / 100 + "%");
        }
    });
})();
