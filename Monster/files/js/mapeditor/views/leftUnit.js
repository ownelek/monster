(function() {
    Mapeditor.Views.LeftUnit = Backbone.View.extend({
        tagName: "div",
        className: "unit",

        render: function(i) {
            this.$el.css("top", i * this.model.get("pixelsPerUnit") * 5);
            if(i % 2 === 0) {
                this.$el.html("<span>{0}</span>".format(i * 5));
                this.$("span").css("left", -(('' + (i * 5)).length) * 8 - 2);
            }

            return this;
        }
    });
})();
