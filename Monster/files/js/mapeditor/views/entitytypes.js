(function() {
    Mapeditor.Views.EntityTypes = Backbone.View.extend({
        el: "div#entityTypesList",

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(element, index, list) {
                var view = new Mapeditor.Views.EntityType({ model: element });
                this.$el.append(view.render().$el);
            }, this);
            return this;
        }
    });
})();
