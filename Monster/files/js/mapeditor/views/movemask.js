(function() {
    Mapeditor.Views.MoveMask = Backbone.View.extend({
        tagName: "div",
        className: "movemask block",
        model: Mapeditor.Models.MoveMask,

        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        render: function() {
            var color = this.model.get("color");
            this.$el.data("moveMask", this.model);
            this.$el.html(this.model.id);
            this.$el.css({
                backgroundColor: "rgba({0}, {1}, {2}, 0.6)".format(
                    parseInt(color.substring(1, 3), 16),
                    parseInt(color.substring(3, 5), 16),
                    parseInt(color.substring(5, 7), 16)
                )
            });

            return this;
        }
    });
})();
