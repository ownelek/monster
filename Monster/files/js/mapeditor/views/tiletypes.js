(function() {
    Mapeditor.Views.TileTypes = Backbone.View.extend({
        el: "div#tiles",
        model: Mapeditor.Collections.TileTypes,

        events: {
            "change .tileset_select": "updateTiletypes"
        },

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            var $tileSetSelect = this.$(".tileset_select"),
                tileSets = Mapeditor.Variables.tileSetsCollection;

            tileSets.each(function(tileset) {
                $tileSetSelect.append("<option value='{0}'>{1}</option>".format(tileset.id, tileset.get("name")));
            }, this);

            this.updateTiletypes();
            return this;
        },

        updateTiletypes: function() {
            var $tileList = this.$("#tile_list"),
                selectedTileSetId = this.$(".tileset_select").val();

            $tileList.empty();
            this.collection.forEach(function(element, index, list) {
                if(element.get("tileset").id == selectedTileSetId) {
                    $tileList.append(new Mapeditor.Views.TileType({model: element}).render().$el);
                }
            }, this);
        }
    });
})();
