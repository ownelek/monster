(function() {
    Mapeditor.Views.MoveMasks = Backbone.View.extend({
        el: "div#movemask_list",
        model: Mapeditor.Collections.MoveMasks,

        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(element, index, list) {
                var view = new Mapeditor.Views.MoveMask({ model: element });
                this.$el.append(view.render().$el);
            }, this);
            return this;
        }
    });
})();
