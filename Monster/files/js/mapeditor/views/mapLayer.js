(function() {
    Mapeditor.Views.MapLayer = Backbone.View.extend({
        tagName: "div",
        className: "layer",
        model: Mapeditor.Collections.Tiles,

        initialize: function(attrs, options) {
            this.layer = options.layer;
            this.state = options.state;
            this.rendered = false;

            this.listenTo(this.model, "add:entities", this.addEntity);
        },

        render: function() {
            this.$el.empty();
            this.model.get("tiles").forEach(function(model, index, collection) {
                if(model.get("z") == this.layer)
                    this.$el.append(new Mapeditor.Views.Tile({ model: model }).render().$el);
            }, this);

            this.model.get("entities").each(function(entity) {
                if(entity.get("z") == this.layer)
                    this.$el.append(new Mapeditor.Views.Entity({ model: entity }).render().$el);
            }, this);

            this.toggleLayer(this.state);
            this.rendered = true;
            return this;
        },

        updateZIndex: function() {
            this.$el.css({
                zIndex: this.layer + 1
            });
        },

        toggleLayer: function(state) {
            this.$el.toggleClass("visible", state == "visible" || state == "halfVisible");
            this.$el.toggleClass("half", state == "halfVisible");
        },

        addEntity: function(entity) {
            console.log("added");
            if(entity.get("z") == this.layer)
                this.$el.append(new Mapeditor.Views.Entity({ model: entity }).render().$el);
        }
    });
})();
