(function() {
    Mapeditor.Views.TopMeasure = Backbone.View.extend({
        tagName: "div",
        id: "topMeasure",
        model: Mapeditor.Models.GeneralMap,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:offsetX", this.moveMe);
        },

        render: function() {
            this.$el.empty();
            this.$el.css({
                width: this.model.get("offsetX") + this.model.get("width") + 1000
            });

            this.maxI = this.model.get("width") / this.model.get("pixelsPerUnit");

            for(var i = 0; i < this.maxI; i++)
            {
                var view = new Mapeditor.Views.TopUnit({ model: this.model });
                this.$el.append(view.$el);
                view.render(i);
            }

            return this;
        },

        moveMe: function(model, value) {
            this.$el.css({
                width: this.model.get("offsetX") + this.model.get("width") + 1000
            });

            this.$el.parent().scrollLeft(-value);
        }
    });
})();
