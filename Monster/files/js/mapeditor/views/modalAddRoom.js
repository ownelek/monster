(function() {
    Mapeditor.Views.ModalAddRoom = Backbone.View.extend({
        tagName: "div",
        id: "modalAddRoom",

        initialize: function() {
            this.template = _.template( $("#modalAddRoomTemplate").html() );
            this.actualMap = null;
            console.log("he");
        },

        updateRooms: function() {
            console.log("hi2");
            var $roomsList = this.$("#roomsList"),
                rooms = Mapeditor.Variables.mapsCollection;

            $roomsList.empty();
            $roomsList.append("<option value = '0'> --- </option>");
            rooms.each(function(room) {
                if(room.get("type") == "room" || room.get("type") == "cave") {
                    $roomsList.append("<option value = '{0}'>{1} ({2}) [{3}, {4}]</option>".format(
                        room.id,
                        room.get("name"),
                        room.get("key_name"),
                        room.get("sizex"),
                        room.get("sizey")
                    ));
                }
            }, this);
        },

        render: function() {
            this.$el.html(this.template());
            var $roomsList = this.$("#roomsList"),
                rooms = Mapeditor.Variables.mapsCollection,
                self = this;

            this.$el.dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true,
                width: 500,
                title: "Dodawanie pokoju",
                buttons: {
                    "Dodaj": function() {
                        var id = self.$("#roomsList option:selected").val();
                        if(id)
                        {
                            var room = Mapeditor.Variables.mapsCollection.get(id);
                            self.actualMap.get("linked_scenes").add(room);
                            self.actualMap.save();
                            Mapeditor.setStatus("Dodano pokoj {0} do mapy {1}!".format(room.get("name"), self.actualMap.get("name")));
                            self.actualMap = null;
                            $(this).dialog("close");
                        }
                    },

                    "Anuluj": function() {
                        $(this).dialog("close");
                    }
                }
            });
            return this;
        },

        open: function(map) {
            this.actualMap = map;
            this.updateRooms();
            this.$el.dialog("open");
        }
    });
})();
