(function() {
    Mapeditor.Views.Window = Backbone.View.extend({
        tagName: "div",
        className: "window",
        model: Mapeditor.Models.Window,

        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:active", this.updateActive);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.append(this.model.get("view").$el);
            this.$el.toggleClass("active", this.model.get("active"));
            this.model.get("view").render();
            return this;
        },

        updateActive: function() {
            this.$el.toggleClass("active", this.model.get("active"));

            return this;
        }
    });
})();
