(function() {
    Mapeditor.Views.EntityType = Backbone.View.extend({
        tagName: "div",
        className: "entityType block",

        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        initialize: function() {

        },

        render: function() {
            this.$el.data("entityType", this.model);
            this.$el.css({
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32,
                backgroundImage: "url(/static/{0})".format(this.model.get("sprite"))
            });
            return this;
        }
    });
})();
