(function() {
    Mapeditor.Views.ModalCreateRoom = Backbone.View.extend({
        tagName: "div",
        id: "modalCreateRoom",

        initialize: function() {
            this.template = _.template( $("#modalCreateRoomTemplate").html() );
            this.actualMap = null;
        },

        render: function() {
            this.$el.html(this.template());

            this.$el.dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true,
                width: 500,
                title: "Tworzenie pokoju",
                buttons: {
                    "Utworz": function() {
                        var name = this.$(".input_name").val().trim(),
                            key_name = this.$(".input_keyname").val().trim(),
                            type = this.$(".input_type").val(),
                            sizex = parseInt(this.$(".input_sizex").val()),
                            sizey = parseInt(this.$(".input_sizey").val()),
                            $error = this.$("p.error");

                        if(!name) {
                            $error.text("Musisz podać nazwe pokoju!");
                            $error.show();
                            return;
                        }
                        else if(!key_name) {
                            $error.text("Musisz podać bazową nazwe pokoju!");
                            $error.show();
                            return;
                        }

                        var uniq = true;
                        Mapeditor.Variables.mapsCollection.each(function(room) {
                            if(room.get("key_name") == key_name)
                                uniq = false;
                        });

                        if(!uniq) {
                            $error.text("Nazwa bazowa musi być unikalna!");
                            $error.show();
                            return;
                        }

                        var room = new Mapeditor.Models.Scene({
                            name: name,
                            type: type,
                            key_name: key_name,
                            sizex: sizex,
                            sizey: sizey
                        });
                        Mapeditor.Variables.mapsCollection.add(room);

                        this.$(".saving").show();

                        room.once("sync", function() {
                            console.log(this);
                            this.actualMap.get("linked_scenes").add(room);
                            this.actualMap.save();
                            Mapeditor.setStatus("Utworzono pokoj {0}!".format(name));
                            this.$el.dialog("close");
                        }.bind(this));

                        room.save();
                    }.bind(this),

                    "Anuluj": function() {
                        $(this).dialog("close");
                    }
                }
            });

            return this;
        },

        open: function(map) {
            this.actualMap = map;
            this.$(".saving").hide();
            this.$el.dialog("open");
        }
    });
})();
