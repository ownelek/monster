(function() {
    Mapeditor.Views.Map = Backbone.View.extend({
        tagName: "div",
        className: "map",

        events: {
            "click div.name span": function(event) {
                Mapeditor.openMap(this.model);
                event.preventDefault();
            },

            "mousedown div.name span": function(event) {
                // if we want to open map, it gets automatically saved, coz of bubbling
                // this prevent it
                event.stopPropagation();
            },

            "keypress .editName": function(event) {
                if(event.key == "Enter" && $(event.currentTarget).find("input").val().length > 0) {
                    this.model.set("name", $(event.currentTarget).find("input").val());
                }
            },

            "keypress .editKeyName": function(event) {
                if(event.key == "Enter" && $(event.currentTarget).find("input").val().length > 0) {
                    this.model.set("key_name", $(event.currentTarget).find("input").val());
                }
            }
        },

        initialize: function(attrs, options) {
            //this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:name", this.updateName);
            this.listenTo(this.model, "change:key_name", this.updateName);
            this.listenTo(this.model, "change:x", this.setPosition);
            this.listenTo(this.model, "change:y", this.setPosition);
            this.listenTo(this.model, "change:sizex", this.setSize);
            this.listenTo(this.model, "change:sizey", this.setSize);
            //this.listenTo(this.model, "change:isSaving", this.updateClass);
            this.listenTo(this.model, "validate", this.updateValidate);
            this.listenTo(this.model, "request", this.setSaving);
            this.listenTo(this.model, "sync", this.unsetSaving);
            this.listenTo(this.model, "destroy", this.remove);
            this.template = _.template( $("#mapTemplate").html() );
            this.generalMapModel = options.generalMapModel;
        },

        render: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");
            this.$el.html( this.template({
                name: this.model.get("name"),
                x: this.model.get("x"),
                y: this.model.get("y"),
                sizex: this.model.get("sizex"),
                sizey: this.model.get("sizey")
            }) );

            this.$el.data("map", this.model);
            this.updateName();

            this.$el.css({
    			top: this.model.get("y") * pixelsPerUnit - 1,
    			left: this.model.get("x") * pixelsPerUnit - 1,
    			width: this.model.get("sizex") * pixelsPerUnit + 1,
    			height: this.model.get("sizey") * pixelsPerUnit + 1
    		});

            return this;
        },

        updateName: function() {
            if(!this.model.get("name")) {
                this.$("div.active").removeClass("active");
                this.$(".editName").addClass("active");
            }
            else if(!this.model.get("key_name")) {
                this.$("div.active").removeClass("active");
                this.$(".editKeyName").addClass("active");
            }
            else {
                this.$("div.active").removeClass("active");
                this.$(".name").addClass("active");
                this.$(".name span").text(this.model.get("name"));
            }
        },

        setPosition: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");

            this.$(".coords").html("{0}, {1}".format(this.model.get("x"), this.model.get("y")));
            this.$el.css({
                top: this.model.get("y") * pixelsPerUnit - 1,
                left: this.model.get("x") * pixelsPerUnit - 1
            });
        },

        setSize: function() {
            var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");

            this.$(".sizex").html(this.model.get("sizex"));
            this.$(".sizey").html(this.model.get("sizey"));

            this.$el.css({
                width: this.model.get("sizex") * pixelsPerUnit + 1,
                height: this.model.get("sizey") * pixelsPerUnit + 1
            });
        },

        updateValidate: function(model, isValid) {
            //console.log(isValid);
            this.$el.toggleClass("invalid", !isValid);
        },

        setSaving: function() {
            this.$el.toggleClass("saving", true);
        },

        unsetSaving: function() {
            this.$el.toggleClass("saving", false);
        }
    });
})();
