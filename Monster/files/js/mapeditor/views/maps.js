(function() {
    Mapeditor.Views.Maps = Backbone.View.extend({
        tagName: "div",
        id: "maps",

        events: {
            "mousedown": function(event) {
                if(this.generalMapModel.get("adding")) {
                    this.adding = true;
                    this.startPosition = [ event.pageX, event.pageY ];

                    var offset = this.$el.offset();
                    this.$selectionEl = $(document.createElement("div"));
                    this.$selectionEl.addClass("selection");
                    this.$selectionEl.css({
                        top: this.startPosition[1] - offset.top,
                        left: this.startPosition[0] - offset.left
                    });
                    this.$selectionEl.html( this.mapSelectionTemplate() );
                    this.$el.append(this.$selectionEl);

                    event.preventDefault();
                }
            },
            "mousedown .map": function(event) {
                if(event.which == 1 && !this.generalMapModel.get("deleting"))
                {
                    this.map = $(event.currentTarget).data("map");
                    if(this.map.isNew())
                    {
                        this.map = null;
                        return;
                    }
                    this.moveMap = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldMapPosition = [ this.map.get("x"), this.map.get("y") ];
                    event.preventDefault();
                }
            },
            "click .map": function(event) {
                if(this.generalMapModel.get("deleting"))
                {
                    var map = $(event.currentTarget).data("map");
                    if(confirm("Jesteś pewien ze chcesz usunac {0}?".format(this.map.get("name")))) {
                        map.destroy();
                    }
                }
            },
            "mousedown .map div.resize": function(event) {
                if(event.which == 1) {
                    this.map = $(event.currentTarget).parent().data("map");
                    if(this.map.isNew())
                    {
                        this.map = null;
                        return;
                    }
                    this.resizing = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldMapSize = [ this.map.get("sizex"), this.map.get("sizey") ];
                    event.preventDefault();

                    // so we dont resize and move map at the same moment
                    event.stopPropagation();
                }
            },
            "mousemove": function(event) {
                var pixelsPerUnit = this.generalMapModel.get("pixelsPerUnit");
                if(this.map) {
                    var offset = [
                        Math.floor((event.pageX - this.oldMousePosition[0]) / pixelsPerUnit),
                        Math.floor((event.pageY - this.oldMousePosition[1]) / pixelsPerUnit)
                    ];

                    this.map.validate();

                    if(this.moveMap) {
                        this.map.set({
                            x: this.oldMapPosition[0] + offset[0],
                            y: this.oldMapPosition[1] + offset[1]
                        });
                    }
                    else if(this.resizing) {
                        this.map.set({
                            sizex: this.oldMapSize[0] + offset[0],
                            sizey: this.oldMapSize[1] + offset[1]
                        });
                    }

                    event.preventDefault();
                }
                else if(this.adding) {
                    var elOffset = this.$el.offset(),
                        diff = [
                            event.pageX - this.startPosition[0],
                            event.pageY - this.startPosition[1]
                        ],
                        position = [
                            this.startPosition[0] - elOffset.left,
                            this.startPosition[1] - elOffset.top
                        ],
                        newPosition = [0, 0],
                        size = [0, 0];

                    for(var i = 0; i < 2; i++)
                    {
                        if(diff[i] < 0) {
                            newPosition[i] = Math.floor((position[i] + diff[i]) / pixelsPerUnit) * pixelsPerUnit;
                            size[i] = Math.floor(-diff[i] / pixelsPerUnit) * pixelsPerUnit;
                        }
                        else {
                            newPosition[i] = Math.floor(position[i] / pixelsPerUnit) * pixelsPerUnit;
                            size[i] = Math.floor(diff[i] / pixelsPerUnit) * pixelsPerUnit;
                        }
                    }

                    this.$selectionEl.css({
                        top: newPosition[1],
                        left: newPosition[0],
                        width: size[0],
                        height: size[1]
                    });

                    this.selectionPosition = [ newPosition[0] / pixelsPerUnit, newPosition[1] / pixelsPerUnit ];
                    this.selectionSize = [ size[0] / pixelsPerUnit, size[1] / pixelsPerUnit ];

                    this.$selectionEl.find(".coords").html("{0}, {1}".format(this.selectionPosition[0], this.selectionPosition[1]));

                    this.$selectionEl.find(".sizex").html(this.selectionSize[0]);
                    this.$selectionEl.find(".sizey").html(this.selectionSize[1]);
                }
            },
            "mouseup": function() {
                if(this.map) {
                    if(this.map.isValid())
                        this.map.save();
                    else {
                        if(this.moveMap) {
                            this.map.set({
                                x: this.oldMapPosition[0],
                                y: this.oldMapPosition[1]
                            });
                        }
                        else if(this.resizing) {
                            this.map.set({
                                sizex: this.oldMapSize[0],
                                sizey: this.oldMapSize[1]
                            });
                        }
                    }
                }
                else if(this.adding) {
                    this.$selectionEl.remove();
                    this.generalMapModel.set("adding", false);

                    var model = new Mapeditor.Models.Scene({
                        name: "",
                        key_name: "",
                        x: this.selectionPosition[0],
                        y: this.selectionPosition[1],
                        sizex: this.selectionSize[0],
                        sizey: this.selectionSize[1],
                        tiles: [],
                        entities: []
                    });

                    this.collection.add(model);

                    model.once("change:key_name", function() {
                        if(model.get("name") && model.get("key_name"))
                            model.save();
                    });
                }

                this.moveMap = false;
                this.resizing = false;
                this.map = false;
                this.adding = false;
            }
        },

        initialize: function(attrs, options) {
            this.generalMapModel = options.generalMapModel;

            this.mapSelectionTemplate = _.template( $("#mapSelectionTemplate").html() );

            //this.listenTo(this.collection, "update", this.render);
            this.listenTo(this.collection, "add", this.addMap);

            this.listenTo(this.generalMapModel, "change:offsetX", this.moveMe);
            this.listenTo(this.generalMapModel, "change:offsetY", this.moveMe);
            this.listenTo(this.generalMapModel, "change:adding", this.updateClasses);
            this.listenTo(this.generalMapModel, "change:deleting", this.updateClasses);
        },

        render: function() {
            this.$el.empty();
            this.collection.forEach(function(model, index, collection) {
                this.addMap(model);
            }, this);

            this.moveMe();
            this.updateClasses();

            return this;
        },

        addMap: function(model) {
            if(model.get("type") == "global") {
                var view = new Mapeditor.Views.Map({ model: model }, { generalMapModel: this.generalMapModel });
                this.$el.append(view.render().$el);
            }
        },

        moveMe: function() {
            this.$el.css({
                width: this.generalMapModel.get("offsetX") + this.generalMapModel.get("width") + 1000,
                height: this.generalMapModel.get("offsetY") + this.generalMapModel.get("height") + 1000
            });

            this.$el.parent().scrollTop(-this.generalMapModel.get("offsetY")).scrollLeft(-this.generalMapModel.get("offsetX"));
        },

        updateClasses: function() {
            this.$el.toggleClass("adding", this.generalMapModel.get("adding"))
                    .toggleClass("deleting", this.generalMapModel.get("deleting"));
        }
    });
})();
