(function() {
    Mapeditor.Views.RoomLine = Backbone.View.extend({
        tagName: "div",
        className: "room",

        events: {
            "click .edit": function() {
                Mapeditor.openMap(this.model);
            },
            "click .remove": function() {
                if(confirm("Jestes pewien ze chcesz usunac ten pokoj ? Tej operacji nie mozna cofnac.")) {
                    this.model.destroy();
                }
            },
            "click .removeLink": function() {
                this.parentMap.get("rooms").remove(this.model);
                this.parentMap.save();
                this.remove();
            }
        },

        initialize: function(options) {
            this.template = _.template( $("#roomLineTemplate").html() );
            this.parentMap = options.map;
            this.listenTo(this.model, "destroy", this.remove);
        },

        render: function() {
            this.$el.html( this.template({
                id: this.model.id,
                name: this.model.get("name"),
                sizex: this.model.get("sizex"),
                sizey: this.model.get("sizey")
            }) );
            return this;
        }
    });
})();
