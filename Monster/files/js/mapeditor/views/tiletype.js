(function() {
    Mapeditor.Views.TileType = Backbone.View.extend({
        tagName: "div",
        className: "tile block",
        model: Mapeditor.Models.TileType,
        events: {
            "click": function() {
                $(".block.active").removeClass("active");
                this.$el.addClass("active");
            }
        },

        render: function() {
            this.$el.data("tileType", this.model);
            this.$el.css({
                backgroundPosition: this.model.get("x") + "px " + this.model.get("y") + "px",
                backgroundImage: "url('/static/{0}')".format(this.model.get("tileset").get("path"))
            });

            return this;
        }
    });
})();
