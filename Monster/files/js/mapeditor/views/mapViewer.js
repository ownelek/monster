(function() {
    Mapeditor.Views.MapViewer = Backbone.View.extend({
        events: {
            "click .world .entity": function(event) {
                this.$(".entity.selected").removeClass("selected");
                var $entity = $(event.currentTarget);
                $entity.addClass("selected");
                Mapeditor.Variables.entityPropertiesView.render($entity.data("model"));
            },
            "click .world .tile": function(event) {
                var $active = $("div.block.active"),
                    $selected = $("div.selected");

                if($active.hasClass("entityType")) {
                    var entityType = $active.data("entityType"),
                        tileModel = $(event.currentTarget).data("tileModel");

                    if(!entityType || !tileModel)
                        return;

                    this.model.get("entities").add(new Mapeditor.Models.Entity({
                        scene: this.model,
                        type: entityType,
                        face: "s",
                        x: tileModel.get("x"),
                        y: tileModel.get("y"),
                        z: tileModel.get("z"),
                        properties: entityType.get("properties").map(function(property) {
                            return {
                                key: property.name,
                                value: property.default
                            };
                        })
                    }));

                    $active.removeClass("active");
                }
                else if($selected.length > 0) {
                    $selected.removeClass("selected");
                    Mapeditor.Variables.entityPropertiesView.render();
                }
            },
            "mousedown .world": function(event) {
                if(event.which == 3)
                {
                    this.rightDown = true;
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    this.oldWorldPosition = [ this.worldPosition[0], this.worldPosition[1] ];
                }
            },
            "mousedown .world .tile": function(event) {
                if(event.which == 1)
                {
                    this.mouseDown = true;
                    this.setTile(event);
                    event.preventDefault();
                }
            },
            "mousedown .world .entity.selected": function(event) {
                if(event.which == 1) {
                    this.movingEntity = true;
                    this.movedEntity = $(event.currentTarget).data("model");
                    this.oldEntityPosition = [ this.movedEntity.get("x"), this.movedEntity.get("y") ];
                    this.oldMousePosition = [ event.pageX, event.pageY ];
                    event.preventDefault();
                }
            },
            "mousemove .world .tile": "setTile",
            "mousemove .world": function(event) {
                if(this.rightDown) {
                    var mousePosition = [ event.pageX, event.pageY ];
                    this.worldPosition = [
                        this.oldWorldPosition[0] + mousePosition[0] - this.oldMousePosition[0],
                        this.oldWorldPosition[1] + mousePosition[1] - this.oldMousePosition[1]
                    ];

                    this.$(".world").css({
                        top: this.worldPosition[1],
                        left: this.worldPosition[0]
                    });
                }
                else if(this.movingEntity) {
                    var offset = [
                        event.pageX - this.oldMousePosition[0],
                        event.pageY - this.oldMousePosition[1]
                    ];

                    this.movedEntity.set({
                        x: this.oldEntityPosition[0] + Math.round(offset[0] / 32),
                        y: this.oldEntityPosition[1] + Math.round(offset[1] / 32)
                    });
                }
            },

            "click .tabs .tab": "settingsChangeTab",
            "change input.showMoveMask": "toggleMoveMask",
            "change input.showAllLayers": "toggleAllLayers",
            "click input.upperLayer": "goUpperLayer",
            "click input.lowerLayer": "goLowerLayer",
            "click input.save": "saveMap",
            "click input.removeLayer": "removeActualLayer",
            "click input.createMapImage": "createMapImage",

            "click input.add_room": "addRoomModal",
            "click input.create_room": "createRoomModal",

            "click input.add_encounter": function() {
                this.model.get("monsters").add({
                    type: this.$(".encounter_type").val()
                });
            },

            "change input.encounter_chance": function(event) {
                this.model.set("encounter_ratio", parseInt($(event.currentTarget).val().replace("%", "")));
            },

            "change select.encounter_type": "updateEncounters"
        },

        initialize: function() {
            this.template = _.template( $("#mapEditTemplate").html() );

            this.showMoveMask = false;
            this.showLayer = 0;
            this.showAllLayers = false;
            this.worldPosition = [ 0, 0 ];

            //this.listenTo(this.model.get("tiles"), "update", this.reloadLayers);
            this.listenTo(this.model.get("tiles"), "reset", this.reloadLayers);
            this.listenTo(this.model.get("tiles"), "sync", this.updateStatus);
            this.listenTo(this.model.get("entities"), "sync", this.updateStatus);
            this.listenTo(this.model, "add:linked_scenes", this.addRoom);
            this.listenTo(this.model, "add:monsters", this.updateEncounters);
            this.listenTo(this.model, "sync", this.updateStatus);

            $(document).mouseup(function() {
                this.mouseDown = false;
                this.rightDown = false;
                this.movingEntity = false;
            }.bind(this));
        },

        render: function() {
            this.$el.html(this.template());
            this.worldPosition[0] = this.$(".settings").outerWidth();
            this.$(".world").css({
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32,
                left: this.worldPosition[0]
            });
            this.renderLayers();
            this.updateEncounters();

            if(this.model.get("linked_scenes").length > 0)
            {
                var $roomList = this.$(".roomList");
                this.$(".norooms").hide();
                $roomList.show();

                this.model.get("linked_scenes").each(function(room) {
                    $roomList.append(new Mapeditor.Views.RoomLine({ model: room }).render().$el);
                }, this);
            }
            else {
                this.$(".roomList").hide();
                this.$(".norooms").show();
            }

            // check for inconsistency across tiles
            if(this.model.get("tiles").length > this.model.get("sizex") * this.model.get("sizey") * this.maxZ) {
                if(confirm("Wygląda na to ze czesc tilesow jest poza mapa. Usunac je?")) {
                    this.model.get("tiles").destroy_outsides();
                }
            }

            return this;
        },

        reloadLayers: function() {
            this.layers = [];
            this.model.checkLayers();
            this.maxZ = parseInt(_.max(_.keys(this.model.tilesInLayers)));
            this.maxZ = isNaN(this.maxZ) ? 1 : this.maxZ + 1;

            for(var i = 0; i < this.maxZ; i++)
                this.ensureLayer(i);

            this.render();
        },

        settingsChangeTab: function(event) {
            var index = $(event.currentTarget).index();
            var $tab = $(this.$(".settings .content .tab").get(index));
            this.$(".tab.active").removeClass("active");
            $tab.addClass("active");
        },

        addRoom: function(room) {
            this.$(".roomList").append(new Mapeditor.Views.RoomLine({ model: room }).render().$el);
            this.$(".roomList").show();
            this.$(".norooms").hide();
        },

        toggleMoveMask: function() {
            this.showMoveMask = !this.showMoveMask;
            this.$(".world").toggleClass("showMoveMask", this.showMoveMask);
        },

        toggleAllLayers: function() {
            this.showAllLayers = !this.showAllLayers;
            this.renderLayers();
            _.each(this.layers, function(layer, index, list) {
                var state = "none";
                if(this.showAllLayers || this.showLayer == layer.layer)
                    state = "visible";
                else if(this.showLayer - 1 == layer.layer)
                    state = "halfVisible";

                layer.toggleLayer(state);
            }, this);
        },

        goUpperLayer: function() {
            if(this.showLayer < this.maxZ)
                this.showLayer++;

            this.changeLayer();
        },

        goLowerLayer: function() {
            if(this.showLayer > 0)
                this.showLayer--;

            this.changeLayer();
        },

        changeLayer: function() {
            this.ensureLayer(this.showLayer);
            this.$(".showLayer").text(this.showLayer);
            this.renderLayers();
            _.each(this.layers, function(layer, index, list) {
                var state = "none";
                if(this.showAllLayers || this.showLayer == layer.layer)
                    state = "visible";
                else if(this.showLayer - 1 == layer.layer)
                    state = "halfVisible";

                layer.toggleLayer(state);
            }, this);
        },

        ensureLayer: function(layer) {
            this.model.createLayer(layer);

            if(!this.layers[layer])
            {
                this.layers[layer] = new Mapeditor.Views.MapLayer({ model: this.model }, {
                    layer: layer,
                    state: this.showLayer == layer ? "visible" : (this.showLayer - 1 == layer ? "halfVisible" : "none")
                });
            }
        },

        renderLayers: function() {
            var $world = this.$(".world");
            _.each(this.layers, function(layer, index, list) {
                if(!layer.rendered)
                    $world.append(layer.render().$el);
            });
        },

        removeActualLayer: function() {
            if(this.showLayer > 0 && confirm("Jesteś pewien ze chcesz usunąć aktualną warstwe? Zostanie ona rowniez usunieta na serwerze. Tej operacji nie mozna cofnac."))
                this.removeLayer(this.showLayer);
        },

        removeLayer: function(layer) {
            if(layer > 0)
            {
                Mapeditor.setStatus("Usuwanie warstwy ...");
                this.model.removeLayer(layer);
                this.layers[layer].remove();
                this.layers.splice(layer, 1);

                // reorder layers
                _.each(this.layers, function(layer, index) {
                    layer.layer = index;
                    layer.updateZIndex();
                });

                this.maxZ = this.layers.length;

                if(this.showLayer == layer)
                {
                    this.showLayer = 0;
                    this.changeLayer();
                }

                Mapeditor.setStatus("Usuwanie warstwy ... Gotowe!");
            }
        },

        updateEncounters: function() {
            var $tbody = this.$("table.encounters tbody");

            $tbody.empty();

            this.model.get("monsters").each(function(monsterInMap) {
                if(monsterInMap.get("type") == this.$(".encounter_type").val())
                    $tbody.append(new Mapeditor.Views.MonsterInMap({ model: monsterInMap }).render().$el);
            }, this);

            this.$(".encounter_chance").val(this.model.get("encounter_ratio") + "%");
        },

        setTile: function(event) {
            if(this.mouseDown)
            {
                var tileModel = $(event.currentTarget).data("tileModel");
                if(!this.showMoveMask) {
                    var activeTileType = $("div.tile.active").data("tileType");

                    if( !tileModel || !activeTileType || activeTileType == tileModel.get("tileType"))
                        return;

                    tileModel.set({ tileType: activeTileType });
                }
                else {
                    var activeMoveMask = $("div.movemask.active").data("moveMask");

                    if( !tileModel || !activeMoveMask || activeMoveMask == tileModel.get("moveMask") )
                        return;

                    tileModel.set({ moveMask: activeMoveMask });
                }

                event.preventDefault();
            }
        },

        saveMap: function() {
            if(!Mapeditor.Variables.savingMap && !Mapeditor.Variables.generatingMapImage)
            {
                Mapeditor.Variables.savingMap = true;
                Mapeditor.setStatus("Zapisywanie mapy {0} ...".format(this.model.get("name")));
                /*this.model.get("tiles").clear({ success: function() {

                }.bind(this)});*/
                this.savedSize = 0;
                this.totalSaveSize = this.model.get("tiles").changedTiles().length + this.model.get("entities").length + 1;
                this.model.save();
                this.model.get("tiles").save();
                this.model.get("entities").save();
            }
        },

        updateStatus: function(collection, data, options) {
            if(Mapeditor.Variables.savingMap) {
                console.log("sync", collection instanceof Backbone.Collection, data);

                if(collection instanceof Backbone.Collection) this.savedSize += data.length;
                else this.savedSize++;

                if(this.savedSize / this.totalSaveSize >= 1.0)
                {
                    Mapeditor.Variables.savingMap = false;
                    Mapeditor.setStatus("Zapisywanie mapy {0} ... Gotowe!".format(this.model.get("name")));
                }
                else {
                    Mapeditor.setStatus("Zapisywanie mapy {0} ... {1}% ({2} / {3})".format(
                        this.model.get("name"),
                        Math.round(this.savedSize / this.totalSaveSize * 100),
                        this.savedSize,
                        this.totalSaveSize
                    ));
                }
            }
        },

        createMapImage: function() {
            if(!Mapeditor.Variables.savingMap && !Mapeditor.Variables.generatingMapImage) {
                Mapeditor.setStatus("Generowanie obrazu mapy ...");
                Mapeditor.Variables.generatingMapImage = true;
                $.post(this.model.url() + "/image/", function(data) {
                    Mapeditor.setStatus("Generowanie obrazu mapy ... Gotowe!");
                    Mapeditor.Variables.generatingMapImage = false;
                }, "json");
            }
        },

        createRoomModal: function() {
            Mapeditor.Variables.modalCreateRoomView.open(this.model);
        },

        addRoomModal: function() {
            Mapeditor.Variables.modalAddRoomView.open(this.model);
        }
    });
})();
