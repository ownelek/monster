(function() {
    Mapeditor.Views.EntityProperties = Backbone.View.extend({
        el: "div#entityProperties",

        events: {
            "keyup input": function(event) {
                var $input = $(event.currentTarget);
                if(this.actualModel) {
                    if($input.attr("property-type") == "basic")
                        this.actualModel.set($input.attr("name"), $input.val());
                    else if($input.attr("property-type") == "normal")
                        this.actualModel.setProperty($input.attr("name"), $input.val());
                }
            }
        },

        initialize: function() {
            this.template = _.template( $("#entityPropertiesTemplate").html() );
        },

        render: function(model) {
            this.actualModel = model;
            if(model) {
                this.$el.show();
                this.$el.html(this.template({ model: model }));
            }
            else {
                this.$el.hide();
            }

            return this;
        }
    });
})();
