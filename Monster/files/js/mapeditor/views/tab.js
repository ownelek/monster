(function() {
    Mapeditor.Views.Tab = Backbone.View.extend({
        tagName: "div",
        className: "tab",
        model: Mapeditor.Models.Window,

        events: {
            "click span.title": function() {
                this.model.collection.setActive(this.model);
            },
            "click span.close": function() {
                this.model.collection.remove(this.model);
            }
        },

        initialize: function() {
            this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "change:active", this.render);
            this.listenTo(this.model, "remove", this.remove);
        },

        render: function() {
            this.$el.html("<span class = 'title'>{0}</span> <span class = 'close'>[X]</span>".format(this.model.get("title")));
            this.$el.toggleClass("active", this.model.get("active"));

            return this;
        }
    });
})();
