(function() {
    window.Monster = {
        Models: {},
        Collections: {},
        Views: {},
        Vars: {},

        start: function() {
            Monster.Vars.gameModel = gameModel = new Monster.Models.Game();

            var player = new Monster.Models.CurrentPlayer(),
                playerCollection = new Monster.Collections.Players([ player ]),
                mapsCollection = new Monster.Collections.Scenes(),
                moveMasksCollection = new Monster.Collections.MoveMasks(),
                entityTypesCollection = new Monster.Collections.EntityTypes(),
                entityCollection = new Monster.Collections.Entities(),
                statusEffectsCollection = new Monster.Collections.StatusEffects(),
                elementsCollection = new Monster.Collections.Elements(),
                abilitiesCollection = new Monster.Collections.Abilities(),
                monsterTypesCollection = new Monster.Collections.MonsterTypes(),
                windowModel = new Monster.Models.Window();

            gameModel.set({
                maps: mapsCollection,
                entities: entityCollection,
                players: playerCollection,
                currentPlayer: player,
                windowModel: windowModel
            });

            var sceneNameView = new Monster.Views.SceneName({ model: gameModel }),
                menuView = new Monster.Views.Menu({ model: gameModel }),
                windowView = new Monster.Views.Window({ model: gameModel });

            mapsCollection.fetch({ success: function() {
                player.fetch({ success: function() {
                    player.get("monsters").fetch();
                } });
            } });

            moveMasksCollection.fetch();
            entityTypesCollection.fetch();
            entityCollection.fetch();
            statusEffectsCollection.fetch();
            elementsCollection.fetch();
            abilitiesCollection.fetch();
            monsterTypesCollection.fetch();
            gameModel.fetchSize();
            gameModel.openConnection();

            $(window).unload(function() {
                gameModel.closeConnection();
            });

            var $windowWrapper = $("#windowWrapper"),
                $document = $(document);

            Monster.Vars.gameModel = gameModel;
            Monster.Vars.$windowWrapper = $windowWrapper;

            $("#windowView").append(windowView.render().$el);
            $windowWrapper.append(sceneNameView.render().$el);
            $windowWrapper.append(menuView.render().$el);

            $document.keydown(function(event) {
                gameModel.trigger("keydown", event);
            });

            $document.keyup(function(event) {
                gameModel.trigger("keyup", event);
            });

            $document.keypress(function(event) {
                gameModel.trigger("keypress", event);
            });
        }
    };
})();

(function() {
    Monster.Models.Player = Backbone.RelationalModel.extend({
        defaults: {
            odd: false,
            speed: 200,
            direction_to_offset: {
                n: { x: 0, y: -1 },
                w: { x: -1, y: 0 },
                e: { x: 1, y: 0 },
                s: { x: 0, y: 1 }
            },
            locked: false
        },

        initialize: function() {
            this.game = Monster.Vars.gameModel;
            this.listenTo(this.game, "socket:received", this.received);
            this.listenTo(this, "change:x", this.checkCollisions);
            this.listenTo(this, "change:y", this.checkCollisions);
        },

        received: function(gameModel, data) {
            if(data.action == "player.step" && data.playerId == this.id)
            {
                this.move(data.direction);
            }
        },

        checkCollisions: function() {
            this.get("scene").get("entities").each(function(entity) {
                if(entity.get("x") == this.get("x") && entity.get("y") == this.get("y") && entity.get("z") == this.get("z"))
                    entity.onPlayerCollide(this);
            }, this);
        },

        move: function(direction) {
            if(!this.game.get("connected"))
                return;

            var direction_to_offset = this.get("direction_to_offset");

            if(_.keys(direction_to_offset).indexOf(direction) == -1 || this.get("locked"))
                return;

            var map = this.get("scene"),
                offset = direction_to_offset[direction];

            if(this.get("x") + offset.x < map.get("x") || this.get("y") + offset.y < map.get("y") ||
                this.get("x") + offset.x >= map.get("x") + map.get("sizex") || this.get("y") + offset.y >= map.get("y") + map.get("sizey")) {

                var foundMap = map.collection.find(function(map) {
                    return this.get("x") + offset.x >= map.get("x") && this.get("y") + offset.y >= map.get("y") &&
                        this.get("x") + offset.x < map.get("x") + map.get("sizex") && this.get("y") + offset.y < map.get("y") + map.get("sizey");
                }, this);

                if(foundMap) {
                    this.set("scene", foundMap);
                }
                else {
                    return;
                }
            }

            this.set({
                odd: !this.get("odd"),
                face: direction,
                x: this.get("x") + direction_to_offset[direction].x,
                y: this.get("y") + direction_to_offset[direction].y
            });

            this.trigger("step", this, direction, direction_to_offset[direction]);

            setTimeout(function() {
                this.trigger("endstep", this, direction);
            }.bind(this), this.get("speed"));
        },

        relations: [
            {
                type: Backbone.HasOne,
                key: "scene",
                relatedModel: "Monster.Models.Scene",
                includeInJSON: true,
                reverseRelation: {
                    key: "players",
                    collectionType: "Monster.Collections.Players"
                }
            },
            {
                type: Backbone.HasMany,
                key: "monsters",
                relatedModel: "Monster.Models.Monster",
                collectionType: "Monster.Collections.Monsters",
                includeInJSON: "id",
                reverseRelation: {
                    key: "owner"
                }
            }
        ]
    });
})();

(function() {
    Monster.Models.Ability = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "element_type",
                relatedModel: "Monster.Models.Element"
            },
            {
                type: Backbone.HasOne,
                key: "status_effect",
                relatedModel: "Monster.Models.StatusEffect"
            }
        ]
    });
})();

(function() {
    Monster.Models.CurrentPlayer = Monster.Models.Player.extend({
        defaults: {
            odd: false,
            speed: 200,
            direction_to_offset: {
                n: { x: 0, y: -1 },
                w: { x: -1, y: 0 },
                e: { x: 1, y: 0 },
                s: { x: 0, y: 1 }
            },
            locked: false
        },

        url: function() {
            return "/api/players/current/";
        },

        initialize: function() {
            this.game = Monster.Vars.gameModel;
            this.listenTo(this.game, "socket:received", this.received);
            this.listenTo(this, "change:x", this.checkCollisions);
            this.listenTo(this, "change:y", this.checkCollisions);
        },

        received: function(gameModel, data) {
            if(data.action == "player.crossedMap" && data.playerId == this.id)
            {
                this.set("scene", data.newMapId);
                console.log("Crossed");
            }
        },

        checkCollisions: function() {
            this.get("scene").get("entities").each(function(entity) {
                if(entity.get("x") == this.get("x") && entity.get("y") == this.get("y") && entity.get("z") == this.get("z"))
                    entity.onPlayerCollide(this);
            }, this);
        },

        move: function(direction) {
            if(!this.game.get("connected"))
                return;

            var direction_to_offset = this.get("direction_to_offset");

            if(_.keys(direction_to_offset).indexOf(direction) == -1 || this.get("locked") || this.get("isStepping"))
                return;

            var map = this.get("scene"),
                offset = direction_to_offset[direction];

            if(this.get("x") + offset.x < map.get("x") || this.get("y") + offset.y < map.get("y") ||
                this.get("x") + offset.x >= map.get("x") + map.get("sizex") || this.get("y") + offset.y >= map.get("y") + map.get("sizey")) {

                var foundMap = map.collection.find(function(map) {
                    return this.get("x") + offset.x >= map.get("x") && this.get("y") + offset.y >= map.get("y") &&
                        this.get("x") + offset.x < map.get("x") + map.get("sizex") && this.get("y") + offset.y < map.get("y") + map.get("sizey");
                }, this);

                if(foundMap) {
                    this.set("scene", foundMap);
                }
                else {
                    return;
                }
            }

            Monster.Vars.gameModel.sendMessage({
                action: "player.step",
                direction: direction
            });

            this.set({
                isMoving: true,
                isStepping: true,
                direction: direction,
                odd: !this.get("odd"),
                face: direction
            });

            var tile = this.getRelativeTile(direction_to_offset[direction]);
            if(tile && tile.get("moveMask").canEnter(this))
            {
                this.set({
                    x: this.get("x") + direction_to_offset[direction].x,
                    y: this.get("y") + direction_to_offset[direction].y
                });
            }

            this.trigger("step", this, direction, direction_to_offset[direction]);

            setTimeout(function() {
                this.set("isStepping", false);
                this.trigger("endstep", this, direction);
            }.bind(this), this.get("speed"));
        },

        getRelativeTile: function(offset) {
            var map = this.get("scene"),
                tile = map.positionMap[
                    this.get("z") * map.get("sizex") * map.get("sizey") +
                    (this.get("y") + offset.y) * map.get("sizex") +
                    this.get("x") + offset.x
                ];

            return tile;
        }
    });
})();

(function() {
    Monster.Models.Element = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasMany,
                key: "strong_against"
            },
            {
                type: Backbone.HasMany,
                key: "weak_against"
            }
        ]
    });
})();

(function() {
    Monster.Models.Entity = Backbone.RelationalModel.extend({
        getProperty: function(key) {
            return _.find(this.get("properties"), function(prop) {
                return key == prop.key;
            });
        },
        subModelTypes: {
            1: "Monster.Models.EntityEnterScene",
        },
        subModelTypeAttribute: "type",
        relations: [{
            type: Backbone.HasOne,
            key: "type",
            relatedModel: "Monster.Models.EntityType"
        }]
    });
})();

(function() {
    Monster.Models.EntityType = Backbone.RelationalModel.extend({
        defaults: {
            name: "",
            sprite: "",
            visible: false,
            sizex: 0,
            sizey: 0
        }
    });
})();

(function() {
    Monster.Models.Game = Backbone.Model.extend({
        defaults: {
            connected: false
        },
        url: "ws://127.0.0.1:80/game",
        initialize: function(attrs, options) {

        },

        openConnection: function() {
            var socket = new WebSocket(_.result(this, "url"));
            this.set("socket", socket);
            socket.onopen = function() {
                console.log("Connected!");
                
                this.set("connected", true);
                this.trigger("socket:connected", this);
            }.bind(this);

            socket.onmessage = function(event) {
                this.trigger("socket:received", this, event);
            }.bind(this);

            socket.onclose = function() {
                this.set("connected", false);
                this.trigger("socket:closed", this);

                // try to reconnect
                console.log("Lost connection");
                console.log("Reconnecting in 5");
                setTimeout(this.openConnection.bind(this), 5000);
            }.bind(this);
        },

        sendMessage: function(message) {
            this.get("socket").send(JSON.stringify(message));
        },

        closeConnection: function() {
            this.get("socket").close();
        },

        fetchSize: function() {
            $.get("/api/maps/size/", function(data) {
                this.set(data);
            }.bind(this), "json");
        }
    });
})();

(function() {
    Monster.Models.LearnableAbility = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "ability",
                relatedModel: "Monster.Models.Ability",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                key: "monster_type",
                relatedModel: "Monster.Models.MonsterType",
                includeInJSON: "id",
                reverseRelation: {
                    key: "abilities"
                }
            }
        ]
    });
})();

(function() {
    Monster.Models.Monster = Backbone.RelationalModel.extend({
        getMaxHealth: function() {
            return this.get("monster_type").get("base_health") + this.get("monster_type").get("health_growth") * this.get("level");
        },
        getAttack: function() {
            return this.get("monster_type").get("base_attack") + this.get("monster_type").get("attack_growth") * this.get("level");
        },
        getDefense: function() {
            return this.get("monster_type").get("base_defense") + this.get("monster_type").get("defense_growth") * this.get("level");
        },
        getSpeed: function() {
            return this.get("monster_type").get("base_speed") + this.get("monster_type").get("speed_growth") * this.get("level");
        },
        relations: [
            {
                type: Backbone.HasOne,
                key: "monster_type",
                relatedModel: "Monster.Models.MonsterType"
            }
        ]
    });
})();

(function() {
    Monster.Models.MonsterType = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "element_type",
                relatedModel: "Monster.Models.Element"
            },
            {
                type: Backbone.HasOne,
                key: "evolution"
            }
        ]
    });
})();

(function() {
    Monster.Models.MoveMask = Backbone.RelationalModel.extend({
        canEnter: function() {
            return true;
        },

        onEnter: function() {},
        subModelTypes: {
            "Move": "Monster.Models.NoBlockMoveMask",
            "Block": "Monster.Models.BlockMoveMask",
            "Wild": "Monster.Models.WildMoveMask"
        },
        subModelTypeAttribute: "key_name"
    });
})();

(function() {
    Monster.Models.Scene = Backbone.RelationalModel.extend({
        initialize: function() {
            this.positionMap = [];
            this.listenTo(this.get("tiles"), "update", this.updatePositionMap);
            this._fetched = false;
        },

        distance: function(otherMap) {
            var center = [ this.get("x") + this.get("sizex") / 2, this.get("y") + this.get("sizey") / 2 ],
                otherCenter = [ otherMap.get("x") + otherMap.get("sizex") / 2, otherMap.get("y") + otherMap.get("sizey") / 2 ];

            return Math.sqrt(Math.pow(center[0] - otherCenter[0], 2.0) + Math.pow(center[1] - otherCenter[1], 2.0));
        },

        fetchLayersImage: function(options) {
            $.get(this.url() + "/image/", function(data) {
                this.set("layers", data);
                if(options && options.success) options.success.call(this, data);
            }.bind(this), "json");
        },

        fetchClosestMaps: function(allDoneCallback) {
            if(this.get("type") != "global")
            {
                if(allDoneCallback) allDoneCallback.call(this);
                return []; // dont fetch any maps if we are not in open space
            }

            var closestMaps = this.collection.reject(function(map) {
                return map.get("type") != "global" || map == this ||
                    this.distance(map) > (Math.sqrt(Math.pow(this.get("sizex"), 2.0) + Math.pow(this.get("sizey"), 2.0)) / 2) +
                        (Math.sqrt(Math.pow(map.get("sizex"), 2.0) + Math.pow(map.get("sizey"), 2.0)) / 2);
            }, this).sort(function(a, b) {
                    return this.distance(a) - this.distance(b);
            }.bind(this));

            var count = 0;
            function check() {
                count++;
                if(count == closestMaps.length && allDoneCallback)
                    allDoneCallback.call(this);
            }

            _.each(closestMaps, function(map) {
                map.fetchAll({}, check);
            });

            return closestMaps;
        },

        fetchAll: function(options, allDoneCallback) {
            if(this._fetched)
            {
                allDoneCallback.call(this);
                return;
            }

            var success = options.success, count = 0;
            options.success = function() {
                count++;
                if(success) success.apply(this, arguments);
                if(allDoneCallback && count == 3) allDoneCallback.call(this);
            };

            this.get("tiles").fetch(options);
            this.get("entities").fetch(options);
            this.fetchLayersImage(options);
            this._fetched = true;
        },

        updatePositionMap: function(collection, options) {
            collection.each(function(tile) {
                this.positionMap[this.get("sizex") * this.get("sizey") * tile.get("z") + this.get("sizex") * tile.get("y") + tile.get("x")] = tile;
            }, this);
        },

        relations: [
            {
                type: Backbone.HasMany,
                key: "tiles",
                relatedModel: "Monster.Models.Tile",
                collectionType: "Monster.Collections.Tiles",
                reverseRelation: {
                    key: "scene"
                }
            },
            {
                type: Backbone.HasMany,
                key: "entities",
                relatedModel: "Monster.Models.Entity",
                collectionType: "Monster.Collections.Entities",
                reverseRelation: {
                    key: "scene"
                }
            }
        ]
    });
})();

(function() {
    Monster.Models.StatusEffect = Backbone.RelationalModel.extend();
})();

(function() {
    Monster.Models.Tile = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "moveMask",
                keySource: "move_mask",
                relatedModel: "Monster.Models.MoveMask"
            }
        ]
    });
})();

(function() {
    Monster.Models.UsableAbility = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "monster",
                relatedModel: "Monster.Models.Monster",
                includeInJSON: "id",
                reverseRelation: {
                    key: "abilities"
                }
            },
            {
                type: Backbone.HasOne,
                key: "ability",
                relatedModel: "Monster.Models.Ability",
                includeInJSON: "id"
            }
        ]
    });
})();

(function() {
    Monster.Models.Window = Backbone.Model.extend({
        defaults: {
            scrollX: 0,
            scrollY: 0,
            sizeX: 16,
            sizeY: 16,
            marginX: 3,
            marginY: 3
        },

        initialize: function() {
            this.game = Monster.Vars.gameModel;
            this.listenTo(this.game, "socket:received", this.handleSocket);
        },

        handleSocket: function(game, data) {
            if(data.action == "player.connected") {
                this.game.get("players").add(data.player);
            }
            else if(data.action == "player.disconnected") {
                this.game.get("players").remove(data.player);
            }
        }
    });
})();

(function() {
    Monster.Models.EntityEnterScene = Monster.Models.Entity.extend({
        onTick: function() {

        },

        onPlayerCollide: function(player) {
            console.log("enterscene");
            var entities = Monster.Vars.gameModel.get("entities"),
                scene = player.get("scene"),
                ent = entities.get(parseInt(this.getProperty("destination").value)),
                opposite_direction = {
                    "n": "s",
                    "s": "n",
                    "e": "w",
                    "w": "e"
                };

            if(ent) {
                player.set("locked", true);
                player.set({
                    scene: ent.get("scene"),
                    x: ent.get("x") + player.get("direction_to_offset")[opposite_direction[ent.get("face")]].x,
                    y: ent.get("y") + player.get("direction_to_offset")[opposite_direction[ent.get("face")]].y,
                    z: ent.get("z"),
                    face: opposite_direction[ent.get("face")],
                    isMoving: false
                });
            }
            else {
                console.log("Destination not exists");
            }
        }
    });
})();

(function() {
    Monster.Models.BlockMoveMask = Monster.Models.MoveMask.extend({
        canEnter: function(player) {
            return false;
        }
    });
})();

(function() {
    Monster.Models.NoBlockMoveMask = Monster.Models.MoveMask.extend({
        canEnter: function(player) {
            return true;
        }
    });
})();

(function() {
    Monster.Models.WildMoveMask = Monster.Models.MoveMask.extend({
        canEnter: function(player) {
            return true;
        }
    });
})();

(function() {
    Monster.Collections.Abilities = Backbone.Collection.extend({
        model: Monster.Models.Ability,
        url: "/api/abilities/"
    });
})();

(function() {
    Monster.Collections.Elements = Backbone.Collection.extend({
        model: Monster.Models.Element,
        url: "/api/elements/"
    });
})();

(function() {
    Monster.Collections.Entities = Backbone.Collection.extend({
        model: Monster.Models.Entity,
        url: function() {
            if(this.scene)
                return "/api/maps/{0}/entities/".format(this.scene.id);

            return "/api/entities/";
        }
    });
})();

(function() {
    Monster.Collections.EntityTypes = Backbone.Collection.extend({
        model: Monster.Models.EntityType,
        url: "/api/entitytypes/"
    });
})();

(function() {
    Monster.Collections.Monsters = Backbone.Collection.extend({
        model: Monster.Models.Monster,

        url: function() {
            return this.owner ? "/api/players/{0}/monsters/".format(this.owner.id) : "/api/monsters/";
        }
    });
})();

(function() {
    Monster.Collections.MonsterTypes = Backbone.Collection.extend({
        model: Monster.Models.MonsterType,
        url: "/api/monstertypes/"
    });
})();

(function() {
    Monster.Collections.MoveMasks = Backbone.Collection.extend({
        model: Monster.Models.MoveMask,
        url: function() {
            return "/api/movemasks/";
        }
    });
})();

(function() {
    Monster.Collections.Players = Backbone.Collection.extend({
        model: Monster.Models.Player,
        url: function() {
            return "/api/players/";
        }
    });
})();

(function() {
    Monster.Collections.Scenes = Backbone.Collection.extend({
        model: Monster.Models.Scene,
        url: function() {
            return "/api/maps/";
        }
    });
})();

(function() {
    Monster.Collections.StatusEffects = Backbone.Collection.extend({
        model: Monster.Models.StatusEffect,
        url: "/api/status_effects/"
    });
})();

(function() {
    Monster.Collections.Tiles = Backbone.Collection.extend({
        model: Monster.Models.Tile,
        url: function() {
            return "/api/maps/{0}/tiles/".format(this.scene.id);
        }
    });
})();

(function() {
    Monster.Views.Fading = Backbone.View.extend({
        tagName: "div",
        id: "fading",

        events: {
            "transitionend": function(event) {
                this.trigger("transitionend", this, event);
            }
        },
        
        render: function() {
            this.$el.css({
                width: this.model.get("world_width") * 32,
                height: this.model.get("world_height") * 32
            });
            return this;
        },

        fadeIn: function() {
            this.$el.toggleClass("active", true);
        },

        fadeOut: function() {
            this.$el.toggleClass("active", false);
        }
    });
})();

    (function() {
    Monster.Views.FightScene = Backbone.View.extend({
        tagName: "div",
        id: "fight-scene",

        events: {

        },

        initialize: function() {
            this.listenTo(Monster.Vars.gameModel, "keypress", this.handleKeyPress);

            this.template = _.template( $("#fightSceneTemplate").html() );
            this.selectedDialog = 0;
            this.selectedColumn = 0;
            this.selectedRow = 0;
        },

        render: function() {
            this.$el.html(this.template(
                {
                    fighting_monster: this.model.get("monsters").find(function(monster) {
                        return monster.get("slot") === 0;
                    }),
                    player: this.model
                }
            ));
            this.renderSelection();
            return this;
        },

        renderSelection: function() {
            var dialog = ".right_dialog";
            if(this.selectedDialog)
                dialog = ".fight_dialog";

            this.$(".entry.selected").removeClass("selected");
            this.$("{0} tr:nth-child({1}) td:nth-child({2}) span.entry".format(dialog, this.selectedRow + 1, this.selectedColumn + 1)).addClass("selected");
        },

        handleKeyPress: function(event) {
            var key = event.key.toLowerCase();
            if(key == 'w') {
                this.selectedRow = Math.max(0, this.selectedRow - 1);
            }
            else if(key == "s") {
                this.selectedRow = Math.min(1, this.selectedRow + 1);
            }
            else if(key == "d") {
                this.selectedColumn = Math.min(1, this.selectedColumn + 1);
            }
            else if(key == "a") {
                this.selectedColumn = Math.max(0, this.selectedColumn - 1);
            }
            else if(key == "enter") {

            }

            this.renderSelection();
        }
    });
})();

 (function() {
    Monster.Views.Map = Backbone.View.extend({
        tagName: "div",
        className: "world",

        initialize: function() {
            this.listenTo(this.model, "change", this.render);
        },

        render: function() {
            this.$el.css({
                top: this.model.get("y") * 32,
                left: this.model.get("x") * 32,
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32
            });

            this.$el.empty();
            for(var i = 0; i < this.model.get("maxZ"); i++)
                this.$el.append(new Monster.Views.MapLayer({ model: this.model }, { layer: i }).render().$el);
            return this;
        }
    });
})();

(function() {
    Monster.Views.MapLayer = Backbone.View.extend({
        tagName: "div",
        className: "layer",

        initialize: function(attrs, options) {
            this.layer = options.layer;
        },

        render: function() {
            this.$el.css({
                backgroundImage: "url(/static/images/maps/{0}/{1}.png)".format(this.model.get("key_name"), this.layer),
                zIndex: this.layer
            });
            return this;
        }
    });
})();


(function() {
    Monster.Views.Menu = Backbone.View.extend({
        tagName: "div",
        id: "menu",

        initialize: function() {
            this.menuEntries = [
                {
                    text: "MONSTERDEX",
                    view: function() {
                        return Monster.Views.Pokedex;
                    }
                },
                {
                    text: "MONSTER",
                    view: function() {
                        return Monster.Views.MenuEntryMonsters;
                    }
                }
            ];
            this.selection = 0;
            this.visible = false;
            this.listenTo(this.model, "keypress", this.handleKeyPress);
            this.template = _.template( $("#menuTemplate").html(), { variable: "entries" });
        },

        render: function() {
            this.$el.html(this.template(this.menuEntries));
            return this;
        },

        toggleMenu: function() {
            this.selection = 0;
            this.visible = !this.visible;
            this.changeSelection();
            this.$el.toggleClass("show");
        },

        changeSelection: function() {
            this.$("li.selected").removeClass("selected");
            this.$("li[value={0}]".format(this.selection)).addClass("selected");
        },

        handleKeyPress: function(event) {
            if(event.key.toLowerCase() == "m") {
                this.toggleMenu();
            }

            if(!this.visible)
                return;

            if(event.key == "ArrowUp") {
                this.selection = Math.max(this.selection - 1, 0);
                this.changeSelection();
            }
            else if(event.key == "ArrowDown") {
                this.selection = Math.min(this.selection + 1, this.menuEntries.length - 1);
                this.changeSelection();
            }
            else if(event.key == "Enter") {
                var view = _.result(this.menuEntries[this.selection], "view");
                Monster.Vars.$windowWrapper.append(new view().render().$el);
            }
        }
    });
})();

(function() {
    Monster.Views.MenuEntry = Backbone.View.extend({
        tagName: "div",
        className: "menu-entry",
        templateId: "#basicMenuEntryTemplate",
        tabs: [ "Default" ],
        initialize: function() {
        }
    });
})();

(function() {
    Monster.Views.Player = Backbone.View.extend({
        tagName: "div",
        className: "player",

        initialize: function(attrs, options) {
            this.listenTo(this.model, "change:locked", this.setLock);
            this.listenTo(this.model, "change:x", this.setPosition);
            this.listenTo(this.model, "change:y", this.setPosition);
            this.listenTo(this.model, "change:z", this.setPosition);
            this.listenTo(this.model, "change:face", this.setFace);
            this.listenTo(this.model, "step", this.step);
            this.listenTo(this.model, "endstep", this.endStep);
        },

        render: function() {
            this.$el.css({
                transitionDuration: "{0}s".format(this.model.get("speed") / 1000)
            });

            this.setPosition();
            this.$el.addClass(this.model.get("face"));
            return this;
        },

        setLock: function() {
            if(!this.model.get("locked") && this.model.previous("locked"))
            {
                this.$el.css({
                    transitionDuration: "0s"
                });

                window.requestAnimationFrame(function() {
                    this.setPosition();
                    this.setFace();
                    window.requestAnimationFrame(function() {
                        this.$el.css({
                            transitionDuration: "{0}s".format(this.model.get("speed") / 1000)
                        });
                    }.bind(this));
                }.bind(this));
            }
        },

        setFace: function() {
            if(!this.model.get("locked")) {
                this.$el.toggleClass(this.model.get("face"), true);
                this.$el.removeClass("n s w e".replace(this.model.get("face"), ""));
            }
        },

        setPosition: function() {
            if(!this.model.get("locked"))
            {
                this.$el.css({
                    top: this.model.get("y") * 32 - 32,
                    left: this.model.get("x") * 32,
                    zIndex: this.model.get("z") + 1
                });
            }
        },

        step: function(player, direction, offset) {
            this.setFace();
            this.$el.toggleClass("moving", true);
            this.$el.toggleClass("odd", player.get("odd"));
        },

        endStep: function(player, direction) {
            if(this.model.get("isMoving"))
                this.model.move(this.model.get("direction"));
            else
                this.$el.removeClass("moving");
        },

        remove: function() {
            console.log("player.remove called");
            console.trace();
        }
    });
})();

(function() {
    Monster.Views.SceneName = Backbone.View.extend({
        tagName: "div",
        id: "sceneName",

        initialize: function() {
            this.listenTo(this.model.get("currentPlayer"), "change:scene", this.showScene);
            this.timeout = null;
        },

        render: function() {
            this.$el.append(document.createElement("span"));
            this.$("span").addClass("name");
            return this;
        },

        showScene: function() {
            var player = this.model.get("currentPlayer");
            if(player.get("scene").get("type") == "global") {
                this.$(".name").text(player.get("scene").get("name"));
                this.$el.addClass("show");
                if(this.timeout)
                    clearTimeout(this.timeout);

                this.timeout = setTimeout(function() {
                    this.$el.removeClass("show");
                }.bind(this), 2000);
            }
        }
    });
})();

(function() {
    Monster.Views.Window = Backbone.View.extend({
        tagName: "div",
        id: "window",

        initialize: function() {
            this.listenTo(this.model.get("currentPlayer"), "step", this.updateScroll);
            this.listenTo(this.model.get("currentPlayer"), "sync", this.updatePlayer);
            this.listenTo(this.model.get("currentPlayer"), "change:scene", this.changeScene);
            //this.listenTo(this.model.get("currentPlayer"), "sync", this.resetScroll);

            //this.listenTo(this.model.get("windowModel"), "change:scrollX", this.setScroll);
            //this.listenTo(this.model.get("windowModel"), "change:scrollY", this.setScroll);

            this.listenTo(this.model.get("maps"), "update", this.updateMaps);

            this.listenTo(this.model.get("players"), "add", this.addPlayer);

            this.listenTo(this.model, "change:world_width", this.render);
            this.listenTo(this.model, "change:world_height", this.render);

            this.lockMoving = false;
            this.fadingView = new Monster.Views.Fading({ model: this.model });
            this.mapsViews = [];
            this.key_to_direction = {
                w: "n",
                d: "e",
                s: "s",
                a: "w"
            };

            $(document).on("keydown", this.handleKeyDown.bind(this));
            $(document).on("keyup", this.handleKeyUp.bind(this));
        },

        render: function() {
            this.$el.append(this.fadingView.render().$el);
            this.$el.css({
                width: this.model.get("world_width") * 32,
                height: this.model.get("world_height") * 32
            });
            return this;
        },

        loadActualMap: function(player, allDoneCallback) {
            var count = 0, self = this;
            function check() {
                count++;
                if(count == 2) {
                    _.each(maps, function(map) {
                        this.$el.append(this.mapsViews[map.id].$el);
                    }, self);
                    if(allDoneCallback) allDoneCallback.call(this);
                }
            }

            this.$el.append(this.mapsViews[player.get("scene").id].$el);
            player.get("scene").fetchAll({}, check);
            this.resetScroll();

            var maps = player.get("scene").fetchClosestMaps(check);
            console.log("closest", maps, player.get("scene"));
        },

        updateMaps: function(maps, options) {
            maps.each(function(map) {
                this.mapsViews[map.id] = new Monster.Views.Map({ model: map }).render();
            }, this);
        },

        updatePlayer: function(player, response, options) {
            this.$el.append(new Monster.Views.Player({ model: player }).render().$el);
            this.loadActualMap(player);
        },

        addPlayer: function(model, collection, options) {
            this.$el.append(new Monster.Views.Player({ model: model }).render().$el);
        },

        changeScene: function(player) {
            if(!player.previous("scene"))
                return;

            if(player.get("scene").get("type") != player.previous("scene").get("type") || player.get("scene").get("type") != "global")
            {
                this.fadingView.fadeIn();
                this.lockMoving = true;
                this.fadingView.once("transitionend", function(view, event) {
                    _.each(this.mapsViews, function(mapView) {
                        if(mapView)
                            mapView.remove();
                    }, this);

                    this.loadActualMap(player, function() {
                        player.set("locked", false);
                        this.fadingView.fadeOut();
                        this.fadingView.once("transitionend", function(view, event) {
                            this.lockMoving = false;
                        }, this);
                    }.bind(this));
                }, this);
            }
            else
            {
                var maps = player.get("scene").fetchClosestMaps();
                _.each(this.mapsViews, function(mapView) {
                    if(!mapView || mapView.model == player.get("scene"))
                        return;

                    if(maps.indexOf(mapView.model) == -1) {
                        mapView.remove();
                    }
                    else {
                        this.$el.append(mapView.$el);
                    }
                }, this);
            }
        },

        updateScroll: function(player, options) {
            if(player.get("locked"))
                return;

            var windowModel = this.model.get("windowModel"),
                position = {
                    x: player.get("x") - windowModel.get("scrollX"),
                    y: player.get("y") - windowModel.get("scrollY")
                },
                toScroll = {};


            if(position.x < windowModel.get("marginX"))
                toScroll.x = position.x - windowModel.get("marginX");
            else if(position.x >= windowModel.get("sizeX") - windowModel.get("marginX"))
                toScroll.x = position.x - (windowModel.get("sizeX") - windowModel.get("marginX")) + 1;
            else
                toScroll.x = 0;

            if(position.y < windowModel.get("marginY"))
                toScroll.y = position.y - windowModel.get("marginY");
            else if(position.y >= windowModel.get("sizeY") - windowModel.get("marginY"))
                toScroll.y = position.y - (windowModel.get("sizeY") - windowModel.get("marginY")) + 1;
            else
                toScroll.y = 0;

            windowModel.set({
                scrollX: Math.max(windowModel.get("scrollX") + toScroll.x, 0),
                scrollY: Math.max(windowModel.get("scrollY") + toScroll.y, 0)
            });

            this.$el.parent().scrollTo({
                top: "+={0}px".format(toScroll.y * 32),
                left: "+={0}px".format(toScroll.x * 32)
            }, this.model.get("currentPlayer").get("speed"), {
                easing: "linear"
            });
        },

        resetScroll: function() {
            var player = this.model.get("currentPlayer"),
                windowModel = this.model.get("windowModel"),
                map = player.get("scene");

            windowModel.set({
                scrollX: Math.max(0, player.get("x") - windowModel.get("sizeX") / 2),
                scrollY: Math.max(0, player.get("y") - windowModel.get("sizeY") / 2)
            });

            this.$el.parent().scrollTop(windowModel.get("scrollY") * 32)
                             .scrollLeft(windowModel.get("scrollX") * 32);
        },

        handleKeyDown: function(event) {
            var player = this.model.get("currentPlayer");
            if(player.get("locked") || this.lockMoving)
                return;

            if(!player.get("isMoving"))
            {
                if(player.get("isStepping")) {
                    player.set({
                        isMoving: true,
                        direction: this.key_to_direction[event.key]
                    });
                }
                else {
                    player.move(this.key_to_direction[event.key]);
                }
            }
            else
                player.set("direction", this.key_to_direction[event.key]);
        },

        handleKeyUp: function(event) {
            var player = this.model.get("currentPlayer");
            if(player.get("isMoving") && this.key_to_direction[event.key] && this.key_to_direction[event.key] == player.get("direction"))
                player.set("isMoving", false);
        }
    });
})();

(function() {
    Monster.Views.MenuEntryMonsters = Backbone.View.extend({
        tagName: "div",
        id: "menu-monsters",
        initialize: function() {
            var gameModel = Monster.Vars.gameModel;
            this.listenTo(gameModel, "keypress", this.handleKeyPress);
            this.selection = 0;
            this.template = _.template( $("#menuEntryMonstersTemplate").html(), { variable: "monsters" });
        },

        render: function() {
            var player = Monster.Vars.gameModel.get("currentPlayer");
            this.$el.html(this.template(player.get("monsters").first(6)));
            this.changeSelection();
            return this;
        },

        changeSelection: function() {
            this.$("div.slot.selected").removeClass("selected");
            this.$("div.slot[slot={0}]".format(this.selection)).addClass("selected");
        },

        handleKeyPress: function(event) {
            var player = Monster.Vars.gameModel.get("currentPlayer");
            if(event.key == "Escape")
                this.remove();
            else if(event.key == "ArrowUp") {
                this.selection = Math.max(this.selection - 1, 0);
                this.changeSelection();
            }
            else if(event.key == "ArrowDown") {
                this.selection = Math.min(this.selection + 1, player.get("monsters").length - 1);
                this.changeSelection();
            }
        }
    });
})();

(function() {
    Monster.testBattleUI = function() {
        var currentPlayer = Monster.Vars.gameModel.get("currentPlayer"),
            view = new Monster.Views.FightScene({ model: currentPlayer });

        $("#windowWrapper").append(view.render().$el);
    };
})();
