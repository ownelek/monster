 (function() {
    Monster.Views.Map = Backbone.View.extend({
        tagName: "div",
        className: "world",

        initialize: function() {
            this.listenTo(this.model, "change", this.render);
        },

        render: function() {
            this.$el.css({
                top: this.model.get("y") * 32,
                left: this.model.get("x") * 32,
                width: this.model.get("sizex") * 32,
                height: this.model.get("sizey") * 32
            });

            this.$el.empty();
            for(var i = 0; i < this.model.get("maxZ"); i++)
                this.$el.append(new Monster.Views.MapLayer({ model: this.model }, { layer: i }).render().$el);
            return this;
        }
    });
})();
