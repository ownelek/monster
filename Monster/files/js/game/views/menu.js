(function() {
    Monster.Views.Menu = Backbone.View.extend({
        tagName: "div",
        id: "menu",

        initialize: function() {
            this.menuEntries = [
                {
                    text: "MONSTERDEX",
                    view: function() {
                        return Monster.Views.Pokedex;
                    }
                },
                {
                    text: "MONSTER",
                    view: function() {
                        return Monster.Views.MenuEntryMonsters;
                    }
                }
            ];
            this.selection = 0;
            this.visible = false;
            this.listenTo(this.model, "keypress", this.handleKeyPress);
            this.template = _.template( $("#menuTemplate").html(), { variable: "entries" });
        },

        render: function() {
            this.$el.html(this.template(this.menuEntries));
            return this;
        },

        toggleMenu: function() {
            this.selection = 0;
            this.visible = !this.visible;
            this.changeSelection();
            this.$el.toggleClass("show");
        },

        changeSelection: function() {
            this.$("li.selected").removeClass("selected");
            this.$("li[value={0}]".format(this.selection)).addClass("selected");
        },

        handleKeyPress: function(event) {
            if(event.key.toLowerCase() == "m") {
                this.toggleMenu();
            }

            if(!this.visible)
                return;

            if(event.key == "ArrowUp") {
                this.selection = Math.max(this.selection - 1, 0);
                this.changeSelection();
            }
            else if(event.key == "ArrowDown") {
                this.selection = Math.min(this.selection + 1, this.menuEntries.length - 1);
                this.changeSelection();
            }
            else if(event.key == "Enter") {
                var view = _.result(this.menuEntries[this.selection], "view");
                Monster.Vars.$windowWrapper.append(new view().render().$el);
            }
        }
    });
})();
