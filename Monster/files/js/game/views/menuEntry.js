(function() {
    Monster.Views.MenuEntry = Backbone.View.extend({
        tagName: "div",
        className: "menu-entry",
        templateId: "#basicMenuEntryTemplate",
        tabs: [ "Default" ],
        initialize: function() {
        }
    });
})();
