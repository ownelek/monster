(function() {
    Monster.Views.MenuEntryMonsters = Backbone.View.extend({
        tagName: "div",
        id: "menu-monsters",
        initialize: function() {
            var gameModel = Monster.Vars.gameModel;
            this.listenTo(gameModel, "keypress", this.handleKeyPress);
            this.selection = 0;
            this.template = _.template( $("#menuEntryMonstersTemplate").html(), { variable: "monsters" });
        },

        render: function() {
            var player = Monster.Vars.gameModel.get("currentPlayer");
            this.$el.html(this.template(player.get("monsters").first(6)));
            this.changeSelection();
            return this;
        },

        changeSelection: function() {
            this.$("div.slot.selected").removeClass("selected");
            this.$("div.slot[slot={0}]".format(this.selection)).addClass("selected");
        },

        handleKeyPress: function(event) {
            var player = Monster.Vars.gameModel.get("currentPlayer");
            if(event.key == "Escape")
                this.remove();
            else if(event.key == "ArrowUp") {
                this.selection = Math.max(this.selection - 1, 0);
                this.changeSelection();
            }
            else if(event.key == "ArrowDown") {
                this.selection = Math.min(this.selection + 1, player.get("monsters").length - 1);
                this.changeSelection();
            }
        }
    });
})();
