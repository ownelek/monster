(function() {
    Monster.Views.Player = Backbone.View.extend({
        tagName: "div",
        className: "player",

        initialize: function(attrs, options) {
            this.listenTo(this.model, "change:locked", this.setLock);
            this.listenTo(this.model, "change:x", this.setPosition);
            this.listenTo(this.model, "change:y", this.setPosition);
            this.listenTo(this.model, "change:z", this.setPosition);
            this.listenTo(this.model, "change:face", this.setFace);
            this.listenTo(this.model, "step", this.step);
            this.listenTo(this.model, "endstep", this.endStep);
        },

        render: function() {
            this.$el.css({
                transitionDuration: "{0}s".format(this.model.get("speed") / 1000)
            });

            this.setPosition();
            this.$el.addClass(this.model.get("face"));
            return this;
        },

        setLock: function() {
            if(!this.model.get("locked") && this.model.previous("locked"))
            {
                this.$el.css({
                    transitionDuration: "0s"
                });

                window.requestAnimationFrame(function() {
                    this.setPosition();
                    this.setFace();
                    window.requestAnimationFrame(function() {
                        this.$el.css({
                            transitionDuration: "{0}s".format(this.model.get("speed") / 1000)
                        });
                    }.bind(this));
                }.bind(this));
            }
        },

        setFace: function() {
            if(!this.model.get("locked")) {
                this.$el.toggleClass(this.model.get("face"), true);
                this.$el.removeClass("n s w e".replace(this.model.get("face"), ""));
            }
        },

        setPosition: function() {
            if(!this.model.get("locked"))
            {
                this.$el.css({
                    top: this.model.get("y") * 32 - 32,
                    left: this.model.get("x") * 32,
                    zIndex: this.model.get("z") + 1
                });
            }
        },

        step: function(player, direction, offset) {
            this.setFace();
            this.$el.toggleClass("moving", true);
            this.$el.toggleClass("odd", player.get("odd"));
        },

        endStep: function(player, direction) {
            if(this.model.get("isMoving"))
                this.model.move(this.model.get("direction"));
            else
                this.$el.removeClass("moving");
        },

        remove: function() {
            console.log("player.remove called");
            console.trace();
        }
    });
})();
