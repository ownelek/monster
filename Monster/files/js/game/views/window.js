(function() {
    Monster.Views.Window = Backbone.View.extend({
        tagName: "div",
        id: "window",

        initialize: function() {
            this.listenTo(this.model.get("currentPlayer"), "step", this.updateScroll);
            this.listenTo(this.model.get("currentPlayer"), "sync", this.updatePlayer);
            this.listenTo(this.model.get("currentPlayer"), "change:scene", this.changeScene);
            //this.listenTo(this.model.get("currentPlayer"), "sync", this.resetScroll);

            //this.listenTo(this.model.get("windowModel"), "change:scrollX", this.setScroll);
            //this.listenTo(this.model.get("windowModel"), "change:scrollY", this.setScroll);

            this.listenTo(this.model.get("maps"), "update", this.updateMaps);

            this.listenTo(this.model.get("players"), "add", this.addPlayer);

            this.listenTo(this.model, "change:world_width", this.render);
            this.listenTo(this.model, "change:world_height", this.render);

            this.lockMoving = false;
            this.fadingView = new Monster.Views.Fading({ model: this.model });
            this.mapsViews = [];
            this.key_to_direction = {
                w: "n",
                d: "e",
                s: "s",
                a: "w"
            };

            $(document).on("keydown", this.handleKeyDown.bind(this));
            $(document).on("keyup", this.handleKeyUp.bind(this));
        },

        render: function() {
            this.$el.append(this.fadingView.render().$el);
            this.$el.css({
                width: this.model.get("world_width") * 32,
                height: this.model.get("world_height") * 32
            });
            return this;
        },

        loadActualMap: function(player, allDoneCallback) {
            var count = 0, self = this;
            function check() {
                count++;
                if(count == 2) {
                    _.each(maps, function(map) {
                        this.$el.append(this.mapsViews[map.id].$el);
                    }, self);
                    if(allDoneCallback) allDoneCallback.call(this);
                }
            }

            this.$el.append(this.mapsViews[player.get("scene").id].$el);
            player.get("scene").fetchAll({}, check);
            this.resetScroll();

            var maps = player.get("scene").fetchClosestMaps(check);
            console.log("closest", maps, player.get("scene"));
        },

        updateMaps: function(maps, options) {
            maps.each(function(map) {
                this.mapsViews[map.id] = new Monster.Views.Map({ model: map }).render();
            }, this);
        },

        updatePlayer: function(player, response, options) {
            this.$el.append(new Monster.Views.Player({ model: player }).render().$el);
            this.loadActualMap(player);
        },

        addPlayer: function(model, collection, options) {
            this.$el.append(new Monster.Views.Player({ model: model }).render().$el);
        },

        changeScene: function(player) {
            if(!player.previous("scene"))
                return;

            if(player.get("scene").get("type") != player.previous("scene").get("type") || player.get("scene").get("type") != "global")
            {
                this.fadingView.fadeIn();
                this.lockMoving = true;
                this.fadingView.once("transitionend", function(view, event) {
                    _.each(this.mapsViews, function(mapView) {
                        if(mapView)
                            mapView.remove();
                    }, this);

                    this.loadActualMap(player, function() {
                        player.set("locked", false);
                        this.fadingView.fadeOut();
                        this.fadingView.once("transitionend", function(view, event) {
                            this.lockMoving = false;
                        }, this);
                    }.bind(this));
                }, this);
            }
            else
            {
                var maps = player.get("scene").fetchClosestMaps();
                _.each(this.mapsViews, function(mapView) {
                    if(!mapView || mapView.model == player.get("scene"))
                        return;

                    if(maps.indexOf(mapView.model) == -1) {
                        mapView.remove();
                    }
                    else {
                        this.$el.append(mapView.$el);
                    }
                }, this);
            }
        },

        updateScroll: function(player, options) {
            if(player.get("locked"))
                return;

            var windowModel = this.model.get("windowModel"),
                position = {
                    x: player.get("x") - windowModel.get("scrollX"),
                    y: player.get("y") - windowModel.get("scrollY")
                },
                toScroll = {};


            if(position.x < windowModel.get("marginX"))
                toScroll.x = position.x - windowModel.get("marginX");
            else if(position.x >= windowModel.get("sizeX") - windowModel.get("marginX"))
                toScroll.x = position.x - (windowModel.get("sizeX") - windowModel.get("marginX")) + 1;
            else
                toScroll.x = 0;

            if(position.y < windowModel.get("marginY"))
                toScroll.y = position.y - windowModel.get("marginY");
            else if(position.y >= windowModel.get("sizeY") - windowModel.get("marginY"))
                toScroll.y = position.y - (windowModel.get("sizeY") - windowModel.get("marginY")) + 1;
            else
                toScroll.y = 0;

            windowModel.set({
                scrollX: Math.max(windowModel.get("scrollX") + toScroll.x, 0),
                scrollY: Math.max(windowModel.get("scrollY") + toScroll.y, 0)
            });

            this.$el.parent().scrollTo({
                top: "+={0}px".format(toScroll.y * 32),
                left: "+={0}px".format(toScroll.x * 32)
            }, this.model.get("currentPlayer").get("speed"), {
                easing: "linear"
            });
        },

        resetScroll: function() {
            var player = this.model.get("currentPlayer"),
                windowModel = this.model.get("windowModel"),
                map = player.get("scene");

            windowModel.set({
                scrollX: Math.max(0, player.get("x") - windowModel.get("sizeX") / 2),
                scrollY: Math.max(0, player.get("y") - windowModel.get("sizeY") / 2)
            });

            this.$el.parent().scrollTop(windowModel.get("scrollY") * 32)
                             .scrollLeft(windowModel.get("scrollX") * 32);
        },

        handleKeyDown: function(event) {
            var player = this.model.get("currentPlayer");
            if(player.get("locked") || this.lockMoving)
                return;

            if(!player.get("isMoving"))
            {
                if(player.get("isStepping")) {
                    player.set({
                        isMoving: true,
                        direction: this.key_to_direction[event.key]
                    });
                }
                else {
                    player.move(this.key_to_direction[event.key]);
                }
            }
            else
                player.set("direction", this.key_to_direction[event.key]);
        },

        handleKeyUp: function(event) {
            var player = this.model.get("currentPlayer");
            if(player.get("isMoving") && this.key_to_direction[event.key] && this.key_to_direction[event.key] == player.get("direction"))
                player.set("isMoving", false);
        }
    });
})();
