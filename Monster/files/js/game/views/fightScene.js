    (function() {
    Monster.Views.FightScene = Backbone.View.extend({
        tagName: "div",
        id: "fight-scene",

        events: {

        },

        initialize: function() {
            this.listenTo(Monster.Vars.gameModel, "keypress", this.handleKeyPress);

            this.template = _.template( $("#fightSceneTemplate").html() );
            this.selectedDialog = 0;
            this.selectedColumn = 0;
            this.selectedRow = 0;
        },

        render: function() {
            this.$el.html(this.template(
                {
                    fighting_monster: this.model.get("monsters").find(function(monster) {
                        return monster.get("slot") === 0;
                    }),
                    player: this.model
                }
            ));
            this.renderSelection();
            return this;
        },

        renderSelection: function() {
            var dialog = ".right_dialog";
            if(this.selectedDialog)
                dialog = ".fight_dialog";

            this.$(".entry.selected").removeClass("selected");
            this.$("{0} tr:nth-child({1}) td:nth-child({2}) span.entry".format(dialog, this.selectedRow + 1, this.selectedColumn + 1)).addClass("selected");
        },

        handleKeyPress: function(event) {
            var key = event.key.toLowerCase();
            if(key == 'w') {
                this.selectedRow = Math.max(0, this.selectedRow - 1);
            }
            else if(key == "s") {
                this.selectedRow = Math.min(1, this.selectedRow + 1);
            }
            else if(key == "d") {
                this.selectedColumn = Math.min(1, this.selectedColumn + 1);
            }
            else if(key == "a") {
                this.selectedColumn = Math.max(0, this.selectedColumn - 1);
            }
            else if(key == "enter") {

            }

            this.renderSelection();
        }
    });
})();
