(function() {
    Monster.Views.Fading = Backbone.View.extend({
        tagName: "div",
        id: "fading",

        events: {
            "transitionend": function(event) {
                this.trigger("transitionend", this, event);
            }
        },
        
        render: function() {
            this.$el.css({
                width: this.model.get("world_width") * 32,
                height: this.model.get("world_height") * 32
            });
            return this;
        },

        fadeIn: function() {
            this.$el.toggleClass("active", true);
        },

        fadeOut: function() {
            this.$el.toggleClass("active", false);
        }
    });
})();
