(function() {
    Monster.Views.SceneName = Backbone.View.extend({
        tagName: "div",
        id: "sceneName",

        initialize: function() {
            this.listenTo(this.model.get("currentPlayer"), "change:scene", this.showScene);
            this.timeout = null;
        },

        render: function() {
            this.$el.append(document.createElement("span"));
            this.$("span").addClass("name");
            return this;
        },

        showScene: function() {
            var player = this.model.get("currentPlayer");
            if(player.get("scene").get("type") == "global") {
                this.$(".name").text(player.get("scene").get("name"));
                this.$el.addClass("show");
                if(this.timeout)
                    clearTimeout(this.timeout);

                this.timeout = setTimeout(function() {
                    this.$el.removeClass("show");
                }.bind(this), 2000);
            }
        }
    });
})();
