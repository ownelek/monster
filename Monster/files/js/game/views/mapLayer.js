(function() {
    Monster.Views.MapLayer = Backbone.View.extend({
        tagName: "div",
        className: "layer",

        initialize: function(attrs, options) {
            this.layer = options.layer;
        },

        render: function() {
            this.$el.css({
                backgroundImage: "url(/static/images/maps/{0}/{1}.png)".format(this.model.get("key_name"), this.layer),
                zIndex: this.layer
            });
            return this;
        }
    });
})();
