(function() {
    window.Monster = {
        Models: {},
        Collections: {},
        Views: {},
        Vars: {},

        start: function() {
            Monster.Vars.gameModel = gameModel = new Monster.Models.Game();

            var player = new Monster.Models.CurrentPlayer(),
                playerCollection = new Monster.Collections.Players([ player ]),
                mapsCollection = new Monster.Collections.Scenes(),
                moveMasksCollection = new Monster.Collections.MoveMasks(),
                entityTypesCollection = new Monster.Collections.EntityTypes(),
                entityCollection = new Monster.Collections.Entities(),
                statusEffectsCollection = new Monster.Collections.StatusEffects(),
                elementsCollection = new Monster.Collections.Elements(),
                abilitiesCollection = new Monster.Collections.Abilities(),
                monsterTypesCollection = new Monster.Collections.MonsterTypes(),
                windowModel = new Monster.Models.Window();

            gameModel.set({
                maps: mapsCollection,
                entities: entityCollection,
                players: playerCollection,
                currentPlayer: player,
                windowModel: windowModel
            });

            var sceneNameView = new Monster.Views.SceneName({ model: gameModel }),
                menuView = new Monster.Views.Menu({ model: gameModel }),
                windowView = new Monster.Views.Window({ model: gameModel });

            mapsCollection.fetch({ success: function() {
                player.fetch({ success: function() {
                    player.get("monsters").fetch();
                } });
            } });

            moveMasksCollection.fetch();
            entityTypesCollection.fetch();
            entityCollection.fetch();
            statusEffectsCollection.fetch();
            elementsCollection.fetch();
            abilitiesCollection.fetch();
            monsterTypesCollection.fetch();
            gameModel.fetchSize();
            gameModel.openConnection();

            $(window).unload(function() {
                gameModel.closeConnection();
            });

            var $windowWrapper = $("#windowWrapper"),
                $document = $(document);

            Monster.Vars.gameModel = gameModel;
            Monster.Vars.$windowWrapper = $windowWrapper;

            $("#windowView").append(windowView.render().$el);
            $windowWrapper.append(sceneNameView.render().$el);
            $windowWrapper.append(menuView.render().$el);

            $document.keydown(function(event) {
                gameModel.trigger("keydown", event);
            });

            $document.keyup(function(event) {
                gameModel.trigger("keyup", event);
            });

            $document.keypress(function(event) {
                gameModel.trigger("keypress", event);
            });
        }
    };
})();
