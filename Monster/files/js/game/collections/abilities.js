(function() {
    Monster.Collections.Abilities = Backbone.Collection.extend({
        model: Monster.Models.Ability,
        url: "/api/abilities/"
    });
})();
