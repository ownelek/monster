(function() {
    Monster.Collections.Tiles = Backbone.Collection.extend({
        model: Monster.Models.Tile,
        url: function() {
            return "/api/maps/{0}/tiles/".format(this.scene.id);
        }
    });
})();
