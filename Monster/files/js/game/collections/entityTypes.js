(function() {
    Monster.Collections.EntityTypes = Backbone.Collection.extend({
        model: Monster.Models.EntityType,
        url: "/api/entitytypes/"
    });
})();
