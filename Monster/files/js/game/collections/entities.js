(function() {
    Monster.Collections.Entities = Backbone.Collection.extend({
        model: Monster.Models.Entity,
        url: function() {
            if(this.scene)
                return "/api/maps/{0}/entities/".format(this.scene.id);

            return "/api/entities/";
        }
    });
})();
