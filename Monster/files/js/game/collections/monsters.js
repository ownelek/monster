(function() {
    Monster.Collections.Monsters = Backbone.Collection.extend({
        model: Monster.Models.Monster,

        url: function() {
            return this.owner ? "/api/players/{0}/monsters/".format(this.owner.id) : "/api/monsters/";
        }
    });
})();
