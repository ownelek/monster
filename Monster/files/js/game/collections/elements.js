(function() {
    Monster.Collections.Elements = Backbone.Collection.extend({
        model: Monster.Models.Element,
        url: "/api/elements/"
    });
})();
