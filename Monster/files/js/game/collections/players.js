(function() {
    Monster.Collections.Players = Backbone.Collection.extend({
        model: Monster.Models.Player,
        url: function() {
            return "/api/players/";
        }
    });
})();
