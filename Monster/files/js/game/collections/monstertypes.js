(function() {
    Monster.Collections.MonsterTypes = Backbone.Collection.extend({
        model: Monster.Models.MonsterType,
        url: "/api/monstertypes/"
    });
})();
