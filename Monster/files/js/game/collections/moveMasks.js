(function() {
    Monster.Collections.MoveMasks = Backbone.Collection.extend({
        model: Monster.Models.MoveMask,
        url: function() {
            return "/api/movemasks/";
        }
    });
})();
