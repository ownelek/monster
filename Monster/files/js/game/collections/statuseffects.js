(function() {
    Monster.Collections.StatusEffects = Backbone.Collection.extend({
        model: Monster.Models.StatusEffect,
        url: "/api/status_effects/"
    });
})();
