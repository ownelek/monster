(function() {
    Monster.Collections.Scenes = Backbone.Collection.extend({
        model: Monster.Models.Scene,
        url: function() {
            return "/api/maps/";
        }
    });
})();
