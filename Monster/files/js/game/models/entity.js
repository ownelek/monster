(function() {
    Monster.Models.Entity = Backbone.RelationalModel.extend({
        getProperty: function(key) {
            return _.find(this.get("properties"), function(prop) {
                return key == prop.key;
            });
        },
        subModelTypes: {
            1: "Monster.Models.EntityEnterScene",
        },
        subModelTypeAttribute: "type",
        relations: [{
            type: Backbone.HasOne,
            key: "type",
            relatedModel: "Monster.Models.EntityType"
        }]
    });
})();
