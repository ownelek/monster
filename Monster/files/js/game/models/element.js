(function() {
    Monster.Models.Element = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasMany,
                key: "strong_against"
            },
            {
                type: Backbone.HasMany,
                key: "weak_against"
            }
        ]
    });
})();
