(function() {
    Monster.Models.Player = Backbone.RelationalModel.extend({
        defaults: {
            odd: false,
            speed: 200,
            direction_to_offset: {
                n: { x: 0, y: -1 },
                w: { x: -1, y: 0 },
                e: { x: 1, y: 0 },
                s: { x: 0, y: 1 }
            },
            locked: false
        },

        initialize: function() {
            this.game = Monster.Vars.gameModel;
            this.listenTo(this.game, "socket:received", this.received);
            this.listenTo(this, "change:x", this.checkCollisions);
            this.listenTo(this, "change:y", this.checkCollisions);
        },

        received: function(gameModel, data) {
            if(data.action == "player.step" && data.playerId == this.id)
            {
                this.move(data.direction);
            }
        },

        checkCollisions: function() {
            this.get("scene").get("entities").each(function(entity) {
                if(entity.get("x") == this.get("x") && entity.get("y") == this.get("y") && entity.get("z") == this.get("z"))
                    entity.onPlayerCollide(this);
            }, this);
        },

        move: function(direction) {
            if(!this.game.get("connected"))
                return;

            var direction_to_offset = this.get("direction_to_offset");

            if(_.keys(direction_to_offset).indexOf(direction) == -1 || this.get("locked"))
                return;

            var map = this.get("scene"),
                offset = direction_to_offset[direction];

            if(this.get("x") + offset.x < map.get("x") || this.get("y") + offset.y < map.get("y") ||
                this.get("x") + offset.x >= map.get("x") + map.get("sizex") || this.get("y") + offset.y >= map.get("y") + map.get("sizey")) {

                var foundMap = map.collection.find(function(map) {
                    return this.get("x") + offset.x >= map.get("x") && this.get("y") + offset.y >= map.get("y") &&
                        this.get("x") + offset.x < map.get("x") + map.get("sizex") && this.get("y") + offset.y < map.get("y") + map.get("sizey");
                }, this);

                if(foundMap) {
                    this.set("scene", foundMap);
                }
                else {
                    return;
                }
            }

            this.set({
                odd: !this.get("odd"),
                face: direction,
                x: this.get("x") + direction_to_offset[direction].x,
                y: this.get("y") + direction_to_offset[direction].y
            });

            this.trigger("step", this, direction, direction_to_offset[direction]);

            setTimeout(function() {
                this.trigger("endstep", this, direction);
            }.bind(this), this.get("speed"));
        },

        relations: [
            {
                type: Backbone.HasOne,
                key: "scene",
                relatedModel: "Monster.Models.Scene",
                includeInJSON: true,
                reverseRelation: {
                    key: "players",
                    collectionType: "Monster.Collections.Players"
                }
            },
            {
                type: Backbone.HasMany,
                key: "monsters",
                relatedModel: "Monster.Models.Monster",
                collectionType: "Monster.Collections.Monsters",
                includeInJSON: "id",
                reverseRelation: {
                    key: "owner"
                }
            }
        ]
    });
})();
