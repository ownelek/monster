(function() {
    Monster.Models.Window = Backbone.Model.extend({
        defaults: {
            scrollX: 0,
            scrollY: 0,
            sizeX: 16,
            sizeY: 16,
            marginX: 3,
            marginY: 3
        },

        initialize: function() {
            this.game = Monster.Vars.gameModel;
            this.listenTo(this.game, "socket:received", this.handleSocket);
        },

        handleSocket: function(game, data) {
            if(data.action == "player.connected") {
                this.game.get("players").add(data.player);
            }
            else if(data.action == "player.disconnected") {
                this.game.get("players").remove(data.player);
            }
        }
    });
})();
