(function() {
    Monster.Models.MoveMask = Backbone.RelationalModel.extend({
        canEnter: function() {
            return true;
        },

        onEnter: function() {},
        subModelTypes: {
            "Move": "Monster.Models.NoBlockMoveMask",
            "Block": "Monster.Models.BlockMoveMask",
            "Wild": "Monster.Models.WildMoveMask"
        },
        subModelTypeAttribute: "key_name"
    });
})();
