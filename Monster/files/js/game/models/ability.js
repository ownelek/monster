(function() {
    Monster.Models.Ability = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "element_type",
                relatedModel: "Monster.Models.Element"
            },
            {
                type: Backbone.HasOne,
                key: "status_effect",
                relatedModel: "Monster.Models.StatusEffect"
            }
        ]
    });
})();
