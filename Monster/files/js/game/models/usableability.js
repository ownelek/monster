(function() {
    Monster.Models.UsableAbility = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "monster",
                relatedModel: "Monster.Models.Monster",
                includeInJSON: "id",
                reverseRelation: {
                    key: "abilities"
                }
            },
            {
                type: Backbone.HasOne,
                key: "ability",
                relatedModel: "Monster.Models.Ability",
                includeInJSON: "id"
            }
        ]
    });
})();
