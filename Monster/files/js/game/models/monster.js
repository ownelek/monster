(function() {
    Monster.Models.Monster = Backbone.RelationalModel.extend({
        getMaxHealth: function() {
            return this.get("monster_type").get("base_health") + this.get("monster_type").get("health_growth") * this.get("level");
        },
        getAttack: function() {
            return this.get("monster_type").get("base_attack") + this.get("monster_type").get("attack_growth") * this.get("level");
        },
        getDefense: function() {
            return this.get("monster_type").get("base_defense") + this.get("monster_type").get("defense_growth") * this.get("level");
        },
        getSpeed: function() {
            return this.get("monster_type").get("base_speed") + this.get("monster_type").get("speed_growth") * this.get("level");
        },
        relations: [
            {
                type: Backbone.HasOne,
                key: "monster_type",
                relatedModel: "Monster.Models.MonsterType"
            }
        ]
    });
})();
