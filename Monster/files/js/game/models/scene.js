(function() {
    Monster.Models.Scene = Backbone.RelationalModel.extend({
        initialize: function() {
            this.positionMap = [];
            this.listenTo(this.get("tiles"), "update", this.updatePositionMap);
            this._fetched = false;
        },

        distance: function(otherMap) {
            var center = [ this.get("x") + this.get("sizex") / 2, this.get("y") + this.get("sizey") / 2 ],
                otherCenter = [ otherMap.get("x") + otherMap.get("sizex") / 2, otherMap.get("y") + otherMap.get("sizey") / 2 ];

            return Math.sqrt(Math.pow(center[0] - otherCenter[0], 2.0) + Math.pow(center[1] - otherCenter[1], 2.0));
        },

        fetchLayersImage: function(options) {
            $.get(this.url() + "/image/", function(data) {
                this.set("layers", data);
                if(options && options.success) options.success.call(this, data);
            }.bind(this), "json");
        },

        fetchClosestMaps: function(allDoneCallback) {
            if(this.get("type") != "global")
            {
                if(allDoneCallback) allDoneCallback.call(this);
                return []; // dont fetch any maps if we are not in open space
            }

            var closestMaps = this.collection.reject(function(map) {
                return map.get("type") != "global" || map == this ||
                    this.distance(map) > (Math.sqrt(Math.pow(this.get("sizex"), 2.0) + Math.pow(this.get("sizey"), 2.0)) / 2) +
                        (Math.sqrt(Math.pow(map.get("sizex"), 2.0) + Math.pow(map.get("sizey"), 2.0)) / 2);
            }, this).sort(function(a, b) {
                    return this.distance(a) - this.distance(b);
            }.bind(this));

            var count = 0;
            function check() {
                count++;
                if(count == closestMaps.length && allDoneCallback)
                    allDoneCallback.call(this);
            }

            _.each(closestMaps, function(map) {
                map.fetchAll({}, check);
            });

            return closestMaps;
        },

        fetchAll: function(options, allDoneCallback) {
            if(this._fetched)
            {
                allDoneCallback.call(this);
                return;
            }

            var success = options.success, count = 0;
            options.success = function() {
                count++;
                if(success) success.apply(this, arguments);
                if(allDoneCallback && count == 3) allDoneCallback.call(this);
            };

            this.get("tiles").fetch(options);
            this.get("entities").fetch(options);
            this.fetchLayersImage(options);
            this._fetched = true;
        },

        updatePositionMap: function(collection, options) {
            collection.each(function(tile) {
                this.positionMap[this.get("sizex") * this.get("sizey") * tile.get("z") + this.get("sizex") * tile.get("y") + tile.get("x")] = tile;
            }, this);
        },

        relations: [
            {
                type: Backbone.HasMany,
                key: "tiles",
                relatedModel: "Monster.Models.Tile",
                collectionType: "Monster.Collections.Tiles",
                reverseRelation: {
                    key: "scene"
                }
            },
            {
                type: Backbone.HasMany,
                key: "entities",
                relatedModel: "Monster.Models.Entity",
                collectionType: "Monster.Collections.Entities",
                reverseRelation: {
                    key: "scene"
                }
            }
        ]
    });
})();
