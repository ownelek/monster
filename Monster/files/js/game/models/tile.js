(function() {
    Monster.Models.Tile = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "moveMask",
                keySource: "move_mask",
                relatedModel: "Monster.Models.MoveMask"
            }
        ]
    });
})();
