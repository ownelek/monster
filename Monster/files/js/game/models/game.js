(function() {
    Monster.Models.Game = Backbone.Model.extend({
        defaults: {
            connected: false
        },
        url: "ws://127.0.0.1:80/game",
        initialize: function(attrs, options) {

        },

        openConnection: function() {
            var socket = new WebSocket(_.result(this, "url"));
            this.set("socket", socket);
            socket.onopen = function() {
                console.log("Connected!");
                
                this.set("connected", true);
                this.trigger("socket:connected", this);
            }.bind(this);

            socket.onmessage = function(event) {
                this.trigger("socket:received", this, event);
            }.bind(this);

            socket.onclose = function() {
                this.set("connected", false);
                this.trigger("socket:closed", this);

                // try to reconnect
                console.log("Lost connection");
                console.log("Reconnecting in 5");
                setTimeout(this.openConnection.bind(this), 5000);
            }.bind(this);
        },

        sendMessage: function(message) {
            this.get("socket").send(JSON.stringify(message));
        },

        closeConnection: function() {
            this.get("socket").close();
        },

        fetchSize: function() {
            $.get("/api/maps/size/", function(data) {
                this.set(data);
            }.bind(this), "json");
        }
    });
})();
