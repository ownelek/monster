(function() {
    Monster.Models.LearnableAbility = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "ability",
                relatedModel: "Monster.Models.Ability",
                includeInJSON: "id"
            },
            {
                type: Backbone.HasOne,
                key: "monster_type",
                relatedModel: "Monster.Models.MonsterType",
                includeInJSON: "id",
                reverseRelation: {
                    key: "abilities"
                }
            }
        ]
    });
})();
