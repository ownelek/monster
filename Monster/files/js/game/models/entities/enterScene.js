(function() {
    Monster.Models.EntityEnterScene = Monster.Models.Entity.extend({
        onTick: function() {

        },

        onPlayerCollide: function(player) {
            console.log("enterscene");
            var entities = Monster.Vars.gameModel.get("entities"),
                scene = player.get("scene"),
                ent = entities.get(parseInt(this.getProperty("destination").value)),
                opposite_direction = {
                    "n": "s",
                    "s": "n",
                    "e": "w",
                    "w": "e"
                };

            if(ent) {
                player.set("locked", true);
                player.set({
                    scene: ent.get("scene"),
                    x: ent.get("x") + player.get("direction_to_offset")[opposite_direction[ent.get("face")]].x,
                    y: ent.get("y") + player.get("direction_to_offset")[opposite_direction[ent.get("face")]].y,
                    z: ent.get("z"),
                    face: opposite_direction[ent.get("face")],
                    isMoving: false
                });
            }
            else {
                console.log("Destination not exists");
            }
        }
    });
})();
