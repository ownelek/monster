(function() {
    Monster.Models.MonsterType = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: "element_type",
                relatedModel: "Monster.Models.Element"
            },
            {
                type: Backbone.HasOne,
                key: "evolution"
            }
        ]
    });
})();
