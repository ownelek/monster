(function() {
    Monster.testBattleUI = function() {
        var currentPlayer = Monster.Vars.gameModel.get("currentPlayer"),
            view = new Monster.Views.FightScene({ model: currentPlayer });

        $("#windowWrapper").append(view.render().$el);
    };
})();
