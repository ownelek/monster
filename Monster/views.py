from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
import json


@ensure_csrf_cookie
@csrf_protect
def play(request):
    return render(request, "index.html")


@ensure_csrf_cookie
@csrf_protect
def editor(request):
    return render(request, "mapedit.html")


def unauthorized(request):
    return HttpResponse(json.dumps({"error": "Unauthorized access"}))


def check(request):
    return HttpResponse(request.user)


def test(request):
    return render(request, "create_test.html")

def pma(request):
    return redirect("http://127.0.0.1:8080/phpmyadmin/", permanent=True)

@ensure_csrf_cookie
@csrf_protect
def login_view(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(play)
            else:
                return render(request, "login.html", {"error": "Disabled account"})

        else:
            return render(request, "login.html", {"error": "Invalid username/password"})
    if request.user.is_authenticated():
        return render(request, "login.html", {"error": "Jestes zalogowany"})

    return render(request, "login.html")


def logout_view(request):
    if request.user.is_authenticated():
        logout(request)
    return HttpResponse("done")
