__author__ = 'Czarek'

from datetime import datetime, timedelta

import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import tornado.wsgi

import os
import json
from Monster import wsgi

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Monster.settings")

import django

from importlib import import_module
from django.conf import settings
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore

from django.contrib.auth.models import User, UserManager
from django.db import connection
from django.dispatch import receiver

from map.models import *
from map.serializers import PlayerSerializer
from map.signals import monster_encountered

from game import actions

class Server:
    players = []
    step_time = timedelta(milliseconds=400)
    actions = {}

    def __init__(self, *args, **kwargs):
        print("Preparing server")
        wsgi_django = tornado.wsgi.WSGIContainer(wsgi.application)
        self.application = tornado.web.Application([
            (r'/game', PlayerHandler, { "server": self }),
            (r".*", tornado.web.FallbackHandler, {"fallback": wsgi_django})
        ], debug=True, static_path="Monster/files/", static_url_prefix="/static/")

        print("Preparing player actions")
        for key, value in actions.__dict__.items():
            if "Command" in key:
                action = getattr(actions, key)
                self.actions[action.command] = action()


    def start(self):
        self.http_server = tornado.httpserver.HTTPServer(self.application)
        self.http_server.listen(80)
        try:
            tornado.ioloop.IOLoop.instance().start()
        except KeyboardInterrupt:
            tornado.ioloop.IOLoop.instance().stop()

class MessageData:
    def __init__(self, data):
        self._data = data

    def __getattr__(self, name):
        return self._data[name] if name in self._data else None

class PlayerHandler(tornado.websocket.WebSocketHandler):
    locked = False
    fighting_monster = None
    encountered_monster = None
    last_step_time = datetime.now()
    player = None

    def get_current_user(self):
        self.session = SessionStore(session_key=self.get_cookie("sessionid"))
        try:
            return User.objects.get(id=self.session.get("_auth_user_id"))
        except User.DoesNotExist:
            return None

    def initialize(self, **kwargs):
        self.player = Player.objects.get(user=self.current_user)
        self.server = kwargs.pop("server", None)

    def open(self):
        if not self.current_user:
            self.close()
            return

        #print(self.request)
        self.write_message({"return": "Welcome"})

        self.server.players.append(self)

        print("Player", self.player.nickname, "connected")
        for handler in self.server.players:
            if handler != self:
                handler.write_message({
                    "action": "player.connected",
                    "player": PlayerSerializer(self.player).data
                })

    @receiver(monster_encountered)
    def on_monster_encounter(self, sender, **kwargs):
        if sender == self.player:
            self.write_message({
                "action": "player.monster_encounter",
                "monster": kwargs['monster']
            })
            self.locked = True
            self.fighting_monster = self.player.monsters.get(slot=0)
            self.encountered_monster = kwargs['monster']

    def on_message(self, message):
        #print(self)
        data = MessageData(json.loads(message))
        try:
            self.server.actions[data.action].execute(self, data)
        except KeyError:
            print("Invalid action", data.action)

    def on_close(self):
        try:
            self.server.players.remove(self)
        except ValueError:
            pass

        if self.player:
            self.player.save()

            for handler in self.server.players:
                handler.write_message({
                    "action": "player.disconnected",
                    "player": self.player.id
                })
            print("Player", self.player.nickname, "disconnected")

    def check_origin(self, origin):
        return True


if __name__ == "__main__":
    django.setup()
    print("Django Setup Done")
    server = Server()
    print("Starting server.")
    server.start()
    print("Closing")
