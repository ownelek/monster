from map.models import UsableAbility
from datetime import datetime, timedelta

class Command:
    command = 'invalid.command'

    def execute(self):
        pass

class PlayerStepCommand(Command):
    command = 'player.step'
    step_time = timedelta(milliseconds=200)

    def execute(self, player, data):
        if player.last_step_time + self.step_time <= datetime.now() and not player.locked:
            player.last_step_time = datetime.now()
            player.player.move(data.direction)

            for handler in player.server.players:
                handler.write_message({
                    "action": "player.step",
                    "playerId": player.player.id,
                    "direction": data.direction
                })

class PlayerMonsterAttackCommand(Command):
    command = 'player.monster_attack'

    def execute(self, player, data):
        if player.player.state == 'fight' and player.fighting_monster is not None:
            zero_pp, missed, damage, critical = player.fighting_monster.use_ability(UsableAbility.objects.get(id=data.ability_id), player.encountered_monster)
            if not zero_pp:
                if player.encountered_monster.health <= 0:
                    player.write_message({
                        "action": "monster.damage_dealt",
                        "monsterId": player.encountered_monster.id,
                        "missed": missed,
                        "damage": damage,
                        "critical": critical
                    })

                    player.write_message({
                        "action": "monster.gain_experience",
                        "monsterId": player.fighting_monster.id,
                        "experience": player.encountered_monster.takedown_experience
                    })

                    player.fighting_monster.experience += player.encountered_monster.takedown_experience

                    if player.fighting_monster.experience >= player.fighting_monster.next_level_experience:
                        player.fighting_monster.experience -= player.fighting_monster.next_level_experience
                        player.fighting_monster.level += 1

                    player.fighting_monster.save()
                    player.encountered_monster.delete()

                    player.player.state = 'walk'
                    player.player.save()
                else:
                    player.write_message({
                        "action": "monster.damage_dealt",
                        "monsterId": player.encountered_monster.id,
                        "missed": missed,
                        "damage": damage,
                        "critical": critical
                    })

                    zero_pp, missed, damage, critical = player.encountered_monster.use_ability(
                        random.choice(UsableAbility.objects.filter(monster=player.encountered_monster)), player.fighting_monster)

                    player.write_message({
                        "action": "monster.damage_dealt",
                        "monsterId": player.fighting_monster.id,
                        "missed": missed,
                        "damage": damage,
                        "critical": critical
                    })

                    player.encountered_monster.save()

                    if player.fighting_monster.health <= 0:
                        player.encountered_monster.delete()
