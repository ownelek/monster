import datetime

from tornado.websocket import WebSocketHandler

from importlib import import_module
from django.conf import settings
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore

from player.models import Player

class Game:
    players = {}
    def __init__(self):
        wsgi_django = tornado.wsgi.WSGIContainer(wsgi.application)
        application = tornado.web.Application([
            (r'/game', PlayerHandler),
            (r".*", tornado.web.FallbackHandler, {"fallback": wsgi_django})
        ], debug=True, static_path="Monster/files/", static_url_prefix="/static/")

        http_server = tornado.httpserver.HTTPServer(application)

    def get_player(self, handler):
        return self.players[handler.get_cookie("sessionid")]

    def add_player(self, handler):
        self.players[handler.get_cookie("sessionid")] = Player(handler.get_cookie("sessionid"), handler)

    def remove_player(self, handler):
        del self.player[handler.get_cookie("sessionid")]
