from map.models import MoveMask, Monster
from map.signals import monster_encountered

import random

class MoveMoveMask(MoveMask):
    def can_enter(self, player):
        return True

    def on_enter(self, player):
        pass

    class Meta:
        proxy = True

class BlockMoveMask(MoveMask):
    def can_enter(self, player):
        return False

    def on_enter(self, player):
        pass

    class Meta:
        proxy = True

class WildMoveMask(MoveMask):
    def can_enter(self, player):
        return True

    def on_enter(self, player):
        if random.randrange(100) > self.scene.encounter_ratio:
            return

        monsters = tuple(self.scene.monsters.filter(type='grass'))
        max_freq = 0
        for monster in monsters:
            max_freq = monster.frequency

        rand = random.randrange(max_freq)

        for monster in monsters:
            if rand > monster.frequency:
                rand -= monster.frequency
            else:
                rmonster = Monster(name=monster.monster.name, monster_type=monster, health=0, level=random.randrange(monster.min_level, monster.max_level))
                rmonster.health = rmonster.max_health
                rmonster.save()
                player.state = 'fight'
                monster_encountered.send(sender=player, monster=rmonster)
                break

    class Meta:
        proxy = True
