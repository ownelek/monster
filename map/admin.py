from django.contrib import admin
from map.models import Tile, TileType

admin.site.register(Tile)
admin.site.register(TileType)
