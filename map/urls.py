from django.conf.urls import patterns, url, include

from map import views
from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

router = routers.DefaultRouter()
router.register("players", views.PlayerViewSet)
router.register("maps", views.MapViewSet, base_name="maps")
router.register("tilesets", views.TileSetViewSet)
router.register("tiletypes", views.TileTypeViewSet)
router.register("movemasks", views.MoveMaskViewSet)
router.register("entitytypes", views.EntityTypeViewSet)
router.register("entities", views.EntityViewSet)
router.register("status_effects", views.StatusEffectViewSet)
router.register("elements", views.ElementViewSet)
router.register("abilities", views.AbilityViewSet)
router.register("monstertypes", views.MonsterTypeViewSet)
router.register("monsters", views.MonsterViewSet)

maps_router = NestedSimpleRouter(router, r"maps", lookup="map")
maps_router.register("tiles", views.TileViewSet, base_name="tiles")
maps_router.register("entities", views.MapEntityViewSet, base_name="entities")

player_router = NestedSimpleRouter(router, r"players", lookup="player")
player_router.register("monsters", views.PlayerMonsterViewSet)

urlpatterns = patterns('',
    url(r"^", include(router.urls)),
    url(r"^", include(maps_router.urls)),
    url(r"^", include(player_router.urls)),
)
