import traceback

from django.db.models import F

from map.models import *
from rest_framework import serializers

class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = "__all__"

class TileSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = TileSet
        fields = "__all__"

class TileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tile
        fields = "__all__"

class TileTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TileType
        fields = "__all__"

class MoveMaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = MoveMask
        fields = "__all__"

class MonsterInMapSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = MonsterInMap
        fields = "__all__"

class SceneSerializer(serializers.ModelSerializer):
    maxZ = serializers.IntegerField(read_only=True, min_value=0)
    monsters = MonsterInMapSerializer(many=True)

    def update(self, instance, validated_data):
        offsetX = validated_data['x'] - instance.x
        offsetY = validated_data['y'] - instance.y

        monsters_in_map_data = validated_data.pop("monsters")

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        Tile.objects.filter(scene=instance).update(x=F("x") + offsetX, y=F("y") + offsetY)

        for monster in monsters_in_map_data:
            mid = monster.pop('id', None)
            if mid:
                obj = MonsterInMap.objects.get(id=mid)
                if monster['monster'].id == 0:
                    obj.delete()
                else:
                    for attr, value in monster.items():
                        setattr(obj, attr, value)
                    obj.save()
            else:
                MonsterInMap.objects.create(**monster)

        return instance

    class Meta:
        model = Scene
        exclude = ("wild_monsters",)

class EntityTypePropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = EntityTypeProperty
        fields = "__all__"

class EntityTypeSerializer(serializers.ModelSerializer):
    properties = EntityTypePropertySerializer(many=True)
    class Meta:
        model = EntityType
        fields = "__all__"

class EntityPropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = EntityProperty
        exclude = ("entity",)

class EntitySerializer(serializers.ModelSerializer):
    properties = EntityPropertySerializer(many=True)

    def create(self, validated_data):
        props_data = validated_data.pop("properties")
        entity = Entity.objects.create(**validated_data)
        defaults = entity.type.get_defaults()

        for index, prop in enumerate(props_data):
            prop['entity'] = entity
            if prop['key'] in defaults:
                prop['value'] = prop['value'] or defaults[prop['key']]
            else:
                del props_data[index] # dont add properties not mentioned in defaults

        EntityProperty.objects.bulk_create([EntityProperty(**prop) for prop in props_data])
        return entity

    def update(self, instance, validated_data):
        props_data = validated_data.pop("properties")
        props = instance.properties.all()
        #print(instance.properties.all())
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()

        for prop in props_data:
            #print(prop)
            o = props.get(key=prop['key'])
            o.value = prop['value']
            o.save()

        return instance

    class Meta:
        model = Entity
        fields = "__all__"

class StatusEffectSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = StatusEffect

class ElementSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Element

class AbilitySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Ability

class LearnableAbilitySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = LearnableAbility

class UsableAbilitySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = UsableAbility

class MonsterTypeSerializer(serializers.ModelSerializer):
    abilities = LearnableAbilitySerializer(many=True)
    class Meta:
        exclude = ("abilities_set",)
        model = MonsterType

class MonsterSerializer(serializers.ModelSerializer):
    abilities = UsableAbilitySerializer(many=True)
    class Meta:
        exclude = ("abilities_set",)
        model = Monster
