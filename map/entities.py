from map.models import *

class EnterScene(Entity):
    def on_tick(self):
        pass

    def on_player_collide(self, player):
        print("EnterScene Collision")
        opposite_direction = {
            "s": "n",
            "n": "s",
            "e": "w",
            "w": "e"
        }
        try:
            dest_ent = Entity.objects.get(id=int(self.get_property("destination")))
            player.scene = dest_ent.scene
            player.x = dest_ent.x
            player.y = dest_ent.y
            player.z = dest_ent.z
            player.move(opposite_direction[dest_ent.face])
        except Entity.DoesNotExist:
            print("Destination not exists")

    class Meta:
        proxy = True

name_to_class = {
    "enter_scene": EnterScene
}
