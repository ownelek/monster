from django.conf import settings
from django.db import models

import random

direction_to_offset = {
    "n": (0, -1),
    "e": (1, 0),
    "w": (-1, 0),
    "s": (0, 1),
}

class EntityType(models.Model):
    name = models.CharField(max_length=32, unique=True)
    sprite = models.CharField(max_length=64, null=False)
    visible = models.BooleanField(null=False, default=False)
    sizex = models.IntegerField(null=False)
    sizey = models.IntegerField(null=False)

    def get_defaults(self):
        ret = {}
        for prop in self.properties.all():
            ret[prop.name] = prop.default

        return ret

class EntityTypeProperty(models.Model):
    entity_type = models.ForeignKey(EntityType, related_name="properties")
    name = models.CharField(max_length=32, null=False)
    default = models.CharField(max_length=64)

    class Meta:
        unique_together = ("entity_type", "name")

class Scene(models.Model):
    key_name = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=32)
    type = models.CharField(max_length=32, choices=(
        ("global", "Global"),
        ("room", "Room"),
        ("cave", "Cave")
    ))
    x = models.IntegerField(blank=False)
    y = models.IntegerField(blank=False)
    sizex = models.IntegerField(null=False)
    sizey = models.IntegerField(null=False)
    linked_scenes = models.ManyToManyField("self", related_name="linked_scenes")
    wild_monsters = models.ManyToManyField("MonsterType", through="MonsterInMap")
    encounter_ratio = models.IntegerField(null=False)

    def __str__(self):
        return "%s (%s, %i) [%i, %i]" % (self.name, self.key_name, self.id, self.sizex, self.sizey)

class MonsterInMap(models.Model):
    monster = models.ForeignKey("MonsterType")
    scene = models.ForeignKey(Scene, related_name="monsters")
    type = models.CharField(max_length=16, choices=(
        ("grass", "Grass"),
        ("water", "Water")
    ))
    min_level = models.IntegerField(null=False)
    max_level = models.IntegerField(null=False)
    frequency = models.IntegerField(null=False)

class Entity(models.Model):
    type = models.ForeignKey(EntityType)
    scene = models.ForeignKey(Scene, related_name="entities")
    face = models.CharField(max_length=1, null=False, choices=(
        ("n", "North"),
        ("w", "West"),
        ("e", "East"),
        ("s", "South")
    ))
    x = models.IntegerField(null=False)
    y = models.IntegerField(null=False)
    z = models.IntegerField(null=False)

    @classmethod
    def from_db(cls, db, field_names, values):
        from map import entities
        klass = entities.name_to_class[EntityType.objects.get(id=values[1]).name]
        if cls._deferred:
            instance = klass(**zip(field_names, values))
        else:
            instance = klass(*values)
        instance._state.adding = False
        instance._state.db = db
        # customization to store the original field values on the instance
        instance._loaded_values = dict(zip(field_names, values))
        return instance

    def get_property(self, name):
        return self.properties.get(key=name).value

    def __str__(self):
        return "Entity(type={type}, scene={scene}, face={face}, x={x}, y={y}, z={z}, properties={properties})".format(
            type=self.type,
            scene=self.scene,
            face=self.face,
            x=self.x,
            y=self.y,
            z=self.z,
            properties=self.properties
        )

class EntityProperty(models.Model):
    entity = models.ForeignKey(Entity, related_name="properties")
    key = models.CharField(max_length=32, null=False)
    value = models.CharField(max_length=64, null=False)

    def __str__(self):
        return "EntityProperty(key={key}, value={value})".format(
            key=self.key,
            value=self.value
        )

class TileSet(models.Model):
    name = models.CharField(max_length=32, unique=True)
    path = models.CharField(max_length=128)

    def __str__(self):
        return "TileSet(id={id}, name={name}, path={path})".format(id=self.id, name=self.name, path=self.path)

class TileType(models.Model):
    tileset = models.ForeignKey(TileSet, related_name="tile_types")
    key_name = models.CharField(max_length=32, unique=True)
    x = models.IntegerField(blank=False)
    y = models.IntegerField(blank=False)

    def __str__(self):
        return "TileType(id={id}, key_name={key_name}, x={x}, y={y})".format(id=self.id, key_name=self.key_name, x=self.x, y=self.y)

class Sprite(models.Model):
    name = models.CharField(max_length=32, unique=True)
    file = models.ImageField(upload_to='sprites')

    def serializable(self):
        return {"id": self.id, "name": self.name, "filepath": self.file.path, "fileurl": self.file.url }


class Player(models.Model):
    nickname = models.CharField(blank=False, null=False, max_length=32, unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    x = models.IntegerField(null=False, blank=False)
    y = models.IntegerField(null=False, blank=False)
    z = models.IntegerField(null=False, blank=False)
    scene = models.ForeignKey(to=Scene, related_name="players")
    state = models.CharField(max_length=16, choices=(
        ("walk", "Walking"),
        ("fight", "Fighting")
    ), default="walk")
    sprite = models.ForeignKey(to=Sprite)
    face = models.CharField(
        default="n", blank=False,
        choices=(
            ("n", "North"),
            ("e", "East"),
            ("w", "West"),
            ("s", "South")
        ), max_length=1
    )

    last_login = models.DateTimeField(auto_now_add=True, null=True)

    def move(self, direction):
        offset = [0, 0]
        oscene = self.scene

        try:
            offset = direction_to_offset[direction]
        except:
            pass

        if (self.x + offset[0] >= self.scene.x + self.scene.sizex or self.y + offset[1] >= self.scene.y + self.scene.sizey or
            self.x + offset[0] < self.scene.x or self.y + offset[1] < self.scene.y):
            if self.scene.type == 'global':
                print("hi crossed")
                oscene = Tile.objects.exclude(scene=self.scene).filter(scene__type="global", x=self.x + offset[0], y=self.y + offset[1], z=self.z)[0].scene
                print(oscene)
            else:
                return

        move_mask = Tile.objects.get(scene=oscene, x=self.x + offset[0], y=self.y + offset[1], z=self.z).move_mask

        if move_mask.can_enter(self):
            self.scene = oscene
            self.x += offset[0]
            self.y += offset[1]
            move_mask.on_enter(self)

        self.face = direction
        print("Player {name} at Scene({scene_id}) x={x}, y={y}, z={z}, face={face}".format(
            name=self.nickname,
            scene_id=self.scene.id,
            x=self.x,
            y=self.y,
            z=self.z,
            face=self.face
        ))

        for ent in self.scene.entities.filter(x=self.x, y=self.y, z=self.z):
            ent.on_player_collide(self)

    class Meta:
        db_table = "players"

    def __str__(self):
        return "Player: %s @ %s(%i)[%i, %i, %i] facing %s, last_login = %s" % (
            self.nickname, self.scene.name, self.scene.id, self.x, self.y, self.z, self.face, self.last_login
        )

class MoveMask(models.Model):
    key_name = models.CharField(max_length=32, unique=True)
    color = models.CharField(max_length=7)

    @classmethod
    def from_db(cls, db, field_names, values):
        from map import movemasks
        print(values)
        klass = getattr(movemasks, values[1] + "MoveMask", None)
        if klass is None:
            return None

        if cls._deferred:
            instance = klass(**zip(field_names, values))
        else:
            instance = klass(*values)
        instance._state.adding = False
        instance._state.db = db
        # customization to store the original field values on the instance
        instance._loaded_values = dict(zip(field_names, values))
        return instance

    def __str__(self):
        return "{classname}(id={id}, name={name}, color={color})".format(classname=self.__class__.__name__, id=self.id, name=self.key_name, color=self.color)

class Tile(models.Model):
    scene = models.ForeignKey(Scene, related_name="tiles")
    tile_type = models.ForeignKey(TileType)
    x = models.IntegerField(blank=False)
    y = models.IntegerField(blank=False)
    z = models.IntegerField(blank=False, default=0)
    move_mask = models.ForeignKey(MoveMask)

    @property
    def ox(self):
        return self.x - self.scene.x

    @property
    def oy(self):
        return self.y - self.scene.y

    def __str__(self):
        return "Tile(id={id}, scene={scene}, x={x}, y={y}, z={z}, tile_type={tile_type}, move_mask={move_mask})".format(
            id=self.id,
            scene=self.scene,
            x=self.x,
            y=self.y,
            z=self.z,
            tile_type=self.tile_type,
            move_mask=self.move_mask
        )

    class Meta:
        ordering = ["z", "x", "y"]

class StatusEffect(models.Model):
    name = models.CharField(max_length=32)

class Element(models.Model):
    name = models.CharField(max_length=16, null=False, unique=True)
    strong_against = models.ManyToManyField("self", symmetrical=False, related_name="weak_against")

class Ability(models.Model):
    name = models.CharField(max_length=16, null=False, unique=True)
    max_pp = models.IntegerField(null=False)
    category = models.CharField(max_length=10, null=False, choices=(
        ("normal", "Normal"),
        ("status", "Status"),
        ("special", "Special")
    ))
    element_type = models.ForeignKey(Element)
    damage = models.IntegerField(null=False)
    accuracy = models.IntegerField(null=False)
    status_effect = models.ForeignKey(StatusEffect, null=True)
    status_chance = models.IntegerField(null=True)

class LearnableAbility(models.Model):
    monster_type = models.ForeignKey("MonsterType", related_name="abilities")
    ability = models.ForeignKey(Ability)
    learn_level = models.IntegerField()

class UsableAbility(models.Model):
    monster = models.ForeignKey("Monster", related_name="abilities")
    ability = models.ForeignKey(Ability)
    pp = models.IntegerField(null=False)
    slot = models.IntegerField(null=False)

class MonsterType(models.Model):
    name = models.CharField(max_length=64, null=False, unique=True)
    element_type = models.ForeignKey(Element)

    base_health = models.IntegerField(null=False)
    health_growth = models.IntegerField(null=False)

    base_attack = models.IntegerField(null=False)
    attack_growth = models.IntegerField(null=False)

    base_defense = models.IntegerField(null=False)
    defense_growth = models.IntegerField(null=False)

    base_speed = models.IntegerField(null=False)
    speed_growth = models.IntegerField(null=False)

    abilities_set = models.ManyToManyField(Ability, through=LearnableAbility)
    evolution = models.ForeignKey("self", null=True, related_name="previous_evolution")

class Monster(models.Model):
    name = models.CharField(max_length=64, null=False)
    monster_type = models.ForeignKey(MonsterType)
    owner = models.ForeignKey("Player", null=True, related_name="monsters") # when null its wild monster
    health = models.IntegerField(null=False)
    level = models.IntegerField(null=False, default=1)
    experience = models.IntegerField(null=False, default=0)
    slot = models.IntegerField(null=False, default=0)
    abilities_set = models.ManyToManyField(Ability, through=UsableAbility)

    @property
    def max_health(self):
        return self.monster_type.base_health + self.monster_type.health_growth * self.level

    @property
    def attack(self):
        return self.monster_type.base_attack + self.monster_type.attack_growth * self.level

    @property
    def defense(self):
        return self.monster_type.base_defense + self.monster_type.defense_growth * self.level

    @property
    def speed(self):
        return self.monster_type.base_speed + self.monster_type.speed_growth * self.level

    @property
    def element_type(self):
        return self.monster_type.element_type

    @property
    def takedown_experience(self):
        return round(self.level * 100 * (1.5 if owner is not None else 1.0) / 7)

    @property
    def next_level_experience(self):
        return self.level * self.level * self.level

    def use_ability(self, ability, enemy):
        if ability.pp <= 0:
            return True, False, 0, False

        if random.randrange(0, 100) < ability.ability.accuracy:
            return False, True, 0, False # missed, not critical

        critical_chance = 0.20
        critical = False
        damage = ((self.level * 2 + 10) / 250) * (self.attack / enemy.defense) * ability.ability.damage

        # Same Type Attack Bonus
        if ability.ability.element_type == self.element_type:
            damage *= 1.5

        # Crits
        if random.random() < critical_chance:
            damage *= 1.5
            critical = True

        # Elemental Weakness
        if ability.ability.element_type in enemy.element_type.weak_against.all():
            damage *= 2.0
        elif ability.ability.element_type in enemy.element_type.strong_against.all():
            damage *= 0.5

        damage = round(damage)
        ability.pp -= 1
        enemy.health -= damage
        enemy.save()

        return False, False, damage, critical

    class Meta:
        ordering = ["slot"]
