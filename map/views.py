from PIL import Image
from pathlib import Path

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import Max, F, Q

from rest_framework import viewsets, views
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from map.models import *
from map.serializers import *

class PlayerViewSet(viewsets.ModelViewSet):
    serializer_class = PlayerSerializer
    queryset = Player.objects.all()

    @list_route(methods=["get"])
    def current(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            player = self.queryset.get(user__id=request.user.id)
            return Response(PlayerSerializer(player).data, 200)

        return Response({}, 403)

class PlayerMonsterViewSet(viewsets.ModelViewSet):
    serializer_class = MonsterSerializer
    queryset = Monster.objects.none()

    def get_queryset(self):
        return Monster.objects.filter(owner=self.kwargs['player_pk']).select_related("monster_type")

class MapViewSet(viewsets.ModelViewSet):
    queryset = Scene.objects.all().annotate(maxZ=Max("tiles__z") + 1)
    serializer_class = SceneSerializer

    @list_route(methods=["get"])
    def size(self, request, *args, **kwargs):
        width = self.queryset.filter(type='global').aggregate(width=Max(F("x") + F("sizex")))
        height = self.queryset.filter(type='global').aggregate(height=Max(F("y") + F("sizey")))
        return Response({ "world_width": width['width'], "world_height": height['height'] }, 200)

    @detail_route(methods=["post"])
    def removeLayer(self, request, *args, **kwargs):
        layer = request.POST['layer']
        m = self.get_object()
        Tile.objects.filter(scene=m, z=layer).delete()
        Tile.objects.filter(scene=m, z__gt=layer).update(z=F("z") - 1)
        return Response({"detail": "done"}, 200)

    @detail_route(methods=["get","post"])
    def image(self, request, *args, **kwargs):
        if request.method == "GET":
            ret = []

            m = self.get_object()
            maxZ = Tile.objects.filter(scene=m).aggregate(Max('z'))

            try:
                for i in range(maxZ['z__max'] + 1):
                    ret.append("%simages/maps/%s/%d.png" % (settings.STATIC_URL, m.key_name, i))
            except:
                pass

            return Response(ret, 200)

        elif request.method == "POST":
            m = self.get_object()
            z = Tile.objects.filter(scene=m).aggregate(Max('z'))
            base_path = Path(settings.BASE_DIR) / "Monster" / "files";
            tilesets_images = {}

            for i in range(z['z__max'] + 1):
                map_image = Image.new("RGBA", (m.sizex * 32, m.sizey * 32))
                tiles = Tile.objects.filter(scene=m, x__lt=m.x + m.sizex, y__lt=m.y + m.sizey, z=i)

                for tile in tiles:
                    if tile.tile_type.tileset.name not in tilesets_images:
                        tilesets_images[tile.tile_type.tileset.name] = Image.open(base_path / tile.tile_type.tileset.path)

                    tileset_image = tilesets_images[tile.tile_type.tileset.name]
                    cropped = tileset_image.crop((-tile.tile_type.x, -tile.tile_type.y, -tile.tile_type.x + 32, -tile.tile_type.y + 32))
                    map_image.paste(cropped, (tile.ox * 32, tile.oy * 32))
                    #cropped.save(base_path / "maps" / m.key_name / ("%d.png" % tile.id))

                (base_path / "images" / "maps" / m.key_name).mkdir(exist_ok=True)
                map_image.save(str(base_path / "images" / "maps" / m.key_name / ("%d.png" % i)))

            return Response({"detail": "done"})

class TileViewSet(viewsets.ModelViewSet):
    serializer_class = TileSerializer

    def get_queryset(self):
        mapid = self.kwargs.get('map_pk')
        return Tile.objects.filter(scene__id=mapid)

    @list_route(methods=["delete"])
    def clear_outside(self, request, *args, **kwargs):
        scene = Scene.objects.get(id=self.kwargs.get("map_pk"))
        self.get_queryset().filter(Q(x__gte=scene.x + scene.sizex) | Q(y__gte=scene.y + scene.sizey) | Q(x__lt=scene.x) | Q(y__lt=scene.y)).delete()
        return Response({"detail": "done"}, 200)

    @list_route(methods=["post"])
    def clear(self, request, *args, **kwargs):
        self.get_queryset().delete()
        return Response({"detail": "Tiles removed"}, 200)

    def list(self, request, *args, **kwargs):
        return Response(TileSerializer(self.get_queryset(), many=True).data, 200)

    def create(self, request, *args, **kwargs):
        ret = []
        for tile in request.data:
            serializer = self.get_serializer(self.get_queryset().get(id=tile['id']) if "id" in tile else None, tile)
            if serializer.is_valid():
                ret.append(serializer.save())

        return Response(self.get_serializer(ret, many=True).data, 200)


class EntityViewSet(viewsets.ModelViewSet):
    serializer_class = EntitySerializer
    queryset = Entity.objects.all()

class MapEntityViewSet(EntityViewSet):
    def get_queryset(self):
        mapid = self.kwargs.get("map_pk", None)
        return Entity.objects.filter(scene__id=mapid)

    def create(self, request, *args, **kwargs):
        ret = []
        for ent in request.data:
            serializer = self.get_serializer(self.get_queryset().get(id=ent['id']) if "id" in ent else None, ent)

            if serializer.is_valid():
                ret.append(serializer.save())

        #print(ret[0])
        return Response(self.get_serializer(ret, many=True).data, 200)

class EntityTypeViewSet(viewsets.ModelViewSet):
    serializer_class = EntityTypeSerializer
    queryset = EntityType.objects.all()

class TileSetViewSet(viewsets.ModelViewSet):
    queryset = TileSet.objects.all()
    serializer_class = TileSetSerializer

class TileTypeViewSet(viewsets.ModelViewSet):
    queryset = TileType.objects.all()
    serializer_class = TileTypeSerializer

class MoveMaskViewSet(viewsets.ModelViewSet):
    queryset = MoveMask.objects.all()
    serializer_class = MoveMaskSerializer

class StatusEffectViewSet(viewsets.ModelViewSet):
    queryset = StatusEffect.objects.all()
    serializer_class = StatusEffectSerializer

class ElementViewSet(viewsets.ModelViewSet):
    queryset = Element.objects.all()
    serializer_class = ElementSerializer

class AbilityViewSet(viewsets.ModelViewSet):
    queryset = Ability.objects.all()
    serializer_class = AbilitySerializer

class MonsterTypeViewSet(viewsets.ModelViewSet):
    queryset = MonsterType.objects.all()
    serializer_class = MonsterTypeSerializer

class MonsterViewSet(viewsets.ModelViewSet):
    queryset = Monster.objects.all()
    serializer_class = MonsterSerializer
