from django.dispatch import Signal

monster_encountered = Signal(providing_args=['monster'])
