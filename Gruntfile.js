module.exports = function(grunt) {
    function jsMapeditor(name) {
        return "Monster/files/js/mapeditor/" + name;
    }

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            mapeditor: {
                src: [
                    jsMapeditor("app.js"),
                    jsMapeditor("models/urls.js"),
                    jsMapeditor("models/scene.js"),
                    jsMapeditor("models/*.js"),
                    jsMapeditor("collections/*.js"),
                    jsMapeditor("views/*.js")
                ],
                dest: "Monster/files/js/mapeditor.js"
            },
            game: {
                src: [
                    "Monster/files/js/game/app.js",
                    "Monster/files/js/game/models/player.js",
                    "Monster/files/js/game/models/*.js",
                    "Monster/files/js/game/models/*/*.js",
                    "Monster/files/js/game/collections/*.js",
                    "Monster/files/js/game/views/*.js",
                    "Monster/files/js/game/views/*/*.js",
                    "Monster/files/js/game/tests.js"
                ],
                dest: "Monster/files/js/game.js"
            }
        },
        watch: {
            mapeditor: {
                files: ['Monster/files/js/mapeditor/*.js', 'Monster/files/js/mapeditor/*/*.js'],
                tasks: ['concat:mapeditor'],
                options: {
                    spawn: true,
                },
            },
            game: {
                files: ['Monster/files/js/game/*.js', "Monster/files/js/game/*/*.js", "Monster/files/js/game/*/*/*.js"],
                tasks: ['concat:game'],
                options: {
                    spawn: true
                }
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            monster: {
                tasks: ["watch:mapeditor", "watch:game"]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['concat:mapeditor', 'concat:game', "concurrent:monster"]);
    //grunt.registerTask('game', []);
};
